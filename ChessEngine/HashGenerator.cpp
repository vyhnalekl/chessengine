#include "stdafx.h"
#include <random>
#include "Board.h"
#include "Rating.h"
#include <iostream>
#include <fstream>
unsigned long long PiecesRand[12][64];
unsigned long long SideOnMove;
unsigned long long CastlePermition[4];
unsigned long long enPass[16];

unsigned long long getRandom64() {
	unsigned long long result = (unsigned long long) rand(); // 32 bit rand
	result <<= 32;
	result = result | (unsigned long long) rand(); // 32 bit rand
	return result;
}
int initCallCounter = 0;
/*
	returns true if constants are sucessfully loaded from file
			false if they are generated
*/
bool Init(bool file) {
	if (!file) {
		for (int i = 0; i < 12; i++) {
			for (int j = 0; j < 64; j++) {
				PiecesRand[i][j] = getRandom64();
			}
		}
		for (int j = 0; j < 16; j++) {
			enPass[j] = getRandom64();
		}
		SideOnMove = getRandom64();
		for (int i = 0; i < 4; i++) {
			CastlePermition[i] = getRandom64();
		}
		return false;
	}
	initCallCounter++;
	if (initCallCounter > 1) {
		std::cout << "ERROR: Hash Generator is initialized twice!" << std::endl;
		return false;
	}
	try {
		std::ifstream f("random", std::fstream::binary);
		//while (!f.eof()) {
			unsigned long long n = 0;
			for (int i = 0; i < 12; i++) {
				for (int j = 0; j < 64; j++) {
					f.read(reinterpret_cast<char*>(&n), sizeof(n));
					PiecesRand[i][j] = n;
				}
			}
			for (int j = 0; j < 4; j++) {
				f.read(reinterpret_cast<char*>(&n), sizeof(n));
				CastlePermition[j] = n;
			}
			for (int j = 0; j < 16; j++) {
				f.read(reinterpret_cast<char*>(&n), sizeof(n));
				enPass[j] = n;
			}
			std::cout << "INFO: Hash constants are succesfully loaded from file" << std::endl;
		//}
			f.close();
			return true;
	}
	catch (...) {
		// must be called before hashing 
		// only once 
		std::cout << "WARNING: Hash File Not found can't use saved games" << std::endl;
		for (int i = 0; i < 12; i++) {
			for (int j = 0; j < 64; j++) {
				PiecesRand[i][j] = getRandom64();
			}
		}
		for (int j = 0; j < 16; j++) {
			enPass[j] = getRandom64();
		}
		SideOnMove = getRandom64();
		for (int i = 0; i < 4; i++) {
			CastlePermition[i] = getRandom64();
		}
	}
	return false;
}

/*
Hashing: 
		Pieces
		Who is on Move
		CastlePremition
		EnPassant
*/
char getEnPassantIndex(U64 sqr) {
	U64 temp = sqr;
	return POP(&temp);
}
unsigned long long generatePositionKey( Board * const b) {
	if (initCallCounter < 1) {
		std::cout << "ERROR: Hash Generator has not ben initialized !" << std::endl;
		return 0;
	}
	unsigned long long result = 0;
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if ((*b).whitePiecesList[i][j] != NONE) {
				result ^= PiecesRand[i][to64index((*b).whitePiecesList[i][j])];
			}
		}
	}
	//king
	result ^= PiecesRand[5][to64index((*b).whitePiecesList[5][0])];
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if ((*b).blackPiecesList[i][j] != NONE) {
				// first piecesRand[6] is for white
				result ^= PiecesRand[i + 6][to64index((*b).blackPiecesList[i][j])];
			}
		}
	}
	//king
	result ^= PiecesRand[11][to64index((*b).blackPiecesList[5][0])];
	
	result ^= (unsigned long long) (*b).whiteOnMove;
	result ^= enPass[getEnPassantIndex(b->enPassant)];
	result ^= CastlePermition[(*b).whiteCastleAlowed];
	result ^= CastlePermition[(*b).blackCastleAlowed];

	return result;
}
/*
unsigned long long updateCastelingKey(Board * const b, Move mv) {

	unsigned long long result = (*b).key;
	try {
		char movedPiece = (*b).pieces[mv.to];
		result ^= PiecesRand[movedPiece - 1][to64index(mv.from)];
		result ^= PiecesRand[movedPiece - 1][to64index(mv.to)];
		if (mv.to == G8) {
			result ^= PiecesRand[blackRook - 1][to64index(H8)];
			result ^= PiecesRand[blackRook - 1][to64index(F8)];
		}else if (mv.to == C8) {
			result ^= PiecesRand[blackRook - 1][to64index(A8)];
			result ^= PiecesRand[blackRook - 1][to64index(D8)];
		}else if (mv.to == G1) {
			result ^= PiecesRand[whiteRook - 1][to64index(H1)];
			result ^= PiecesRand[whiteRook - 1][to64index(F1)];
		}
		else if (mv.to == C1) {
			result ^= PiecesRand[whiteRook - 1][to64index(A1)];
			result ^= PiecesRand[whiteRook - 1][to64index(D1)];
		}
		else {
			std::cout << "ERROR: Not a Casteling Move" << std::endl;
		}

		result ^= movedPiece < BLACK_OFFSET; // TODO maybe a nit sophisticated solution
		if ((*b).whiteCastleAlowed != mv.currentCastleWhite) {
			result ^= CastlePermition[(*b).whiteCastleAlowed];
		}
		if ((*b).blackCastleAlowed != mv.currentCastleBlack) {
			result ^= CastlePermition[(*b).blackCastleAlowed];
		}
	}
	catch (...) {
		std::cout << "ERROR: unexpected error when updating hash key of board" << std::endl;
	}
	return result;
}
unsigned long long updateTransformKey(Board * const b, Move mv) {
	unsigned long long result = (*b).key;
	try {
		char transformedPiece = (*b).pieces[mv.to];
		result ^= PiecesRand[transformedPiece - 1][to64index(mv.to)];
		if (transformedPiece < BLACK_OFFSET) {
			result ^= PiecesRand[whitePawn - 1][to64index(mv.from)];
		}
		else {
			result ^= PiecesRand[blackPawn - 1][to64index(mv.from)];
		}
		
		

		if (mv.taken != EMPTY) {
			result ^= PiecesRand[mv.taken - 1][to64index(mv.to)];
		}
		result ^= transformedPiece < BLACK_OFFSET; // TODO maybe a nit sophisticated solution
		if ((*b).whiteCastleAlowed != mv.currentCastleWhite) {
			result ^= CastlePermition[(*b).whiteCastleAlowed];
		}
		if ((*b).blackCastleAlowed != mv.currentCastleBlack) {
			result ^= CastlePermition[(*b).blackCastleAlowed];
		}
	}
	catch (...) {
		std::cout << "ERROR: unexpected error when updating hash key of board" << std::endl;
	}

	return result;
}
unsigned long long updateEnPassKey(Board * const b, Move mv) {
	unsigned long long result = (*b).key;
	char movedPiece = (*b).pieces[mv.to];
	result ^= PiecesRand[movedPiece - 1][to64index(mv.from)];
	result ^= PiecesRand[movedPiece - 1][to64index(mv.to)];

	if (movedPiece == whitePawn) {
		result ^= PiecesRand[blackPawn -1][to64index(mv.to - 10)];
	}
	else {
		result ^= PiecesRand[whitePawn - 1][to64index(mv.to + 10)];
	}
	result ^= movedPiece == whitePawn;
	return result;
}
unsigned long long updateKey( Board * const b, Move mv) {
	
	unsigned long long result = (*b).key;
	try {
		if (mv.isCastelingMove) {
			return updateCastelingKey(b, mv);
		}
		if (mv.transforms != EMPTY) {
			return updateTransformKey(b, mv);
		}
		if (mv.isEnPassantMove) {
			return updateEnPassKey(b, mv);
		}
		char movedPiece = (*b).pieces[mv.to];
		result ^= PiecesRand[movedPiece - 1][to64index(mv.from)];
		result ^= PiecesRand[movedPiece - 1][to64index(mv.to)];
		
		if (mv.taken != EMPTY) {
			result ^= PiecesRand[mv.taken - 1][to64index(mv.to)];
		}
		result ^= movedPiece < BLACK_OFFSET; // TODO maybe a nit sophisticated solution
		if (mv.enPassant != NONE) {
			result ^= enPass[getEnPassantIndex(mv.enPassant)];
		}
		if ((*b).whiteCastleAlowed != mv.currentCastleWhite) {
			result ^= CastlePermition[(*b).whiteCastleAlowed];
		}
		if ((*b).blackCastleAlowed != mv.currentCastleBlack) {
			result ^= CastlePermition[(*b).blackCastleAlowed];
		}
	}
	catch (...) {
		std::cout << "ERROR: unexpected error when updating hash key of board" << std::endl;
	}
	
	return result;
}
*/
unsigned long long simpleHash(Board *b) {
	/*unsigned long long hash = 17;
	for (int i = A1; i <= H8; i++) {
		hash = hash * 19 + b->pieces[i];
	}
	hash ^= (unsigned long long) (*b).whiteOnMove;
	hash ^= enPass[getEnPassantIndex(b->enPassant)];
	hash ^= CastlePermition[(*b).whiteCastleAlowed];
	hash ^= CastlePermition[(*b).blackCastleAlowed];
	return hash;*/
	unsigned long long result = 0;
	/*for (int i = 0; i < 5; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if ((*b).whitePiecesList[i][j] != NONE) {
				result ^= PiecesRand[i][to64index((*b).whitePiecesList[i][j])];
			}
		}
	}
	//king
	result ^= PiecesRand[5][to64index((*b).whitePiecesList[5][0])];
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if ((*b).blackPiecesList[i][j] != NONE) {
				// first piecesRand[6] is for white
				result ^= PiecesRand[i + 6][to64index((*b).blackPiecesList[i][j])];
			}
		}
	}
	
	//king
	result ^= PiecesRand[11][to64index((*b).blackPiecesList[5][0])];
	*/
	for (int i = A1; i <= H8; i++) {
		if (b->pieces[i] != EMPTY && b->pieces[i] != NONE) {
			result ^= PiecesRand[b->pieces[i] - 1][to64index(i)];
		}
	}


	result ^= (unsigned long long) (*b).whiteOnMove;
	if (b->enPassant != NONE) {
		result ^= enPass[getEnPassantIndex(b->enPassant)];
	}
	
	result ^= CastlePermition[(*b).whiteCastleAlowed];
	result ^= CastlePermition[(*b).blackCastleAlowed];

	return result;
}

unsigned long long simpleHash(Bitboard *b) {
	U64 temp = 0;
	unsigned long long result = 0;
	for (int i = 0; i < 6; i++) {
		temp = b->whitePiecesList[i];
		while (temp) {
			result ^= PiecesRand[i][POP(&temp)];
		}
		temp = b->blackPiecesList[i];
		while (temp) {
			result ^= PiecesRand[i + 6][POP(&temp)];
		}
	}
	/*
	for (int i = A1; i <= H8; i++) {
		if (b->pieces[i] != EMPTY && b->pieces[i] != NONE) {
			result ^= PiecesRand[b->pieces[i] - 1][to64index(i)];
		}
	}*/

	if (b->whiteOnMove) {
		result ^= SideOnMove;
	}

	
	if (b->enPassant != 0) {
		result ^= enPass[getEnPassantIndex(b->enPassant)];
	}

	result ^= CastlePermition[(*b).whiteCastleAlowed];
	result ^= CastlePermition[(*b).blackCastleAlowed];

	return result;
}

unsigned long long hashMove(U64 key, Move* mv) {
	//Remove Old hash
	key ^= PiecesRand[mv->movedPiece - 1][mv->from];
	//Add moved
	key ^= PiecesRand[mv->movedPiece - 1][mv->to];
	// Remove taken
	if (mv->taken) {
		key ^= PiecesRand[mv->taken - 1][mv->to];
	}
	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}
	
	//Hash side
	key ^= SideOnMove;

	return key;
}

unsigned long long hashTransformationMove(U64 key, Move* mv) {
	//Remove Old hash
	key ^= PiecesRand[mv->movedPiece - 1][mv->from];
	//Add moved
	key ^= PiecesRand[mv->transforms - 1][mv->to];
	// Remove taken
	if (mv->taken) {
		key ^= PiecesRand[mv->taken - 1][mv->to];
	}

	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}

	//Hash side
	key ^= SideOnMove;
	return key;
}

unsigned long long hashWhiteEnPassantMove(U64 key, Move* mv) {
	//Remove Old hash
	key ^= PiecesRand[mv->movedPiece - 1][mv->from];
	//Add moved
	key ^= PiecesRand[mv->movedPiece - 1][mv->to];
	// Remove taken
	key ^= PiecesRand[mv->taken - 1][mv->to - 8];

	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}

	//Hash side
	key ^= SideOnMove;
	return key;
}

unsigned long long hashBlackEnPassantMove(U64 key, Move* mv) {
	//Remove Old hash
	key ^= PiecesRand[mv->movedPiece - 1][mv->from];
	//Add moved
	key ^= PiecesRand[mv->movedPiece - 1][mv->to];
	// Remove taken
	key ^= PiecesRand[mv->taken - 1][mv->to + 8];

	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}

	//Hash side
	key ^= SideOnMove;
	return key;
}

unsigned long long hashCastelingQueenBlackMove(U64 key, Move * mv) {
	//Remove Old hash
	key ^= PiecesRand[5][e8];
	key ^= PiecesRand[3][a8];
	//Add moved
	key ^= PiecesRand[5][c8];
	key ^= PiecesRand[3][d8];

	//Hash side
	key ^= SideOnMove;

	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}
	return key;
}

unsigned long long hashCastelingKingBlackMove(U64 key, Move * mv) {
	//Remove Old hash
	key ^= PiecesRand[5][e8];
	key ^= PiecesRand[3][h8];
	//Add moved
	key ^= PiecesRand[5][g8];
	key ^= PiecesRand[3][f8];

	//Hash side
	key ^= SideOnMove;
	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}

	return key;
}

unsigned long long hashCastelingQueenWhiteMove(U64 key, Move * mv) {
	//Remove Old hash
	key ^= PiecesRand[5][e1];
	key ^= PiecesRand[3][a1];
	//Add moved
	key ^= PiecesRand[5][c1];
	key ^= PiecesRand[3][d1];

	//Hash side
	key ^= SideOnMove;

	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}
	return key;
}

unsigned long long hashCastelingKingWhiteMove(U64 key, Move * mv) {
	//Remove Old hash
	key ^= PiecesRand[5][e1];
	key ^= PiecesRand[3][h1];
	//Add moved
	key ^= PiecesRand[5][g1];
	key ^= PiecesRand[3][f1];

	//Hash side
	key ^= SideOnMove;

	if (mv->enPassant) {
		key ^= enPass[getEnPassantIndex(mv->enPassant)];
	}
	if (mv->enPassantBeforeMove) {
		key ^= enPass[getEnPassantIndex(mv->enPassantBeforeMove)];
	}
	return key;
}

unsigned long long hashCastelingChange(U64 key, char from, char to) {
	//Remove Old hash
	key ^= CastlePermition[from];
	key ^= CastlePermition[to];
	return key;
}


