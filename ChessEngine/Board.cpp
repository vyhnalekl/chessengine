
#include "stdafx.h"
#include "Board.h"
#include "Definitions.cpp"
#include "GenerateMove.h"
#include <iostream>
#include "Debug.h"
#include "MoveList.h"
#include "HashGenerator.h"

Board::Board() {
	for (int i = 0; i < CHESSBOARD_LENGTH; i++) {
		if (41 <= i && i <= 78 && (!(i % 10 == 0 || i % 10 == 9))) {
			this->pieces[i] = EMPTY; // TODO, still empty on left and right side
		}
		else if (A2 <= i && i <= H2) {
			this->pieces[i] = whitePawn;
		}
		else if (A7 <= i && i <= H7) {
			this->pieces[i] = blackPawn;
		}
		else {
			this->pieces[i] = NONE; // away from board
		}
	}
	this->pieces[A1] = this->pieces[H1] = whiteRook;
	this->pieces[B1] = this->pieces[G1] = whiteKnight;
	this->pieces[C1] = this->pieces[F1] = whiteBishop;
	this->pieces[D1] = whiteQueen;
	this->pieces[E1] = whiteKing;

	this->pieces[A8] = this->pieces[H8] = blackRook;
	this->pieces[B8] = this->pieces[G8] = blackKnight;
	this->pieces[C8] = this->pieces[F8] = blackBishop;
	this->pieces[D8] = blackQueen;
	this->pieces[E8] = blackKing;

	//this->whiteOnMove = true;
	this->whiteCastleAlowed = BothSides;
	this->blackCastleAlowed = BothSides;
	//this->key = simpleHash(this);
	/*char whitePieces[6][MAX_NUMBER_OF_PIECES] = { { A2, B2, C2, D2, E2, F2, G2, H2, NONE, NONE },
	{ B1, G2, NONE, NONE, NONE, NONE, NONE, NONE },
	{ C1, F2, NONE, NONE, NONE, NONE, NONE, NONE },
	{ A1, H1 ,NONE, NONE, NONE, NONE, NONE, NONE },
	{ D1, NONE ,NONE, NONE, NONE, NONE, NONE, NONE },
	{ E1, NONE ,NONE, NONE, NONE, NONE, NONE, NONE }, };
	this->whitePiecesList = whitePieces;*/
	//this->piecesOnBoard = 0b1111111111111111000000000000000000000000000000001111111111111111;
}

void Board::doEnPassantMove(Move* mv) {
	//debug
	/*if (pieces[(*mv).to] != EMPTY) {
	std::cout << "Blah" << std::endl;
	}*/
	bool white = (pieces[(*mv).from] < BLACK_OFFSET);
	char piece = this->pieces[(*mv).from];
	this->pieces[(*mv).from] = EMPTY;
	//change value in piece list
	if (white) {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (whitePiecesList[0][i] == (*mv).from) {
				whitePiecesList[0][i] = (*mv).to;
				(*mv).indexOfMoved = i;
				break;
			}
		}
	}
	else {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (blackPiecesList[0][i] == (*mv).from) {
				blackPiecesList[0][i] = (*mv).to;
				(*mv).indexOfMoved = i;
				break;
			}
		}
	}

	(*mv).taken = EMPTY;
	this->pieces[(*mv).to] = piece;
	// Need to remove taken pawn
	if (white) {
		(*mv).taken = blackPawn;
		// white takes black
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (this->blackPiecesList[0][i] == (*mv).to - 10) {
				pieces[blackPiecesList[0][i]] = EMPTY;
				this->blackPiecesList[0][i] = NONE;
				(*mv).indexOfTaken = i;
				break;
			}
		}
	}
	else {
		(*mv).taken = whitePawn; 
		// black takes white
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (this->whitePiecesList[0][i] == (*mv).to + 10) {
				pieces[whitePiecesList[0][i]] = EMPTY;
				this->whitePiecesList[0][i] = NONE;
				(*mv).indexOfTaken = i;
				break;
			}
		}
	}
	/*if (!checkBoard(pieces) ||
	!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
	std::cout << "Blah " << std::endl;
	}*/
}

void Board::undoEnPassantMove(Move *mv) {
	/*if (!checkBoard(pieces) ||
	!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
	std::cout << "Blah " << std::endl;
	}*/
	pieces[(*mv).from] = pieces[(*mv).to];
	bool white = (pieces[(*mv).to] < BLACK_OFFSET);
	//remove piece from piece list
	if (white) {
		whitePiecesList[0][(*mv).indexOfMoved] = (*mv).from;
	}
	else {
		blackPiecesList[0][(*mv).indexOfMoved] = (*mv).from;
	}
	pieces[(*mv).to] = EMPTY;

	// return taken piece
	if (white) {
		blackPiecesList[0][(*mv).indexOfTaken] = (*mv).to - 10;
		pieces[(*mv).to - 10] = blackPawn;
	}
	else {
		whitePiecesList[0][(*mv).indexOfTaken] = (*mv).to + 10;
		pieces[(*mv).to + 10] = whitePawn;
	}

	/*if (!checkBoard(pieces) ||
	!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
	std::cout << "Blah " << std::endl;
	}*/
}

void Board::doTransformationMove(Move *mv) {
	bool white = (pieces[(*mv).from] < BLACK_OFFSET);
	//remove from board
	pieces[(*mv).from] = EMPTY;
	//remove from piece list
	if (white) {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (whitePiecesList[0][i] == (*mv).from) {
				whitePiecesList[0][i] = NONE;
				(*mv).indexOfMoved = i;
				break;
			}
		}
	}
	else {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (blackPiecesList[0][i] == (*mv).from) {
				blackPiecesList[0][i] = NONE;
				(*mv).indexOfMoved = i;
				break;
			}
		}
	}
	//set taken value
	(*mv).taken = pieces[(*mv).to];
	//remove opponent piece from piece list
	if ((*mv).taken != EMPTY) {
		if (!white) { // black
			for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
				if (whitePiecesList[(*mv).taken - 1][i] == (*mv).to) {
					whitePiecesList[(*mv).taken - 1][i] = NONE;
					(*mv).indexOfTaken = i;
					break;
				}
			}
		}
		else {
			for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
				if (blackPiecesList[(*mv).taken - BLACK_OFFSET][i] == (*mv).to) {
					blackPiecesList[(*mv).taken - BLACK_OFFSET][i] = NONE;
					(*mv).indexOfTaken = i;
					break;
				}
			}
		}
	}

	//set new in pieces
	pieces[(*mv).to] = (*mv).transforms;
	// update piece list
	if (white) {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (whitePiecesList[(*mv).transforms - 1][i] == NONE) {
				whitePiecesList[(*mv).transforms - 1][i] = (*mv).to;
				(*mv).indexOfTransformed = i;
				break;
			}
		}
	}
	else {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (blackPiecesList[(*mv).transforms - BLACK_OFFSET][i] == NONE) {
				blackPiecesList[(*mv).transforms - BLACK_OFFSET][i] = (*mv).to;
				(*mv).indexOfTransformed = i;
				break;
			}
		}
	}
	/*if (!checkBoard(pieces) ||
	!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
	std::cout << "Blah " << std::endl;
	}*/
}

void Board::undoTransformationMove(Move *mv) {
	/*if (!checkBoard(pieces) ||
	!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
	std::cout << "Blah " << std::endl;
	}*/
	bool white = (pieces[(*mv).to] < BLACK_OFFSET);
	// set the pawn back
	pieces[(*mv).from] = (white) ? whitePawn : blackPawn;
	// update piece list
	if (white) {
		whitePiecesList[0][(*mv).indexOfMoved] = (*mv).from;
	}
	else {
		blackPiecesList[0][(*mv).indexOfMoved] = (*mv).from;
	}

	// get back opponents piece
	pieces[(*mv).to] = (*mv).taken;
	if ((*mv).taken != EMPTY) {
		//update piece list
		if (!white) { // black
			whitePiecesList[(*mv).taken - 1][(*mv).indexOfTaken] = (*mv).to;
		}
		else {
			blackPiecesList[(*mv).taken - BLACK_OFFSET][(*mv).indexOfTaken] = (*mv).to;
		}
	}

	// remove transformed piece
	if (white) {
		whitePiecesList[(*mv).transforms - 1][(*mv).indexOfTransformed] = NONE;
	}
	else {
		blackPiecesList[(*mv).transforms - BLACK_OFFSET][(*mv).indexOfTransformed] = NONE;
	}

	/*if (!checkBoard(pieces) ||
	!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
	std::cout << "Blah " << std::endl;
	}*/
}

void Board::doCastelingMove(Move *mv) {
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
	pieces[(*mv).to] = pieces[(*mv).from];
	pieces[(*mv).from] = EMPTY;
	//update kings piece list
	if (pieces[(*mv).to] < BLACK_OFFSET) {
		//white
		whitePiecesList[5][0] = (*mv).to;
	}
	else {
		//black
		blackPiecesList[5][0] = (*mv).to;
	}

	if ((*mv).from == E1 && (*mv).to == G1) {
		//king side casteling move
		// need to move rook also
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (this->whitePiecesList[3][i] == H1) {
				this->whitePiecesList[3][i] = F1;
				this->pieces[H1] = EMPTY;
				this->pieces[F1] = whiteRook;
				(*mv).indexOfTransformed = i;
				break;
			}
		}
		this->whiteCastleAlowed = Made;
	}
	else if ((*mv).from == E1 && (*mv).to == C1) {
		//queen side casteling move
		// need to move rook also
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (this->whitePiecesList[3][i] == A1) {
				this->whitePiecesList[3][i] = D1;
				this->pieces[A1] = EMPTY;
				this->pieces[D1] = whiteRook;
				(*mv).indexOfTransformed = i;
				break;
			}
		}
		this->whiteCastleAlowed = Made;
	}
	else if ((*mv).from == E8 && (*mv).to == G8) {
		//king side casteling move
		// need to move rook also
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (this->blackPiecesList[3][i] == H8) {
				this->blackPiecesList[3][i] = F8;
				this->pieces[H8] = EMPTY;
				this->pieces[F8] = blackRook;
				(*mv).indexOfTransformed = i;
				break;
			}
		}
		this->blackCastleAlowed = Made;
	}
	else if ((*mv).from == E8 && (*mv).to == C8) {
		//queen side casteling move
		// need to move rook also
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (this->blackPiecesList[3][i] == A8) {
				this->blackPiecesList[3][i] = D8;
				this->pieces[A8] = EMPTY;
				this->pieces[D8] = blackRook;
				(*mv).indexOfTransformed = i;
				break;
			}
		}
		this->blackCastleAlowed = Made;
	}

	/*else {
		std::cout << "Error Uknknown castelling move" << std::endl;
	}*/
	/*if ((*mv).indexOfTransformed < 0 || (*mv).indexOfTransformed > 9) {
		std::cout << "Error Index" << std::endl;
	}*/

	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
}

void Board::undoCastelingMove(Move *mv) {
	//printBoard(pieces);
	/*if ((*mv).indexOfTransformed < 0 || 9 < (*mv).indexOfTransformed) {
		std::cout << "ERROR : Transformed should contain index" <<std::endl;
	}
	if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/

	pieces[(*mv).from] = pieces[(*mv).to];
	pieces[(*mv).to] = EMPTY;
	bool white = (pieces[(*mv).from] < BLACK_OFFSET);
	// update pieces list
	if (white) {
		whitePiecesList[5][0] = (*mv).from;
	}
	else {
		blackPiecesList[5][0] = (*mv).from;
	}
	//rook
	if ((*mv).from == E1 && (*mv).to == G1) {
		pieces[F1] = EMPTY;
		pieces[H1] = whiteRook;
		whitePiecesList[3][(*mv).indexOfTransformed] = H1;
	}
	else if ((*mv).from == E1 && (*mv).to == C1) {
		pieces[D1] = EMPTY;
		pieces[A1] = whiteRook;
		whitePiecesList[3][(*mv).indexOfTransformed] = A1;
	}
	else if ((*mv).from == E8 && (*mv).to == G8) {
		pieces[F8] = EMPTY;
		pieces[H8] = blackRook;
		blackPiecesList[3][(*mv).indexOfTransformed] = H8;
	}
	else if ((*mv).from == E8 && (*mv).to == C8) {
		pieces[D8] = EMPTY;
		pieces[A8] = blackRook;
		blackPiecesList[3][(*mv).indexOfTransformed] = A8;
	}
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		printBoard(pieces);
		std::cout << "Blah " << std::endl;
	}*/
}

void Board::doMove(Move *mv) {
	char movedPiece = pieces[(*mv).from];
	bool white = (movedPiece < BLACK_OFFSET);
	(*mv).taken = pieces[(*mv).to];
	pieces[(*mv).to] = movedPiece;
	pieces[(*mv).from] = EMPTY;
	//remove piece from piece list
	if (white) {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (whitePiecesList[movedPiece - 1][i] == (*mv).from) {
				whitePiecesList[movedPiece - 1][i] = (*mv).to;
				(*mv).indexOfMoved = i;
				break;
			}
		}
	}
	else {
		for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
			if (blackPiecesList[movedPiece - BLACK_OFFSET][i] == (*mv).from) {
				blackPiecesList[movedPiece - BLACK_OFFSET][i] = (*mv).to;
				(*mv).indexOfMoved = i;
				break;
			}
		}
	}

	//remove taken
	if ((*mv).taken != EMPTY) {
		if (white) {
			for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
				if (blackPiecesList[(*mv).taken - BLACK_OFFSET][i] == (*mv).to) {
					blackPiecesList[(*mv).taken - BLACK_OFFSET][i] = NONE;
					(*mv).indexOfTaken = i;
					break;
				}
			}
		}
		else {
			for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
				if (whitePiecesList[(*mv).taken - 1][i] == (*mv).to) {
					whitePiecesList[(*mv).taken - 1][i] = NONE;
					(*mv).indexOfTaken = i;
					break;
				}
			}
		}
	}
	
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
}

void Board::undoMove(Move *mv) {
	
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		printBoard(pieces);
		std::cout << "Blah " << std::endl;
		std::cout << "Blah " << std::endl;
	}*/
	char movedPiece = pieces[(*mv).to];
	bool white = (movedPiece < BLACK_OFFSET);
	pieces[(*mv).from] = movedPiece;
	//change piece list
	if (white) {
		whitePiecesList[movedPiece - 1][(*mv).indexOfMoved] = (*mv).from;
	}
	else {
		blackPiecesList[movedPiece - BLACK_OFFSET][(*mv).indexOfMoved] = (*mv).from;
	}

	pieces[(*mv).to] = (*mv).taken;
	if ((*mv).taken != EMPTY) {
		if (!white) {
			whitePiecesList[(*mv).taken - 1][(*mv).indexOfTaken] = (*mv).to;
		}
		else {
			blackPiecesList[(*mv).taken - BLACK_OFFSET][(*mv).indexOfTaken] = (*mv).to;
		}
	}
	/*if (!checkMove((*mv), pieces)) {
		std::cout << "Move Error " << std::endl;
		printMove((*mv));
	}
	if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		printBoard(pieces);
		std::cout << "Blah " << std::endl;
		std::cout << "Blah " << std::endl;
		printMove((*mv));
	}*/
}

void Board::updateCastelingOptions(Move *mv) {
	if (whiteCastleAlowed != NO && whiteCastleAlowed != Made) {
		if (pieces[(*mv).to] == whiteKing) {
			this->whiteCastleAlowed = NO;
		}
		else if (pieces[(*mv).to] == whiteRook) {
			if ((*mv).from == A1) {
				if (this->whiteCastleAlowed == BothSides) {
					this->whiteCastleAlowed = KingSide;
				}
				else if (this->whiteCastleAlowed == QueenSide) {
					this->whiteCastleAlowed = NO;
				}
			}
			else if ((*mv).from == H1) {
				if (this->whiteCastleAlowed == BothSides) {
					this->whiteCastleAlowed = QueenSide;
				}
				else if (this->whiteCastleAlowed == KingSide) {
					this->whiteCastleAlowed = NO;
				}
			}
		}
	}

	// castleOptions (Black)
	if (blackCastleAlowed != NO && blackCastleAlowed != Made) {
		if (pieces[(*mv).to] == blackKing) {
			this->blackCastleAlowed = NO;
		}
		else if (pieces[(*mv).to] == blackRook) {
			if ((*mv).from == A8) {
				if (this->blackCastleAlowed == BothSides) {
					this->blackCastleAlowed = KingSide;
				}
				else if (this->blackCastleAlowed == QueenSide) {
					this->blackCastleAlowed = NO;
				}
			}
			else if ((*mv).from == H8) {
				if (this->blackCastleAlowed == BothSides) {
					this->blackCastleAlowed = QueenSide;
				}
				else if (this->blackCastleAlowed == KingSide) {
					this->blackCastleAlowed = NO;
				}
			}
		}
	}
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
}


int Board::PlayMove(Move* mv) {
	this->playCount++;
	ply++;
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
	(*mv).numberOfMovesWithoutTake = numberOfMovesWithoutTake;
	//bool pieceIsKing = pieces[mv.from] == whiteKing || pieces[mv.from] == blackKing;
	/*bool castellingMove = (mv.from == E1 && mv.to == G1) ||
		(mv.from == E1 && mv.to == C1) ||
		(mv.from == E8 && mv.to == G8) ||
		(mv.from == E8 && mv.to == C8);*/
	// Store the current value of castle ( needed for undo move)
	(*mv).currentCastleWhite = this->whiteCastleAlowed;
	(*mv).currentCastleBlack = this->blackCastleAlowed;
	//this->whiteOnMove = (pieces[mv.from] >= BLACK_OFFSET); // if black piece set white is on move to true

	//if (mv.to == this->enPassant && (pieces[mv.from] == whitePawn || pieces[mv.from] == blackPawn)) {
	if ((*mv).isEnPassantMove) {
		//printMove(mv);
		//printBoard(pieces);
		doEnPassantMove(mv);
	}
	else if ((*mv).transforms != EMPTY) {
		doTransformationMove(mv);
		//this->figureScore -= pieceValue[mv->transforms]; // figure is added to player score that is why -
		if (mv->transforms < BLACK_OFFSET) {
			//this->figureScore += pieceValue[whitePawn];
			this->sumOfWhitePieces -= pieceValue[mv->transforms] - pieceValue[whitePawn];
		}
		else {
			//this->figureScore += pieceValue[blackPawn];
			this->sumOfBlackPieces += pieceValue[mv->transforms] - pieceValue[blackPawn];
		}
		
	}
	else if ((*mv).isCastelingMove){//(pieceIsKing && castellingMove) {
		//printMove(mv);
		doCastelingMove(mv);
	}
	else {
		doMove(mv);
	}
	//this->figureScore += pieceValue[mv->taken];
	if (pieces[mv->to] < BLACK_OFFSET) {
		this->sumOfBlackPieces -= pieceValue[mv->taken]; // += because value of black pieces is negative
	}
	else {
		this->sumOfWhitePieces += pieceValue[mv->taken];
	}
	if ((*mv).taken == EMPTY && pieces[(*mv).to] != whitePawn && pieces[(*mv).to] != blackPawn) {
		numberOfMovesWithoutTake++;
	}
	else {
		numberOfMovesWithoutTake = 0;
	}

	updateCastelingOptions(mv);
	//this->key = updateKey(this, (*mv));
	this->enPassant = (*mv).enPassant;

	//this->whiteOnMove = !this->whiteOnMove; // change who is on move
	//printMove(mv);
	history.pushMove((*mv));// add move to history
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
	bool whiteMoved = pieces[mv->to] < BLACK_OFFSET;
	if (whiteMoved && isCheck(whitePiecesList[5][0], pieces)) {
		return CHECK_AFTER_MOVE;
	}
	else if (!whiteMoved && isCheck(blackPiecesList[5][0], pieces)) {
		return CHECK_AFTER_MOVE;
	}
	return true;
}
void Board::UndoMove() {
	//TODO DEBUG REMOVE
	this->undoCount++;
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
	Move last;
	if (history.empty()) {
		// nothing to undo
		std::cout << "Error empty history " << std::endl;
		return;
	}
	last = history.popMove();
	/*this->lastUndid[1] = lastUndid[0];
	this->lastUndid[0] = last;*/
	//this->key = last.keyBeforeMove;
	this->numberOfMovesWithoutTake = last.numberOfMovesWithoutTake;

	char movedPiece = pieces[last.to];
	char pawnLeftOffset = (movedPiece < BLACK_OFFSET) ? PAWN_THREATHENS_LEFT : PAWN_THREATHENS_LEFT * -1;
	char pawnRightOffset = (movedPiece < BLACK_OFFSET) ? PAWN_THREATHENS_RIGHT : PAWN_THREATHENS_RIGHT * -1;
	char pawn = (movedPiece < BLACK_OFFSET) ? whitePawn : blackPawn;
	/*bool pieceIsKing = pieces[last.to] == whiteKing || pieces[last.to] == blackKing;
	bool castellingMove = (last.from == E1 && last.to == G1) ||
		(last.from == E1 && last.to == C1) ||
		(last.from == E8 && last.to == G8) ||
		(last.from == E8 && last.to == C8);*/

	this->enPassant = NONE;
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
	//this->figureScore -= pieceValue[last.taken];
	if (movedPiece < BLACK_OFFSET) {
		this->sumOfBlackPieces += pieceValue[last.taken]; // += because value of black pieces is negative
	}
	else {
		this->sumOfWhitePieces -= pieceValue[last.taken];
	}
	if (last.transforms != EMPTY) {
		undoTransformationMove(&last);
		//this->figureScore += pieceValue[last.transforms]; // figure was added to player score, that is why +
		if (last.transforms < BLACK_OFFSET) {
			//this->figureScore -= pieceValue[whitePawn];
			this->sumOfWhitePieces += pieceValue[last.transforms] - pieceValue[whitePawn];
		}
		else {
			//this->figureScore -= pieceValue[blackPawn];
			this->sumOfBlackPieces -= pieceValue[last.transforms] - pieceValue[blackPawn];
		}
	}
	//else if (((last.to == last.from + pawnLeftOffset && last.taken == EMPTY) ||
	//	(last.to == last.from + pawnRightOffset && last.taken == EMPTY)) && (movedPiece == pawn)) {
	else if (last.isEnPassantMove) {
		//printMove(last);
		//std::cout << (int)enPassant << std::endl;
		undoEnPassantMove(&last);
		this->enPassant = last.to;
	}
	else if (last.isCastelingMove){//(pieceIsKing && castellingMove) {
		//printMove(last);
		undoCastelingMove(&last);
	}
	else {
		undoMove(&last);
	}
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
	//this->whiteOnMove = !whiteOnMove;

	whiteCastleAlowed = last.currentCastleWhite;
	blackCastleAlowed = last.currentCastleBlack;
	
	//switchWhiteOnMove();
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
}

void Board::GenerateMoves(MoveList* result) {
	if (whiteOnMove) {
		//printBoard(pieces);
		GeneratePawnMoves(result, pieces, whitePiecesList[0], true, enPassant);
		GenerateKnightMoves(result, pieces, whitePiecesList[1], true);
		GenerateBishopMoves(result, pieces, whitePiecesList[2], true);
		GenerateRookMoves(result, pieces, whitePiecesList[3], true);
		GenerateQueenMoves(result, pieces, whitePiecesList[4], true);
		GenerateKingMoves(result, pieces, whitePiecesList[5][0], true);

		if (whiteCastleAlowed != NO && whiteCastleAlowed != Made) {
			GenerateCastelingMoves(result, pieces, whitePiecesList[5][0], true, blackPiecesList[5][0], blackPiecesList, whiteCastleAlowed);
		}

	}
	else {
		GeneratePawnMoves(result, pieces, blackPiecesList[0], false, enPassant );
		GenerateKnightMoves(result, pieces, blackPiecesList[1], false);
		GenerateBishopMoves(result, pieces, blackPiecesList[2], false);
		GenerateRookMoves(result, pieces, blackPiecesList[3], false);
		GenerateQueenMoves(result, pieces, blackPiecesList[4], false);
		GenerateKingMoves(result, pieces, blackPiecesList[5][0], false);
		if (blackCastleAlowed != NO && blackCastleAlowed != Made) {
			GenerateCastelingMoves(result, pieces, blackPiecesList[5][0], false, whitePiecesList[5][0], whitePiecesList, blackCastleAlowed);
		}
	}
	
	/*for (int i = 0; i < (*result).length; i++) {
		if (killerMoves[0][ply+1].from == result->moves[i].from
			&& killerMoves[0][ply].to == result->moves[i].to) {
			result->moves[i].score = 90;
		}else if (killerMoves[1][ply + 1].from == result->moves[i].from
			&& killerMoves[1][ply].to == result->moves[i].to) {
			result->moves[i].score = 80;
		}
	}*/
	/*
	if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
	//switchWhiteOnMove();
	//return result;
}

void Board::GenerateCapturingMoves(MoveList * result) {
	if (whiteOnMove) {
		//printBoard(pieces);
		GeneratePawnCaptureMoves(result, pieces, whitePiecesList[0], true, enPassant);
		GenerateKnightCaptureMoves(result, pieces, whitePiecesList[1], true);
		GenerateBishopCaptureMoves(result, pieces, whitePiecesList[2], true);
		GenerateRookCaptureMoves(result, pieces, whitePiecesList[3], true);
		GenerateQueenCaptureMoves(result, pieces, whitePiecesList[4], true);
		GenerateKingCaptureMoves(result, pieces, whitePiecesList[5][0], true);
	}
	else {
		GeneratePawnCaptureMoves(result, pieces, blackPiecesList[0], false, enPassant);
		GenerateKnightCaptureMoves(result, pieces, blackPiecesList[1], false);
		GenerateBishopCaptureMoves(result, pieces, blackPiecesList[2], false);
		GenerateRookCaptureMoves(result, pieces, blackPiecesList[3], false);
		GenerateQueenCaptureMoves(result, pieces, blackPiecesList[4], false);
		GenerateKingCaptureMoves(result, pieces, blackPiecesList[5][0], false);
	}
	return;
}

bool Board::isRepeating(unsigned long long key) { 
	int numberOfRepetitions = 0;
	//unsigned long long hash = history.moves[history.length - 1].keyBeforeMove;a
	for (int i = history.length - 2; i >= 0; i = i - 2) {
		Move mv = history.moves[i];
		if (mv.taken == EMPTY && mv.enPassant == NONE && !mv.isCastelingMove) {
			if (mv.keyBeforeMove == key) {
				/*numberOfRepetitions++;
				if (numberOfRepetitions > 1) {*/
					return true;
				//}
			}
		}
		else {
			return false;
		}
	}
	return false;
}

bool Board::FiftyWithoutTake() {
	return numberOfMovesWithoutTake > 50;
}

bool Board::isDraw() {
	int WKnightCount = 0;
	int WBishopCount = 0;
	int BKnightCount = 0;
	int BBishopCount = 0;
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (whitePiecesList[i][j] != NONE) {
				if (i == 1) {
					WKnightCount++;
					if (WKnightCount == 2) {
						return false;
					}
				}
				else if (i == 2) {
					WBishopCount++;
					if (WBishopCount == 2) {
						return false;
					}
				}
				else {
					return false;
				}
			}
			if (blackPiecesList[i][j] != NONE) {
				if (i == 1) {
					BKnightCount++;
					if (BKnightCount == 2) {
						return false;
					}
				}
				else if (i == 2) {
					BBishopCount++;
					if (BBishopCount == 2) {
						return false;
					}
				}
				else {
					return false;
				}
			}
		}
	}
	return true;
}

void Board::setPieces(char pieces[CHESSBOARD_LENGTH]) {
	for (int i = 0; i < 120; i++) {
		this->pieces[i] = pieces[i];
	}
}
void Board::setWhitePieceList(char whitePL[6][MAX_NUMBER_OF_PIECES]) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			this->whitePiecesList[i][j] = whitePL[i][j];
		}
	}
}
void Board::setBlackPieceList(char blackPL[6][MAX_NUMBER_OF_PIECES]) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			this->blackPiecesList[i][j] = blackPL[i][j];
		}
	}
}
/*
void Board::switchWhiteOnMove() {
	this->whiteOnMove = !whiteOnMove;
}*/
char* Board::getPieces() {
	return this->pieces;
}
char squareTo120(char sq64) {
	int result = A1;
	result += (sq64 / 8) * 10;
	result += sq64 % 8;
	return result;
}
void Board::SetFenPosition(const char* input) {
	int  rank = 7;
	int  file = 0;
	int  piece = 0;
	int  count = 0;
	int  i = 0;
	int  sq64 = 0;
	int  sq120 = 0;

	for (int i = A1; i < H8; i++) {
		if ((!(i % 10 == 0 || i % 10 == 9))) {
			this->pieces[i] = EMPTY;
		}
	}

	while ((rank >= 0) && *input) {
		count = 1;
		switch (*input) {
		case 'p': piece = blackPawn; break;
		case 'r': piece = blackRook; break;
		case 'n': piece = blackKnight; break;
		case 'b': piece = blackBishop; break;
		case 'k': piece = blackKing; break;
		case 'q': piece = blackQueen; break;
		case 'P': piece = whitePawn; break;
		case 'R': piece = whiteRook; break;
		case 'N': piece = whiteKnight; break;
		case 'B': piece = whiteBishop; break;
		case 'K': piece = whiteKing; break;
		case 'Q': piece = whiteQueen; break;

		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
			piece = EMPTY;
			count = *input - '0';
			break;

		case '/':
		case ' ':
			rank--;
			file = 0;
			input++;
			continue;

		default:
			std::cout << "FEN error " << std::endl;
			return;
		}

		for (i = 0; i < count; i++) {
			sq64 = rank * 8 + file;
			sq120 = squareTo120(sq64);
			if (piece != EMPTY) {
				this->pieces[sq120] = piece;
			}
			file++;
		}
		input++;
	}

	this->whiteOnMove = (*input == 'w');
	input += 2;
	bool wk = false, wq = false, bk = false, bq = false;
	for (i = 0; i < 4; i++) {
		if (*input == ' ') {
			if (wk) {
				this->whiteCastleAlowed = (wq) ? BothSides : KingSide;
			}
			else if (wq) {
				this->whiteCastleAlowed = QueenSide;
			}
			else {
				this->whiteCastleAlowed = NO;
			}

			if (bk) {
				this->blackCastleAlowed = (bq) ? BothSides : KingSide;
			}
			else if (bq) {
				this->blackCastleAlowed = QueenSide;
			}
			else {
				this->blackCastleAlowed = NO;
			}
			break;
		}
		
		switch (*input) {
		case 'K': wk = true; break;
		case 'Q': wq = true; break;
		case 'k': bk = true; break;
		case 'q': bq = true; break;
		default:	     break;
		}
		input++;
	}
	input++;

	if (*input != '-') {
		file = input[0] - 'a';
		rank = input[1] - '1';
		this->enPassant = squareTo120(rank * 8 + file);
	}

	//this->key = simpleHash(this);

	for (int i = A1; i < H8; i++) {
		if ((!(i % 10 == 0 || i % 10 == 9))) {
			char piece = this->pieces[i];
			if (piece == 0) {
				continue;
			}
			if (piece < BLACK_OFFSET) {
				for(int j = 0; j < 10; j++){
					if (this->whitePiecesList[piece - 1][j] == NONE) {
						this->whitePiecesList[piece - 1][j] = i;
						break;
					}
				}
			}
			else {
				for (int j = 0; j < 10; j++) {
					if (this->blackPiecesList[piece - BLACK_OFFSET][j] == NONE) {
						this->blackPiecesList[piece - BLACK_OFFSET][j] = i;
						break;
					}
				}
			}
		}
	}

	return;
}