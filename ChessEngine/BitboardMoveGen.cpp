#include "stdafx.h"
#include "BitboardMoveGen.h"
#include "BitboardDebug.h"
#include "Bitset"

U64 BLACK_PAWNS_INIT_POSSITION = 0b0000000011111111000000000000000000000000000000000000000000000000;
U64 WHITE_PAWNS_INIT_POSSITION = 0b0000000000000000000000000000000000000000000000001111111100000000;
U64 NOT_H_FILE = ~((1ULL << h1) |
(1ULL << h2) |
(1ULL << h3) |
(1ULL << h4) |
(1ULL << h5) |
(1ULL << h6) |
(1ULL << h7) |
(1ULL << h8));
U64 NOT_A_FILE = ~((1ULL << a1) |
(1ULL << a2) |
(1ULL << a3) |
(1ULL << a4) |
(1ULL << a5) |
(1ULL << a6) |
(1ULL << a7) |
(1ULL << a8));
U64 ROW_8 = 0b1111111100000000000000000000000000000000000000000000000000000000;
U64 ROW_1 = 0b0000000000000000000000000000000000000000000000000000000011111111;
const short value[] = { 100,300,300,500,900,900,6000,100,300,300,500,900,6000 };
const short PAWN_VAL = 9;
const short BISHOP_VAL = 29;
const short KNIGHT_VAL = 29;
const short ROOK_VAL = 49;
const short QUEEN_VAL = 89;
const short KING_VAL = 89;
/*char BitTable[64] = {
  63, 30, 3, 32, 25, 41, 22, 33, 15, 50, 42, 13, 11, 53, 19, 34, 61, 29, 2,
  51, 21, 43, 45, 10, 18, 47, 1, 54, 9, 57, 0, 35, 62, 31, 40, 4, 49, 5, 52,
  26, 60, 6, 23, 44, 46, 27, 56, 16, 7, 39, 48, 24, 59, 14, 12, 55, 38, 28,
  58, 20, 37, 17, 36, 8
};*/
char POP(U64 *bb) {
	U64 b = *bb ^ (*bb - 1);
	unsigned int fold = (unsigned)((b & 0xffffffff) ^ (b >> 32));
	*bb &= (*bb - 1);
	return BitTable[(fold * 0x783a9b23) >> 26];
}

char COUNT(U64 b) {
	int r;
	for (r = 0; b; r++, b &= b - 1);
	return r;
}
// << UP
// >> DOWN
// TODO enPassant is now U64 but in the struct it is still char
void CountPawnMoves(short* moves, U64 pawns, U64 myPieces, U64 opponentPieces, bool white, U64 enPassant, U64 opponentPieceList[]) {
	if (white) {
		U64 advance = (pawns << UP) & (~opponentPieces) & (~myPieces);
		U64 transforms = advance & ROW_8;
		advance &= ~ROW_8;
		*moves = *moves + COUNT(advance);
		/*while (advance) {
			char to = POP(&advance);
			mv.from = to - UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.taken = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}*/
		// on the 8th row so it transforms
		*moves = *moves + COUNT(transforms) * 4;
		/*while (transforms) {
			char to = POP(&transforms);
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] - PAWN_VAL;
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}*/
		U64 left = ((pawns & NOT_H_FILE) << LEFT_UP) & (opponentPieces | enPassant);
		transforms = left & ROW_8;
		*moves = *moves + COUNT(left);
		/*while (left) {
			char to = POP(&left);
			mv.from = to - LEFT_UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}*/
		*moves = *moves + COUNT(transforms) * 4;
		// on the 8th row so it transforms
		/*while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - LEFT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = score + value[i];
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}*/

		U64 right = ((pawns & NOT_A_FILE) << RIGHT_UP) & (opponentPieces | enPassant);
		transforms = right & ROW_8;
		*moves = *moves + COUNT(right);
		/*while (right) {
			char to = POP(&right);
			mv.from = to - RIGHT_UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}*/
		*moves = *moves + COUNT(transforms);
		// on the 8th row so it transforms
		/*while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - RIGHT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = score + value[i];
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}*/
		U64 advance2 = ((((pawns & WHITE_PAWNS_INIT_POSSITION) << UP) & (~opponentPieces) & (~myPieces)) << UP) & (~opponentPieces) & (~myPieces);
		*moves = *moves + COUNT(advance2);
		/*while (advance2) {
			char to = POP(&advance2);
			mv.from = to - UP * 2;
			mv.to = to;
			mv.enPassant = 1ULL << (to - UP);
			mv.transforms = EMPTY;
			mv.taken = 0;
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}*/
		/*printBitboard(opponentPieces);
		std::cout << "ORIGINAL" << std::endl;
		printBitboard(pawns);
		std::cout << "ADVANCE" << std::endl;
		printBitboard(advance);
		std::cout << "ADVANCE BY TWO" << std::endl;
		printBitboard(advance2);
		std::cout << "LEFT" << std::endl;
		printBitboard(left);
		std::cout << "RIGHT" << std::endl;
		printBitboard(right);*/
	}
	else {
		U64 advance = ((pawns >> UP) & (~opponentPieces)) & (~myPieces);
		U64 left = ((pawns & NOT_A_FILE) >> LEFT_UP) & (opponentPieces | enPassant);
		U64 right = ((pawns & NOT_H_FILE) >> RIGHT_UP) & (opponentPieces | enPassant);
		U64 advance2 = ((((pawns & BLACK_PAWNS_INIT_POSSITION) >> UP) & (~opponentPieces) & (~myPieces)) >> UP) & (~opponentPieces) & (~myPieces);
		U64 transforms = advance & ROW_1;
		*moves = *moves + COUNT(advance);
		*moves = *moves + COUNT(left);
		*moves = *moves + COUNT(right);
		*moves = *moves + COUNT(advance2);
		*moves = *moves + COUNT(transforms) * 4;
		transforms = left & ROW_1;
		*moves = *moves + COUNT(transforms) * 4;
		transforms = right & ROW_1;
		*moves = *moves + COUNT(transforms) * 4;
	}
}

void CountBishopMoves(short* moves, U64 bishops, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	//Generating bishop moves = 
	//Take offsets, generate moves until you don't reach your piece, your opponent piece, the end of the board
	//Work with all of them at the same time
	//LEFT_UP
	U64 temp = bishops;
	U64 attacks;
	//U64 newPossition;
	char i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) & (~myPieces));
		//attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		//newPossition = temp;
	}
	//*moves = *moves + i;
	//RIGHT_UP
	temp = bishops;
	//i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP) & (~myPieces));
		//attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
	}
	//*moves = *moves + i;
	//LEFT DOWN
	temp = bishops;
	//i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) & (~myPieces));
		//attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		
	}
	//*moves = *moves + i;
	//RIGHT_DOWN
	temp = bishops;
	//i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP) & (~myPieces));
		//attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
	}
	*moves = *moves + i;
}
// knight move offset table
U64 KnightOffsets[64] = {
	(1ULL << b3) | (1ULL << c2), //a1
	(1ULL << c3) | (1ULL << d2) | (1ULL << a3), //b1
	(1ULL << d3) | (1ULL << e2) | (1ULL << a2) | (1ULL << b3), //c1
	(1ULL << b2) | (1ULL << c3) | (1ULL << e3) | (1ULL << f2), //d1
	(1ULL << c2) | (1ULL << d3) | (1ULL << f3) | (1ULL << g2), //e1
	(1ULL << d2) | (1ULL << e3) | (1ULL << g3) | (1ULL << h2), //f1
	(1ULL << e2) | (1ULL << f3) | (1ULL << h3), //g1
	(1ULL << f2) | (1ULL << g3), //h1

	(1ULL << c1) | (1ULL << b4) | (1ULL << c3), //a2
	(1ULL << d1) | (1ULL << c4) | (1ULL << d3) | (1ULL << a4), //b2
	(1ULL << a1) | (1ULL << e1) | (1ULL << d4) | (1ULL << e3) | (1ULL << a3) | (1ULL << b4), //c2
	(1ULL << b1) | (1ULL << f1) | (1ULL << b3) | (1ULL << c4) | (1ULL << e4) | (1ULL << f3), //d2
	(1ULL << c1) | (1ULL << g1) | (1ULL << c3) | (1ULL << d4) | (1ULL << f4) | (1ULL << g3), //e2
	(1ULL << d1) | (1ULL << h1) | (1ULL << d3) | (1ULL << e4) | (1ULL << g4) | (1ULL << h3), //f2
	(1ULL << e1) | (1ULL << e3) | (1ULL << f4) | (1ULL << h4), //g2
	(1ULL << f1) | (1ULL << f3) | (1ULL << g4), //h2

	(1ULL << b1) | (1ULL << c2) | (1ULL << b5) | (1ULL << c4), //a3
	(1ULL << a1) | (1ULL << c1) | (1ULL << d2) | (1ULL << c5) | (1ULL << d4) | (1ULL << a5), //b3
	(1ULL << b1) | (1ULL << d1) | (1ULL << a2) | (1ULL << e2) | (1ULL << d5) | (1ULL << e4) | (1ULL << a4) | (1ULL << b5), //c3
	(1ULL << c1) | (1ULL << e1) | (1ULL << b2) | (1ULL << f2) | (1ULL << b4) | (1ULL << c5) | (1ULL << e5) | (1ULL << f4), //d3
	(1ULL << d1) | (1ULL << f1) | (1ULL << c2) | (1ULL << g2) | (1ULL << c4) | (1ULL << d5) | (1ULL << f5) | (1ULL << g4), //e3
	(1ULL << e1) | (1ULL << g1) | (1ULL << d2) | (1ULL << h2) | (1ULL << d4) | (1ULL << e5) | (1ULL << g5) | (1ULL << h4), //f3
	(1ULL << f1) | (1ULL << h1) | (1ULL << e2) | (1ULL << e4) | (1ULL << f5) | (1ULL << h5), //g3
	(1ULL << g1) | (1ULL << f2) | (1ULL << f4) | (1ULL << g5), //h3

	(1ULL << b2) | (1ULL << c3) | (1ULL << b6) | (1ULL << c5), //a4
	(1ULL << a2) | (1ULL << c2) | (1ULL << d3) | (1ULL << c6) | (1ULL << d5) | (1ULL << a6), //b4
	(1ULL << b2) | (1ULL << d2) | (1ULL << a3) | (1ULL << e3) | (1ULL << d6) | (1ULL << e5) | (1ULL << a5) | (1ULL << b6), //c4
	(1ULL << c2) | (1ULL << e2) | (1ULL << b3) | (1ULL << f3) | (1ULL << b5) | (1ULL << c6) | (1ULL << e6) | (1ULL << f5), //d4
	(1ULL << d2) | (1ULL << f2) | (1ULL << c3) | (1ULL << g3) | (1ULL << c5) | (1ULL << d6) | (1ULL << f6) | (1ULL << g5), //e4
	(1ULL << e2) | (1ULL << g2) | (1ULL << d3) | (1ULL << h3) | (1ULL << d5) | (1ULL << e6) | (1ULL << g6) | (1ULL << h5), //f4
	(1ULL << f2) | (1ULL << h2) | (1ULL << e3) | (1ULL << e5) | (1ULL << f6) | (1ULL << h6), //g4
	(1ULL << g2) | (1ULL << f3) | (1ULL << f5) | (1ULL << g6), //h4

	(1ULL << b3) | (1ULL << c4) | (1ULL << b7) | (1ULL << c6), //a5
	(1ULL << a3) | (1ULL << c3) | (1ULL << d4) | (1ULL << c7) | (1ULL << d6) | (1ULL << a7), //b5
	(1ULL << b3) | (1ULL << d3) | (1ULL << a4) | (1ULL << e4) | (1ULL << d7) | (1ULL << e6) | (1ULL << a6) | (1ULL << b7), //c5
	(1ULL << c3) | (1ULL << e3) | (1ULL << b4) | (1ULL << f4) | (1ULL << b6) | (1ULL << c7) | (1ULL << e7) | (1ULL << f6), //d5
	(1ULL << d3) | (1ULL << f3) | (1ULL << c4) | (1ULL << g4) | (1ULL << c6) | (1ULL << d7) | (1ULL << f7) | (1ULL << g6), //e5
	(1ULL << e3) | (1ULL << g3) | (1ULL << d4) | (1ULL << h4) | (1ULL << d6) | (1ULL << e7) | (1ULL << g7) | (1ULL << h6), //f5
	(1ULL << f3) | (1ULL << h3) | (1ULL << e4) | (1ULL << e6) | (1ULL << f7) | (1ULL << h7), //g5
	(1ULL << g3) | (1ULL << f4) | (1ULL << f6) | (1ULL << g7), //h5

	(1ULL << b4) | (1ULL << c5) | (1ULL << b8) | (1ULL << c7), //a6
	(1ULL << a4) | (1ULL << c4) | (1ULL << d5) | (1ULL << c8) | (1ULL << d7) | (1ULL << a8), //b6
	(1ULL << b4) | (1ULL << d4) | (1ULL << a5) | (1ULL << e5) | (1ULL << d8) | (1ULL << e7) | (1ULL << a7) | (1ULL << b8), //c6
	(1ULL << c4) | (1ULL << e4) | (1ULL << b5) | (1ULL << f5) | (1ULL << b7) | (1ULL << c8) | (1ULL << e8) | (1ULL << f7), //d6
	(1ULL << d4) | (1ULL << f4) | (1ULL << c5) | (1ULL << g5) | (1ULL << c7) | (1ULL << d8) | (1ULL << f8) | (1ULL << g7), //e6
	(1ULL << e4) | (1ULL << g4) | (1ULL << d5) | (1ULL << h5) | (1ULL << d7) | (1ULL << e8) | (1ULL << g8) | (1ULL << h7), //f6
	(1ULL << f4) | (1ULL << h4) | (1ULL << e5) | (1ULL << e7) | (1ULL << f8) | (1ULL << h8), //g6
	(1ULL << g4) | (1ULL << f5) | (1ULL << f7) | (1ULL << g8), //h6

	(1ULL << b5) | (1ULL << c6) | (1ULL << c8), //a7
	(1ULL << a5) | (1ULL << c5) | (1ULL << d6) | (1ULL << d8), //b7
	(1ULL << b5) | (1ULL << d5) | (1ULL << a6) | (1ULL << e6) | (1ULL << e8) | (1ULL << a8), //c7
	(1ULL << c5) | (1ULL << e5) | (1ULL << b6) | (1ULL << f6) | (1ULL << b8) | (1ULL << f8), //d7
	(1ULL << d5) | (1ULL << f5) | (1ULL << c6) | (1ULL << g6) | (1ULL << c8) | (1ULL << g8), //e7
	(1ULL << e5) | (1ULL << g5) | (1ULL << d6) | (1ULL << h6) | (1ULL << d8) | (1ULL << h8), //f7
	(1ULL << f5) | (1ULL << h5) | (1ULL << e6) | (1ULL << e8), //g7
	(1ULL << g5) | (1ULL << f6) | (1ULL << f8) , //h7

	(1ULL << b6) | (1ULL << c7), //a8
	(1ULL << a6) | (1ULL << c6) | (1ULL << d7), //b8
	(1ULL << b6) | (1ULL << d6) | (1ULL << a7) | (1ULL << e7) , //c8
	(1ULL << c6) | (1ULL << e6) | (1ULL << b7) | (1ULL << f7) , //d8
	(1ULL << d6) | (1ULL << f6) | (1ULL << c7) | (1ULL << g7) , //e8
	(1ULL << e6) | (1ULL << g6) | (1ULL << d7) | (1ULL << h7), //f8
	(1ULL << f6) | (1ULL << h6) | (1ULL << e7), //g8
	(1ULL << g6) | (1ULL << f7) //h8
};

void CountKnightMoves(short* moves, U64 knights, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	// use the knight move offset table
	U64 pos;
	while (knights) {
		char sq = POP(&knights);
		pos = KnightOffsets[sq];
		pos &= ~myPieces; // dont have a piece on the square
		*moves = *moves +COUNT(pos);
	}

}

void CountRookMoves(short* moves, U64 rooks, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	U64 temp = rooks;
	//U64 attack;
	//U64 pos;
	char i = 0;
	//Move mv;
	while (temp) {
		i++;
		//left
		temp = ((temp & NOT_A_FILE) >> LEFT) & (~myPieces);
		temp &= ~opponentPieces;
	}

	temp = rooks;
	//i = 0;
	while (temp) {
		i++;
		//right
		temp = ((temp & NOT_H_FILE) << LEFT) & (~myPieces);
		//attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		
	}

	temp = rooks;
	//i = 0;
	while (temp) {
		i++;
		//up
		temp = ((temp & (~ROW_8)) << UP) & (~myPieces);
		//attack = temp & opponentPieces;
		temp &= ~opponentPieces;
	}

	temp = rooks;
	//i = 0;
	while (temp) {
		i++;
		//down
		temp = ((temp & (~ROW_1)) >> UP) & (~myPieces);
		//attack = temp & opponentPieces;
		temp &= ~opponentPieces;
	}
	*moves = *moves + i;
}

void CountQueenMoves(short* moves, U64 queens, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	CountRookMoves(moves, queens, myPieces, opponentPieces, piece, opponentPieceList, base);
	CountBishopMoves(moves, queens, myPieces, opponentPieces, piece, opponentPieceList, base);
}

void PawnMoves(MoveList* moves, U64 pawns, U64 myPieces, U64 opponentPieces, bool white, U64 enPassant, U64 opponentPieceList[]){
	if (white) {
		U64 advance = (pawns << UP) & (~opponentPieces) & (~myPieces);
		U64 transforms = advance & ROW_8;
		advance &= ~ROW_8;
		Move mv;

		while (advance) {
			char to = POP(&advance);
			mv.from = to - UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.taken = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}
		// on the 8th row so it transforms
		while (transforms) {
			char to = POP(&transforms);
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] - PAWN_VAL;
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}
		U64 left = ((pawns & NOT_H_FILE) << LEFT_UP) & (opponentPieces | enPassant);
		transforms = left & ROW_8;

		while (left) {
			char to = POP(&left);
			mv.from = to - LEFT_UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}
		// on the 8th row so it transforms
		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - LEFT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i; 
				mv.score = score + value[i];
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}

		U64 right = ((pawns & NOT_A_FILE) << RIGHT_UP) & (opponentPieces | enPassant);
		transforms = right & ROW_8;

		while (right) {
			char to = POP(&right);
			mv.from = to - RIGHT_UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}
		// on the 8th row so it transforms
		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - RIGHT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = score + value[i];
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}
		U64 advance2 = ((((pawns & WHITE_PAWNS_INIT_POSSITION) << UP) & (~opponentPieces) & (~myPieces)) << UP) & (~opponentPieces) & (~myPieces);
		while (advance2) {
			char to = POP(&advance2);
			mv.from = to - UP * 2;
			mv.to = to;
			mv.enPassant = 1ULL << (to - UP);
			mv.transforms = EMPTY;
			mv.taken = 0;
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}
		/*printBitboard(opponentPieces);
		std::cout << "ORIGINAL" << std::endl;
		printBitboard(pawns);
		std::cout << "ADVANCE" << std::endl;
		printBitboard(advance);
		std::cout << "ADVANCE BY TWO" << std::endl;
		printBitboard(advance2);
		std::cout << "LEFT" << std::endl;
		printBitboard(left);
		std::cout << "RIGHT" << std::endl;
		printBitboard(right);*/
	}
	else {
		U64 advance = ((pawns >> UP) & (~opponentPieces)) & (~myPieces);
		U64 left = ((pawns & NOT_A_FILE) >> LEFT_UP) & (opponentPieces | enPassant);
		U64 right = ((pawns & NOT_H_FILE) >> RIGHT_UP) & (opponentPieces | enPassant);
		U64 advance2 = ((((pawns & BLACK_PAWNS_INIT_POSSITION) >> UP) & (~opponentPieces) & (~myPieces)) >> UP) & (~opponentPieces) & (~myPieces);
		U64 transforms = advance & ROW_1;
		/*
		printBitboard(opponentPieces);
		std::cout << "ORIGINAL" << std::endl;
		printBitboard(pawns);
		std::cout << "ADVANCE" << std::endl;
		printBitboard(advance);
		std::cout << "ADVANCE BY TWO" << std::endl;
		printBitboard(advance2);
		std::cout << "LEFT" << std::endl;
		printBitboard(left);
		std::cout << "RIGHT" << std::endl;
		printBitboard(right);*/
		advance &= ~ROW_1;
		Move mv;

		while (advance) {
			char to = POP(&advance);
			mv.from = to + UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.taken = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = blackPawn;
			(*moves).pushMove(mv);
		}

		while (transforms) {
			char to = POP(&transforms);
			for (int i = blackKnight; i < blackKing; i++) {
				mv.from = to + UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i; 
				mv.score = value[i] + 100;// TODO calculate value of taken piece
				mv.movedPiece = blackPawn;
				(*moves).pushMove(mv);
			}
		}

		transforms = left & ROW_1;
		left &= ~ROW_1;
		while (left) {
			char to = POP(&left);
			mv.from = to + LEFT_UP; // TODO check
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			mv.movedPiece = blackPawn;
			(*moves).pushMove(mv);
		}

		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			for (int i = blackKnight; i < blackKing; i++) {
				mv.from = to + LEFT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] + score;
				mv.movedPiece = blackPawn;
				(*moves).pushMove(mv);
			}
		}

		transforms = right & ROW_1;
		right &= ~ROW_1;
		while (right) {
			char to = POP(&right);
			mv.from = to + RIGHT_UP; // TODO check
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			mv.movedPiece = blackPawn;
			(*moves).pushMove(mv);
		}

		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			for (int i = blackKnight; i < blackKing; i++) {
				mv.from = to + RIGHT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] + score;
				mv.movedPiece = blackPawn;
				(*moves).pushMove(mv);
			}
		}

		while (advance2) {
			char to = POP(&advance2);
			mv.from = to + UP * 2;
			mv.to = to;
			mv.taken = 0;
			mv.enPassant = 1ULL << (to + UP);
			mv.transforms = EMPTY;
			mv.movedPiece = blackPawn;
			(*moves).pushMove(mv);
		}
		
	}

	
}

void BishopMoves(MoveList* moves, U64 bishops, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	//Generating bishop moves = 
	//Take offsets, generate moves until you don't reach your piece, your opponent piece, the end of the board
	//Work with all of them at the same time
	Move mv;
	//LEFT_UP
	U64 temp = bishops;
	U64 attacks;
	U64 newPossition;
	char i = 0;
	while(temp){
		i++;
		temp = ((((temp & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq - LEFT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		newPossition = temp;
		while (newPossition) {
			char sq = POP(&newPossition);
			mv.from = sq - LEFT_UP * i;
			mv.to = sq;
			mv.taken = 0;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	//RIGHT_UP
	temp = bishops;
	i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq - RIGHT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		newPossition = temp;
		while (newPossition) {
			char sq = POP(&newPossition);
			mv.from = sq - RIGHT_UP * i;
			mv.to = sq;
			mv.taken = 0;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	//LEFT DOWN
	temp = bishops;
	i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq + LEFT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		newPossition = temp;
		while (newPossition) {
			char sq = POP(&newPossition);
			mv.from = sq + LEFT_UP * i;
			mv.to = sq;
			mv.taken = 0;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	//RIGHT_DOWN
	temp = bishops;
	i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq + RIGHT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		newPossition = temp;
		while (newPossition) {
			char sq = POP(&newPossition);
			mv.from = sq + RIGHT_UP * i;
			mv.to = sq;
			mv.taken = 0;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}
}

void KnightMoves(MoveList* moves, U64 knights, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	// use the knight move offset table
	Move mv;
	U64 pos, attacks;
	while (knights) {
		char sq = POP(&knights);
		pos = KnightOffsets[sq];
		pos &= ~myPieces; // dont have a piece on the square
		attacks = pos & opponentPieces;
		pos &= ~opponentPieces;
		while (attacks) {
			char to = POP(&attacks);
			mv.from = sq;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score = value[i] - KNIGHT_VAL;
					mv.taken = base + i; // piece - 1 because knight and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		while (pos) {
			char to = POP(&pos);
			mv.from = sq;
			mv.to = to;
			mv.enPassant = 0;
			mv.taken = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

}

void RookMoves(MoveList* moves, U64 rooks, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	U64 temp = rooks;
	U64 attack;
	U64 pos;
	char i = 0;
	Move mv;
	while (temp) {
		i++;
		//left
		temp = ((temp & NOT_A_FILE) >> LEFT) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		pos = temp;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq + LEFT * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		while (pos) {
			char sq = POP(&pos);
			mv.from = sq + LEFT * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.taken = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	temp = rooks;
	i = 0;
	while (temp) {
		i++;
		//right
		temp = ((temp & NOT_H_FILE) << LEFT) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		pos = temp;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq - LEFT * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		while (pos) {
			char sq = POP(&pos);
			mv.from = sq - LEFT * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.taken = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	temp = rooks;
	i = 0;
	while (temp) {
		i++;
		//up
		temp = ((temp & (~ROW_8)) << UP) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		pos = temp;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq - UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		while (pos) {
			char sq = POP(&pos);
			mv.from = sq - UP * i;
			mv.to = sq;
			mv.taken = 0;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	temp = rooks;
	i = 0;
	while (temp) {
		i++;
		//down
		temp = ((temp & (~ROW_1)) >> UP) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		pos = temp;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq + UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
		while (pos) {
			char sq = POP(&pos);
			mv.from = sq + UP * i;
			mv.to = sq;
			mv.taken = 0;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}
}

void QueenMoves(MoveList* moves, U64 queens, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[],char base) {
	RookMoves(moves, queens, myPieces, opponentPieces, piece,opponentPieceList, base);
	BishopMoves(moves, queens, myPieces, opponentPieces, piece,opponentPieceList,base);
}

void KingMoves(MoveList* moves, U64 king, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {

	U64 pos = ((king & NOT_A_FILE) >> LEFT) | ((king & NOT_H_FILE) << LEFT) |
		((king & (~ROW_8)) << UP) | ((king & (~ROW_1)) >> UP);
	pos |= (((king & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) | (((king & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP);
	pos |= (((king & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) | (((king & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP);

	pos &= ~myPieces;
	U64 capture = pos & opponentPieces;
	pos &= ~opponentPieces;
	Move mv;
	char from = POP(&king);
	while (capture) {
		char sq = POP(&capture);
		mv.from = from;
		mv.to = sq;
		mv.enPassant = 0;
		mv.transforms = EMPTY;
		for (int i = 0; i < 5; i++) {
			if (opponentPieceList[i] & (1ULL << sq)) {
				mv.score = value[i] - KING_VAL;
				mv.taken = base + i; // piece - 5 because king and pawn
				break;
			}
		}
		mv.movedPiece = piece;
		(*moves).pushMove(mv);
	}
	while (pos) {
		char sq = POP(&pos);
		mv.from = from;
		mv.to = sq;
		mv.taken = 0;
		mv.enPassant = 0;
		mv.transforms = EMPTY;
		mv.movedPiece = piece;
		(*moves).pushMove(mv);
	}

}

void printBB(U64 b) {
	std::cout << std::endl;
	for (int i = 0; i < 8; i++) {
		char a = (char)b;
		std::cout << std::bitset<8>(a) << std::endl;
		b = b >> 8;
	}
	std::cout << std::endl;
}
//((sq & NOT_A_FILE) << LEFT) | ((king & NOT_H_FILE) >> LEFT) |
//((king & (~ROW_8)) << UP) | ((king & (~ROW_1)) >> UP)
bool Threathens(U64 sq, U64 opponentPieceList[], U64 myPieces, U64 opponentPieces, bool white, char square) {
	U64 pos = ((sq & NOT_A_FILE) >> LEFT) | ((sq & NOT_H_FILE) << LEFT) |
		((sq & (~ROW_8)) << UP) | ((sq & (~ROW_1)) >> UP);
	pos |= (((sq & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) | (((sq & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP);
	pos |= (((sq & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) | (((sq & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP);
	if (pos & opponentPieceList[5]) {
		return true;
	}
	// PAWN
	if (white) {
		pos = (((sq & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) | (((sq & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP);
	}
	else {
		pos = (((sq & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) | (((sq & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP); // left and right from white perspective
	}
	if (pos & opponentPieceList[0]) {
		return true;
	}
	//BISHOP & QUEEN
	// LEFT_UP
	U64 temp = sq;
	while (temp) {
		temp = ((((temp & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) & (~myPieces));
		if ((temp & opponentPieces)) { // bishop or queen
			if ((temp & opponentPieceList[2]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}
	// RIGHT UP
	temp = sq;
	while (temp) {
		temp = ((((temp & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP) & (~myPieces));
		if ((temp & opponentPieces)) { // bishop or queen
			if ((temp & opponentPieceList[2]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}
	//LEFT DOWN
	temp = sq;
	while (temp) {
		temp = ((((temp & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) & (~myPieces));
		if ((temp & opponentPieces)) { // bishop or queen
			if ((temp & opponentPieceList[2]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}
	//RIGHT DOWN
	temp = sq;
	while (temp) {
		temp = ((((temp & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP) & (~myPieces));
		if ((temp & opponentPieces)) { // bishop or queen
			if ((temp & opponentPieceList[2]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}

	//ROOK & QUEEN
	// UP
	temp = sq;
	while (temp) {
		temp = (((temp & (~ROW_8)) << UP) & (~myPieces));
		if ((temp & opponentPieces)) { // rook or queen
			if ((temp & opponentPieceList[3]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}
	// DOWN
	temp = sq;
	while (temp) {
		temp = (((temp & (~ROW_1)) >> UP) & (~myPieces));
		if ((temp & opponentPieces)) { // rook or queen
			if ((temp & opponentPieceList[3]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}
	// RIGHT
	temp = sq;
	while (temp) {
		temp = (((temp & NOT_H_FILE) << LEFT) & (~myPieces));
		if ((temp & opponentPieces)) { // rook or queen
			if ((temp & opponentPieceList[3]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}
	// LEFT
	temp = sq;
	while (temp) {
		temp = (((temp  & NOT_A_FILE) >> LEFT) & (~myPieces));
		if ((temp & opponentPieces)) { // rook or queen
			if ((temp & opponentPieceList[3]) || (temp & opponentPieceList[4])) {
				return true;
			}
			else {
				break;
			}
		}
	}

	//KNIGHT
	return KnightOffsets[square] & opponentPieceList[1];
}

void CastelingMoves(MoveList* moves, U64 king, U64 rooks, U64 myPieces, U64 opponentPieces, U64 opponentPieceList[], bool white, char castelingOptions) {
	Move mv;
	if (white) {
		if (king & (1ULL << e1)) {
			if (((castelingOptions == BothSides || castelingOptions == KingSide) && (rooks & (1ULL << h1)))
				&& (!((myPieces | opponentPieces) & ((1ULL << f1) | (1ULL << g1))))) {
				if (!Threathens(1ULL << e1, opponentPieceList, myPieces, opponentPieces, white, e1)) {
					//king side
					//check if opponent threathens f1
					if (!Threathens(1ULL << f1, opponentPieceList, myPieces, opponentPieces, white, f1)) {
						mv.from = e1;
						mv.to = g1;
						mv.enPassant = 0;
						mv.transforms = EMPTY;
						mv.isCastelingMove = true;
						mv.movedPiece = whiteKing;
						(*moves).pushMove(mv);
					}
				}
			}
			if (((castelingOptions == BothSides || castelingOptions == QueenSide) && (rooks & (1ULL << a1)))
				&& (!((myPieces | opponentPieces) & ((1ULL << d1) | (1ULL << c1) | (1ULL << b1))))) {
				if (!Threathens(1ULL << e1, opponentPieceList, myPieces, opponentPieces, white, e1)) {
					//queen side
					//check if opponent threathens d1, b1
					if (!(Threathens(1ULL << d1, opponentPieceList, myPieces, opponentPieces, white, d1) ||
						Threathens(1ULL << b1, opponentPieceList, myPieces, opponentPieces, white, b1))) {
						mv.from = e1;
						mv.to = c1;
						mv.enPassant = 0;
						mv.transforms = EMPTY;
						mv.isCastelingMove = true;
						mv.movedPiece = whiteKing;
						(*moves).pushMove(mv);
					}
				}
			}
		}
	}
	else {
		if (king & (1ULL << e8)) {
			if (((castelingOptions == BothSides || castelingOptions == KingSide) && (rooks & (1ULL << h8)))
				&& (!((myPieces | opponentPieces) & ((1ULL << f8) | (1ULL << g8))))) {
				if (!Threathens(1ULL << e8, opponentPieceList, myPieces, opponentPieces, white, e8)) {
					//king side
					//check if opponent threathens f8
					if (!Threathens(1ULL << f8, opponentPieceList, myPieces, opponentPieces, white, f8)) {
						mv.from = e8;
						mv.to = g8;
						mv.enPassant = 0;
						mv.transforms = EMPTY;
						mv.isCastelingMove = true;
						mv.movedPiece = blackKing;
						(*moves).pushMove(mv);
					}
				}
			}
			if (((castelingOptions == BothSides || castelingOptions == QueenSide) && (rooks & (1ULL << a8)))
				&& (!((myPieces | opponentPieces) & ((1ULL << d8) | (1ULL << c8) | (1ULL << b8))))) {
				//queen side
				//check if opponent threathens d1, b1
				if (!Threathens(1ULL << e8, opponentPieceList, myPieces, opponentPieces, white, e8)) {
					if (!(Threathens(1ULL << d8, opponentPieceList, myPieces, opponentPieces, white, d8) ||
						Threathens(1ULL << b8, opponentPieceList, myPieces, opponentPieces, white, b8))) {
						mv.from = e8;
						mv.to = c8;
						mv.enPassant = 0;
						mv.transforms = EMPTY;
						mv.isCastelingMove = true;
						mv.movedPiece = blackKing;
						(*moves).pushMove(mv);
					}
				}
			}
		}
	}
}

void PawnCaptureMoves(MoveList* moves, U64 pawns, U64 myPieces, U64 opponentPieces, bool white, U64 enPassant, U64 opponentPieceList[]) {
	if (white) {
		//U64 advance = (pawns << UP) & (~opponentPieces) & (~myPieces);
		U64 transforms = (pawns << UP) & (~opponentPieces) & (~myPieces) & ROW_8;
		Move mv;
		// on the 8th row so it transforms
		while (transforms) {
			char to = POP(&transforms);
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] - PAWN_VAL;
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}
		U64 left = ((pawns & NOT_H_FILE) << LEFT_UP) & (opponentPieces | enPassant); // Probably A
		transforms = left & ROW_8;
		while (left) {
			char to = POP(&left);
			mv.from = to - LEFT_UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}
		// on the 8th row so it transforms
		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - LEFT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] + score;
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}

		U64 right = ((pawns & NOT_A_FILE) << RIGHT_UP) & (opponentPieces | enPassant);
		transforms = right & ROW_8;
		while (right) {
			char to = POP(&right);
			mv.from = to - RIGHT_UP;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			mv.movedPiece = whitePawn;
			(*moves).pushMove(mv);
		}
		// on the 8th row so it transforms
		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = blackPawn + i;
					break;
				}
			}
			for (int i = whiteKnight; i < whiteKing; i++) {
				mv.from = to - RIGHT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i; // TODO calculate value of taken piece
				mv.score = value[i] + score;
				mv.movedPiece = whitePawn;
				(*moves).pushMove(mv);
			}
		}
	}
	else {
		U64 left = ((pawns & NOT_A_FILE) >> LEFT_UP) & (opponentPieces | enPassant);
		U64 right = ((pawns & NOT_H_FILE) >> RIGHT_UP) & (opponentPieces | enPassant);
		U64 transforms = ((pawns >> UP) & (~opponentPieces)) & (~myPieces) & ROW_1;
		
		Move mv;
		while (transforms) {
			char to = POP(&transforms);
			for (int i = blackKnight; i < blackKing; i++) {
				mv.from = to + UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] - PAWN_VAL;// TODO calculate value of taken piece
				mv.movedPiece = blackPawn;
				(*moves).pushMove(mv);
			}
		}

		transforms = left & ROW_1;
		left &= ~ROW_1;
		while (left) {
			char to = POP(&left);
			mv.from = to + LEFT_UP; // TODO check
			mv.to = to;
			mv.enPassant = 0;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			mv.transforms = EMPTY;
			mv.movedPiece = blackPawn;
			(*moves).pushMove(mv);
		}

		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			for (int i = blackKnight; i < blackKing; i++) {
				mv.from = to + LEFT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] + score;// TODO calculate value of taken piece
				mv.movedPiece = blackPawn;
				(*moves).pushMove(mv);
			}
		}

		transforms = right & ROW_1;
		right &= ~ROW_1;
		while (right) {
			char to = POP(&right);
			mv.from = to + RIGHT_UP; // TODO check
			mv.to = to;
			mv.enPassant = 0;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score += value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			mv.transforms = EMPTY;
			mv.movedPiece = blackPawn;
			(*moves).pushMove(mv);
		}

		while (transforms) {
			char to = POP(&transforms);
			int score = 0;
			for (int i = 1; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					score = value[i] - PAWN_VAL;
					mv.taken = whitePawn + i;
					break;
				}
			}
			for (int i = blackKnight; i < blackKing; i++) {
				mv.from = to + RIGHT_UP;
				mv.to = to;
				mv.enPassant = 0;
				mv.transforms = i;
				mv.score = value[i] + score;// TODO calculate value of taken piece
				mv.movedPiece = blackPawn;
				(*moves).pushMove(mv);
			}
		}
	}
}

void BishopCaptureMoves(MoveList* moves, U64 bishops, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	//Generating bishop moves = 
	//Take offsets, generate moves until you don't reach your piece, your opponent piece, the end of the board
	//Work with all of them at the same time
	Move mv;
	//LEFT_UP
	U64 temp = bishops;
	U64 attacks;
	char i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq - LEFT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	//RIGHT_UP
	temp = bishops;
	i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq - RIGHT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	//LEFT DOWN
	temp = bishops;
	i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq + RIGHT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	//RIGHT_DOWN
	temp = bishops;
	i = 0;
	while (temp) {
		i++;
		temp = ((((temp & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) & (~myPieces));
		attacks = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attacks) {
			char sq = POP(&attacks);
			mv.from = sq + LEFT_UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - BISHOP_VAL;
					mv.taken = base + i; // piece - 2 because bishop and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}
}

void KnightCaptureMoves(MoveList* moves, U64 knights, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	// use the knight move offset table
	Move mv;
	U64 attacks;
	while (knights) {
		char sq = POP(&knights);
		attacks = KnightOffsets[sq] & opponentPieces;
		while (attacks) {
			char to = POP(&attacks);
			mv.from = sq;
			mv.to = to;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << to)) {
					mv.score = value[i] - KNIGHT_VAL;
					mv.taken = base + i; // piece - 1 because knight and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

}

void RookCaptureMoves(MoveList* moves, U64 rooks, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base)
{
	U64 temp = rooks;
	U64 attack;
	char i = 0;
	Move mv;
	while (temp) {
		i++;
		//left
		temp = ((temp & NOT_A_FILE) >> LEFT) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq + LEFT * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	temp = rooks;
	i = 0;
	while (temp) {
		i++;
		//right
		temp = ((temp & NOT_H_FILE) << LEFT) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq - LEFT * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	temp = rooks;
	i = 0;
	while (temp) {
		i++;
		//up
		temp = ((temp & (~ROW_8)) << UP) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq - UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}

	temp = rooks;
	i = 0;
	while (temp) {
		i++;
		//down
		temp = ((temp & (~ROW_1)) >> UP) & (~myPieces);
		attack = temp & opponentPieces;
		temp &= ~opponentPieces;
		while (attack) {
			char sq = POP(&attack);
			mv.from = sq + UP * i;
			mv.to = sq;
			mv.enPassant = 0;
			mv.transforms = EMPTY;
			for (int i = 0; i < 5; i++) {
				if (opponentPieceList[i] & (1ULL << sq)) {
					mv.score = value[i] - ROOK_VAL;
					mv.taken = base + i; // piece - 3 because rook and pawn
					break;
				}
			}
			mv.movedPiece = piece;
			(*moves).pushMove(mv);
		}
	}
}

void QueenCaptureMoves(MoveList* moves, U64 queens, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	RookCaptureMoves(moves, queens, myPieces, opponentPieces, piece, opponentPieceList, base);
	BishopCaptureMoves(moves, queens, myPieces, opponentPieces, piece, opponentPieceList, base);
}

void KingCaptureMoves(MoveList* moves, U64 king, U64 myPieces, U64 opponentPieces, char piece, U64 opponentPieceList[], char base) {
	U64 pos = ((king & NOT_A_FILE) >> LEFT) | ((king & NOT_H_FILE) << LEFT) |
		((king & (~ROW_8)) << UP) | ((king & (~ROW_1)) >> UP);
	pos |= (((king & NOT_H_FILE) & (~ROW_8)) << LEFT_UP) | (((king & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP);
	pos |= (((king & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) | (((king & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP);
	U64 capture = pos & opponentPieces;
	Move mv;
	char from = POP(&king);
	while (capture) {
		char sq = POP(&capture);
		mv.from = from;
		mv.to = sq;
		mv.enPassant = 0;
		mv.transforms = EMPTY;
		for (int i = 0; i < 5; i++) {
			if (opponentPieceList[i] & (1ULL << sq)) {
				mv.score = value[i] - KING_VAL;
				mv.taken = base + i; // piece - 3 because rook and pawn
				break;
			}
		}
		mv.movedPiece = piece;
		(*moves).pushMove(mv);
	}
}