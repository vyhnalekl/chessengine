#include "stdafx.h"
/*#include "Definitions.cpp"
#include "Board.h"
#include "Debug.h"
#include "HashGenerator.h"
#include <iostream>

char pieces[120] = { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, whiteRook, EMPTY, EMPTY, EMPTY, whiteKing, EMPTY, EMPTY, whiteRook, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, blackRook, EMPTY, EMPTY, EMPTY, blackKing, EMPTY, EMPTY, blackRook, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };

char blackPL[6][10] = { { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ A8, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };
char whitePL[6][10] = { { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ A1, H1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };


char pieces2[120] = { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, whiteBishop, EMPTY, EMPTY, EMPTY, whiteKing, EMPTY, EMPTY, whiteBishop, NONE,
NONE, EMPTY, EMPTY, whitePawn, EMPTY, EMPTY, EMPTY, blackPawn, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, whitePawn, EMPTY, EMPTY, EMPTY, NONE,
NONE, blackPawn, whitePawn, EMPTY, EMPTY, blackPawn, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, whitePawn, EMPTY, NONE,
NONE, blackBishop, EMPTY, EMPTY, EMPTY, blackKing, EMPTY, EMPTY, blackBishop, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char enPassantBoard2 = A6;
char blackPieceListBoard2[6][10] = { { A5, E5, G2, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ A8, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };
char whitePieceListBoard2[6][10] = { { B5, C2, E4, G7, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ A1, H1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };



char pieces3[120] = { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, whiteKnight, EMPTY, EMPTY, EMPTY, whiteKing, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, whiteQueen, EMPTY, EMPTY, EMPTY, blackQueen, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, whiteKnight, EMPTY, EMPTY, EMPTY, NONE,
NONE, blackPawn, whitePawn, EMPTY, EMPTY, blackPawn, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, blackKnight, EMPTY, EMPTY, EMPTY, EMPTY, whitePawn, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, blackKing, EMPTY, EMPTY, blackKnight, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };

char blackPieceListBoard3[6][10] = { { E5, A5, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ B7, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ G2, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };
char whitePieceListBoard3[6][10] = { { G7, B5, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ A1, E4, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ C2, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };

bool notEqualBoards(char b1[CHESSBOARD_LENGTH], char b2[CHESSBOARD_LENGTH]) {
	for (int i = 0; i < CHESSBOARD_LENGTH; i++) {
		if (b1[i] != b2[i]) {
			return true;
		}
	}
	return false;
}
unsigned long long oldHash = 0;
void runBoardTests() {
	Board* b = new Board();
	b->setPieces(pieces);
	b->setBlackPieceList(blackPL);
	b->setWhitePieceList(whitePL);
	std::list<struct Move> moves = b->GenerateMoves();
	//printBoard(b->getPieces());
	//printMoveList(moves);
	std::cout << "Testing Board 1" << std::endl;
	while (!moves.empty()) {
		struct Move mv = moves.front();
		moves.pop_front();

		b->PlayMove(mv);
		std::list<struct Move> moves2 = b->GenerateMoves();
		char *currentBoard = b->getPieces();
		while(!moves2.empty()){
			struct Move mv2 = moves2.front();
			oldHash = generatePositionKey(b);
			moves2.pop_front();
			b->PlayMove(mv2);
			b->UndoMove();
			
			if (notEqualBoards(b->getPieces(), currentBoard)) {
				std::cout << "Error: DO/Undo Move DEPTH 2" << std::endl;
				printBoard(b->getPieces());
				printBoard(currentBoard);
				printMove(mv2);
			}
			if (oldHash != generatePositionKey(b)) {
				std::cout << "Error: Hash function returned differend hash keys for same board" << std::endl;
			}
		}
		b->UndoMove();
		if (notEqualBoards(b->getPieces(), pieces)) {
			std::cout << "Error: DO/Undo Move" << std::endl;
			printBoard(b->getPieces());
			printBoard(pieces);
			printMove(mv);
		}
	}


	b->setPieces(pieces2);
	b->setBlackPieceList(blackPieceListBoard2);
	b->setWhitePieceList(whitePieceListBoard2);
	b->enPassant = enPassantBoard2;
	b->blackCastleAlowed = NO;
	b->whiteCastleAlowed = NO;
	std::list<struct Move> moves2 = b->GenerateMoves();
	//printBoard(b->getPieces());
	//printMoveList(moves);

	std::cout << "Testing Board 2" << std::endl;
	while (!moves2.empty()) {
		struct Move mv = moves2.front();
		moves2.pop_front();

		b->PlayMove(mv);
		std::list<struct Move> moves22 = b->GenerateMoves();
		char *currentBoard = b->getPieces();
		while (!moves22.empty()) {
			struct Move mv2 = moves22.front();
			moves22.pop_front();
			oldHash = generatePositionKey(b);
			b->PlayMove(mv2);
			b->UndoMove();
			if (notEqualBoards(b->getPieces(), currentBoard)) {
				std::cout << "2Error: DO/Undo Move DEPTH 2" << std::endl;
				printBoard(b->getPieces());
				printBoard(currentBoard);
				printMove(mv2);
			}
			//b->UndoMove();
			if (oldHash != generatePositionKey(b)) {
				std::cout << "Error: Hash function returned differend hash keys for same board" << std::endl;
			}
		}
		b->UndoMove();
		if (notEqualBoards(b->getPieces(), pieces2)) {
			std::cout << "2Error: DO/Undo Move" << std::endl;
			printBoard(b->getPieces());
			printBoard(pieces2);
			printMove(mv);
		}
	}

	b->setPieces(pieces3);
	b->setBlackPieceList(blackPieceListBoard3);
	b->setWhitePieceList(whitePieceListBoard3);
	b->blackCastleAlowed = NO;
	b->whiteCastleAlowed = NO;
	std::list<struct Move> moves3 = b->GenerateMoves();
	//printBoard(b->getPieces());
	//printMoveList(moves);

	std::cout << "Testing Board 3" << std::endl;
	while (!moves3.empty()) {
		struct Move mv = moves3.front();
		moves3.pop_front();

		b->PlayMove(mv);
		std::list<struct Move> moves32 = b->GenerateMoves();
		char *currentBoard = b->getPieces();
		while (!moves32.empty()) {
			struct Move mv3 = moves32.front();
			moves32.pop_front();
			b->PlayMove(mv3);
			b->UndoMove();
			if (notEqualBoards(b->getPieces(), currentBoard)) {
				std::cout << "2Error: DO/Undo Move DEPTH 2" << std::endl;
				printBoard(b->getPieces());
				printBoard(currentBoard);
				printMove(mv3);
			}
		}
		b->UndoMove();
		if (notEqualBoards(b->getPieces(), pieces3)) {
			std::cout << "2Error: DO/Undo Move" << std::endl;
			printBoard(b->getPieces());
			printBoard(pieces3);
			printMove(mv);
		}
	}
}
*/