#pragma once
#ifndef DEBUG_H
#define DEBUG_H
#include "stdafx.h"
#include "Definitions.cpp"
#include <iostream>
#include "MoveList.h"
#include "Bitboard.h"

void printMove(struct Move);

void printMoveList(MoveList*);

void printBoard(char pieces[120]);

bool checkMove(struct Move, char[CHESSBOARD_LENGTH]);

bool checkMoveNoCout(struct Move, char[CHESSBOARD_LENGTH]);
bool checkMoveNoCout(struct Move, Bitboard*);

bool checkBoard(char[CHESSBOARD_LENGTH]);

bool checkPiecesList(char[CHESSBOARD_LENGTH], char[6][MAX_NUMBER_OF_PIECES], char[6][MAX_NUMBER_OF_PIECES]);
#endif