#include "stdafx.h"
#include <iostream>
#include "Bitboard.h"
#include "HashGenerator.h"
#include "Debug.h"

namespace {


	int from120To64(char sq) {
		int ff = (sq % 10) - 1;
		int rf = (sq - 21) / 10;
		return (rf * 8) + ff;
	}

	char* print64(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}

	char* print642(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}
	char squareTo120(char sq64) {
		int result = A1;
		result += (sq64 / 8) * 10;
		result += sq64 % 8;
		return result;
	}
	void printBitboard(Bitboard *b) {
		std::cout << std::endl;
		char pieces[120];
		for (int i = 0; i < 120; i++) {
			pieces[i] = EMPTY; // TODO, still empty on left and right side
		}
		for (int i = 0; i < 6; i++) {
			U64 temp = b->whitePiecesList[i];
			while (temp) {
				char sq = POP(&temp);
				pieces[squareTo120(sq)] = i + 1;
			}
		}
		for (int i = 0; i < 6; i++) {
			U64 temp = b->blackPiecesList[i];
			while (temp) {
				char sq = POP(&temp);
				pieces[squareTo120(sq)] = i + 7;
			}
		}
		printBoard(pieces);
		std::cout << std::endl;
	}
	char* getBoardToPrint(Bitboard *b) {
		char pieces[120];
		for (int i = 0; i < 120; i++) {
			pieces[i] = EMPTY; // TODO, still empty on left and right side
		}
		for (int i = 0; i < 6; i++) {
			U64 temp = b->whitePiecesList[i];
			while (temp) {
				char sq = POP(&temp);
				pieces[squareTo120(sq)] = i + 1;
			}
		}
		for (int i = 0; i < 6; i++) {
			U64 temp = b->blackPiecesList[i];
			while (temp) {
				char sq = POP(&temp);
				pieces[squareTo120(sq)] = i + 7;
			}
		}
		return pieces;
	}
}



Move PickBestMoveT(int indexFrom, MoveList moves) { // because it is inefficient to sort all moves
	Move best;
	int indexOfBest = indexFrom;
	for (int i = indexFrom + 1; i < moves.length; i++) {
		if (moves.moves[i].score > moves.moves[indexOfBest].score) {
			indexOfBest = i;
		}
	}
	best = moves.moves[indexOfBest];
	moves.moves[indexOfBest] = moves.moves[indexFrom];
	moves.moves[indexFrom] = best;
	return best;
}
MoveList *ml = new MoveList(50);
int j = 0; // counter of depth
int alphaBetaT(Bitboard *b, SearchData *s, int alpha, int beta, int depthLeft, bool whiteOnMove);
void hashTest(Bitboard *b) {
		SearchData *s = new SearchData();
		alphaBetaT(b, s, -INFINITY, -INFINITY, 5, b->whiteOnMove);
}

int alphaBetaT(Bitboard *b, SearchData *s, int alpha, int beta, int depthLeft, bool whiteOnMove) {
	s->numberOfPositions++;
	Move best;
	best.from = -100;
	int bestScore = -INFINITE;
	int score = -INFINITE;
	b->whiteOnMove = whiteOnMove;

	if (depthLeft == 0) {
		return 0;
	}

	MoveList moves = MoveList(MAX_GENERATE_MOVES);
	(*b).GenerateMoves(&moves);

	int legalMoves = 0;


	for (int i = 0; i < moves.length; i++) {
		Move mv = PickBestMoveT(i, moves);
		bool oldWhite = b->whiteOnMove;
		unsigned long long key = simpleHash(b);
		char* pieces = getBoardToPrint(b);
		if (!(*b).PlayMove(&mv)) {
			(*b).UndoMove();
			continue;
		}
		unsigned long long key2 = simpleHash(b);
		legalMoves++;
		//if ((!b->isRepeating(simpleHash(b))) && (!b->FiftyWithoutTake())) {
			score = -alphaBetaT(b, s, -beta, -alpha, depthLeft - 1, !whiteOnMove);
		/*}
		else {
			score = 0;
		}*/
		//std::cout << key << "\t" << b->key << std::endl;
		if (key == key2) {
			std::cout << "ERROR keys are same" << std::endl;
		}
		(*b).UndoMove();
		key2 = simpleHash(b);
		if (key != key2 && b->whiteOnMove == oldWhite) {
			std::cout << "ERROR keys are differend " << key << " vs " << key2 << std::endl;
			printBoard(pieces);
			printBitboard(b);
		}
		
	}


	free(moves.moves);


	return alpha;
}