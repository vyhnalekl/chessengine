#include "stdafx.h"
#include "Definitions.cpp"
#include "Board.h"
#include "Rating.h"
#include "HashGenerator.h"
#include "Debug.h"
#include "MoveList.h"
#include <chrono>
#include <thread>
#include "Learning.h"
#include "Bitboard.h"
namespace {
	char* print64(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}

	char* print642(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}

	
}
void PrintMove64(Move mv) {
	std::cout << print64(mv.from) << print642(mv.to) << std::endl;
}
int from120To64(char sq) {
	int ff = (sq % 10) - 1;
	int rf = (sq - 21) / 10;
	return (rf * 8) + ff;
}
/*
char* print64(char sq) {
	static char r[2];
	r[0] = ('a' + (sq % 8));
	r[1] = ('1' + (sq / 8));
	return r;
}

char* print642(char sq) {
	static char r[2];
	r[0] = ('a' + (sq % 8));
	r[1] = ('1' + (sq / 8));
	return r;
}*/

bool CompareStateOfTheBoard(Bitboard * bit, Board *  b) {
	bool result = true;
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (b->whitePiecesList[i][j] != NONE) {
				if (!(bit->whitePiecesList[i] & (1ULL << from120To64(b->whitePiecesList[i][j])))) {
					std::cout << "ERROR at white index: " << i << std::endl;
					result = false;
				}
			}
		}
	}
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (b->blackPiecesList[i][j] != NONE) {
				if (!(bit->blackPiecesList[i] & (1ULL << from120To64(b->blackPiecesList[i][j])))) {
					std::cout << "ERROR at black index: " << i << std::endl;
					result = false;
				}
			}
		}
	}
	return result;
}
/*
void PrintMoveList64(MoveList * ml) {
	for (int i = 0; i < ml->length; i++) {
		Move mv = ml->moves[i];
		std::cout << print64(mv.from) << "-->" << print642(mv.to) << std::endl;
	}
}*/
/*
bool checkMoveLists(MoveList *ml1, MoveList *ml2) {
	Move mv, mv2;
	Move old;
	bool found = false;
	for (int j = 0; j < ml2->length; j++) {
		// nothing to undo
		mv = ml2->moves[j];
		char from = from120To64(mv.from);
		char to = from120To64(mv.to);
		for (int i = 0; i < ml1->length; i++) {
			mv2 = ml1->moves[i];
			if (from == mv2.from && to == mv2.to) {
				found = true;
			}
		}
		if (!found) {
			std::cout << "Eroor: \t" << PrMove(mv) << " not found." << std::endl;
			PrintMoveList64(ml1);
			return false;
		}
	}
	return true;
}
*/
//******************************************************
//                 Transposition Table
//******************************************************
unsigned int size = (1024 * 1024 * 10);//4294967294; // 2^32
Transposition* t;
int transpositionErrors = 0;
int transpositionHelps = 0;
int collisionsAllTime = 0;
void initializeTable() {
	t = nullptr;
	t = (Transposition *)malloc(size * sizeof(Transposition));
	while (t == nullptr) {
		size = size / 2;
		t = (Transposition *)malloc(size * sizeof(Transposition));
	}
	try {
		for (unsigned int i = 0; i < size; i++) {
			t[i] = Transposition();
		}
	}
	catch (const std::bad_alloc&) {
		size = size / 2;
		delete[] t;
		initializeTable();
	}
}

void addRecord(unsigned long long hashKey, Move best, int score, bool whiteOnMove, int depth, char fg) {
	/*if (size == 0) {
		std::cout << "ERROR Table have size of 0" << std::endl;
		return;
	}*/

	int index = hashKey % size;
	if (t[index].init) {
		t[index].hashKey = hashKey;
		t[index].move = best;
		t[index].score = score;
		t[index].white = whiteOnMove;
		t[index].depth = depth;
		t[index].init = false;
		t[index].flag = fg;
	}
	else {
		if (t[index].depth < depth) {
			t[index].hashKey = hashKey;
			t[index].move = best;
			t[index].score = score;
			t[index].white = whiteOnMove;
			t[index].depth = depth;
			t[index].init = false;
			t[index].flag = fg;
		}
	}

	//TODO debug remove
	/*for (int i = 0; i < 120; i++) {
		t[index].pieces[i] = pieces[i];
	}*/

	//also remove argument from addRecord and calls
	//also remove property from transposition record
	//TODO debug remove end

}

bool getRecord(unsigned long long hashKey, int depth, int alpha, int beta, int * score, Move * best, bool whiteOnMove) {//TODO remove pieces
	int index = hashKey % size;
	if (!t[index].init) {
		if (t[index].hashKey == hashKey && whiteOnMove == t[index].white) { // check full hash key and side on move
			*best = t[index].move;
			/*for (int i = A1; i < H8; i++) {
				if (pieces[i] != t[index].pieces[i]) {
					best->from = -100;
					collisionsAllTime++;
				}
				return false;
			}*/
			if (depth <= t[index].depth) {
				*score = t[index].score;
				char flag = t[index].flag;
				if (flag == ALPHA && (*score <= alpha)) {
					*score = alpha;
					return true;
				}
				else if (flag == BETA && (*score >= beta)) {
					*score = beta;
					return true;
				}
				else if (flag == EXACT) {
					return true;
				}
			}
		}
	}
	return false;
}

Move PickBestMove(int indexFrom, MoveList moves) { // because it is inefficient to sort all moves
	Move best;
	int indexOfBest = indexFrom;
	for (int i = indexFrom + 1; i < moves.length; i++) {
		if (moves.moves[i].score > moves.moves[indexOfBest].score) {
			indexOfBest = i;
		}
	}
	best = moves.moves[indexOfBest];
	moves.moves[indexOfBest] = moves.moves[indexFrom];
	moves.moves[indexFrom] = best;
	return best;
}


/***********************************************************/
//					Search Functions
/***********************************************************/

int Quiesce(SearchData *s, int alpha, int beta, int depth, bool whiteOnMove, Bitboard* bitboard) {
	s->quiescencePositions++;

	(*bitboard).whiteOnMove = whiteOnMove;
	if (depth < s->maxDepthQuiesce) {
		return rateBoard(bitboard, whiteOnMove);
	}
	int score = rateBoard(bitboard, whiteOnMove);
	if (score >= beta) {
		return beta;
	}
	if (alpha < score) {
		alpha = score;
	}

	MoveList moves = MoveList(MAX_GENERATE_MOVES);
	(*bitboard).GenerateCapturingMoves(&moves);

	for (char i = 0; i < moves.length; i++) {
		Move mv = PickBestMove(i, moves);

		if (!(*bitboard).PlayMove(&mv)) {
			(*bitboard).UndoMove();
			continue;
		}

		if (i > 1 && depth >= s->maxDepthQuiesce + 3) {
			char reduce = i > 6 ? 2 : 1;
			score = -Quiesce(s, -beta, -alpha, depth - 1 - reduce, !whiteOnMove, bitboard);
			if (score > alpha) {
				score = -Quiesce(s, -beta, -alpha, depth - 1, !whiteOnMove, bitboard);
			}
		}
		else {
			score = -Quiesce(s, -beta, -alpha, depth - 1, !whiteOnMove, bitboard);
		}

		(*bitboard).UndoMove();
		if (score >= beta) {
			free(moves.moves);
			moves.moves = NULL;
			return beta;
		}
		if (score > alpha) {
			alpha = score;
		}
	}

	free(moves.moves);
	moves.moves = NULL;
	return alpha;
}

// end"
//position startpos moves d2d4 g8f6 b1c3 d7d5 g1f3 c8f5 e2e3 b8c6 f1b5 d8d6 f3e5 f5d7 b5c6 d7c6 e1g1 d6e6 f2f4 e6d6 c1d2 e7e6 d1e2 c6d7 g2g4 f8e7 g4g5 f6e4 c3e4 d5e4 a2a3 a7a5 e2g2 d7c6 c2c4 e8g8 b2b4 f7f6 g5f6 e7f6 b4b5 c6d7 e5d7 d6d7 g2e4 a8a7 d2c3 a5a4 f1b1 f8d8 a1a2 f6e7 g1h1 d8e8 b1g1 g8h8 e4e5 e7f6 e5h5 a7a8 a2g2 d7d6 c3b4 d6d7 h5f3 e8b8 g2d2 h8g8 d2c2 d7c8 b4c5 c8d7 c2b2 f6h4 f3g4 h4f6 g4f3 f6h4 c5b4 h4f6 b4c3 a8a7 b2d2 d7d6 c3b4 d6d8 f3g4 d8d7 d4d5 b8e8 e3e4 g7g6 b4c5 a7a8 e4e5 f6e7 d5d6 b7b6 g4g2 b6c5 d6e7 d7e7 g1d1 a8d8 g2f3 d8d2 d1d2 e8f8 f3g4 e7f7 d2f2 f8d8 g4f3 d8d4 h1g2 d4c4 g2g3 f7d7 f2b2 d7d4 f3f1 d4c3 g3g4 c4f4 f1f4 c3b2 f4a4 b2g2 g4f4 g6g5 f4e3 g2h3 e3e2 h3h2 e2f3 h2e5 a4e4 e5d5 a3a4 c5c4
// midle
//position startpos moves d2d4 e7e6 g1f3 g8f6 c1f4 d7d5 e2e3 f8d6 f4g5 c7c5 f1b5 c8d7 b1c3 e8g8 e1g1 d6e7 f3e5 d7b5 c3b5 d8b6
int alphaBeta(SearchData *s, int alpha, int beta, int depthLeft, bool whiteOnMove, bool nullPruning, Bitboard * bitboard) {
	s->numberOfPositions++;
	
	if (depthLeft <= 0) {
		return Quiesce(s, alpha, beta, depthLeft - 1, whiteOnMove, bitboard);
	}
	Move best;
	best.from = -100;// when transposition table doesn't help, you need to be able to determinate that.
	bitboard->whiteOnMove = whiteOnMove;
	unsigned long long boardKey = simpleHash(bitboard); // TODO theoreticaly it is just the same as the key of last move in history
	/*if (bitboard->history.length > 0) {
		if (boardKey != bitboard->history.moves[bitboard->history.length - 1].keyAfterMove) {
			Move temp = bitboard->history.moves[bitboard->history.length - 1];
			std::cout << "That is wierd" << std::endl;
			bitboard->UndoMove();
			bitboard->PlayMove(&temp);
		}
	}*/
	/*if ((b->isRepeating(boardKey)) || (b->FiftyWithoutTake())) {

		// TODO problem with fold repetitions
		return 0;
	}*/
	int bestScore = -INFINITE - MAX_GAME_DEPTH; // Must be here othervise engine can play NO MOVE because when there is mate it would return score -INFINITE - depth (which is smaller than -INFINITE)
	int score = -INFINITE - MAX_GAME_DEPTH;
	int OldAlpha = alpha;
	//if (nullPruning) {
		if (getRecord(boardKey, depthLeft, alpha, beta, &score, &best, whiteOnMove)) {
			if (checkMoveNoCout(best, bitboard)) {
				/*if ((bitboard->isRepeating(boardKey, nullPruning))) { // || (b->FiftyWithoutTake())
					// is causing a problem with arena, arena don't recognise 50 moves without capture or pawn move. It lets engines play on
					// TODO problem with fold repetitions
					score = 0;
					bestScore = 0;
					(*bitboard).bestMove = best;
				}
				else {*/
					bitboard->tHelpAllTime++;
					(*bitboard).bestMove = best;
					
					if(bitboard->isRepeating(nullPruning)) {
						return 0;
					}
					return score;
				//}
			}
			else {
				bitboard->tCollisionAllTime++;
				best.from = -100;// when transposition table doesn't help, you need to be able to determinate that.
				score = -INFINITE;
			}
		}
	//}
	
	if (nullPruning) {
		if (whiteOnMove) {
			U64 kingPos = bitboard->whitePiecesList[5];
			if (depthLeft >= 4 && (!Threathens(bitboard->whitePiecesList[5],bitboard->blackPiecesList, bitboard->whitePieces, bitboard->blackPieces, whiteOnMove, POP(&kingPos) ))){
				//s->maxDepthQuiesce = - (depthLeft - 4);
				U64 enPass = bitboard->enPassant;
				bitboard->enPassant = 0;
				score = -alphaBeta(s, -beta, -beta + 1, depthLeft - 4, !whiteOnMove, false, bitboard);
				bitboard->enPassant = enPass;
				bitboard->whiteOnMove = whiteOnMove;
				if (score >= beta) {
					return beta;
				}
				
			}
		}
		else {
			U64 kingPos = bitboard->blackPiecesList[5];
			if (depthLeft >= 4 && (!Threathens(bitboard->blackPiecesList[5], bitboard->whitePiecesList, bitboard->blackPieces, bitboard->whitePieces, whiteOnMove, POP(&kingPos)))) {
				//s->maxDepthQuiesce = - (depthLeft - 4);
				U64 enPass = bitboard->enPassant;
				bitboard->enPassant = 0;
				score = -alphaBeta(s, -beta, -beta + 1, depthLeft - 4, !whiteOnMove, false, bitboard);
				bitboard->enPassant = enPass;
				bitboard->whiteOnMove = whiteOnMove;
				if (score >= beta) {
					return beta;
				}
				
			}
		}
	}
	//bitboard->ply++;

	MoveList moves = MoveList(MAX_GENERATE_MOVES);
	(*bitboard).GenerateMoves(&moves);
	//printMoveList(&moves);
	char legalMoves = 0;
	if (best.from != -100) {
		for (char i = 0; i < moves.length; i++) {
			if (moves.moves[i].from == best.from &&
				moves.moves[i].to == best.to) {
				moves.moves[i].score = 1000;
				bitboard->tHelpAllTime++;
				break;
			}
		}
	}

	for (char i = 0; i < moves.length; i++) {
		Move mv = PickBestMove(i, moves);
		mv.keyBeforeMove = boardKey;
		if (!(*bitboard).PlayMove(&mv)) {
			(*bitboard).UndoMove();
			continue;
		}

		legalMoves++;
		
		if (!(bitboard->isRepeating(nullPruning))) { 
			if (i > 1 && depthLeft >= 3) {
				int reduce = legalMoves > 6 ? 2 : 1;
				score = -alphaBeta(s, -beta, -alpha, depthLeft - 1 - reduce, !whiteOnMove, true, bitboard);
				if (score > alpha) {
					score = -alphaBeta(s, -beta, -alpha, depthLeft - 1, !whiteOnMove, true, bitboard);
				}
			}
			else {
				score = -alphaBeta(s, -beta, -alpha, depthLeft - 1, !whiteOnMove, false, bitboard);
			}
		}
		else {
			// || (b->FiftyWithoutTake())
			// is causing a problem with arena, arena don't recognise 50 moves without capture or pawn move. It lets engines play on
			// TODO problem with fold repetitions
			score = 0;
		}

		//bitboard->whiteOnMove = whiteOnMove;

		(*bitboard).UndoMove();

		if (score > bestScore) {
			bestScore = score;
			best = mv;
			if (score > alpha) {
				if (score >= beta) {
					free(moves.moves);
					moves.moves = NULL;
					mv.score = beta;
					/*if (mv.from == -100) {
						std::cout << "Error";
					}*/
					(*bitboard).bestMove = mv;
					addRecord(boardKey, best, beta, whiteOnMove, depthLeft, BETA);
					return beta;
				}
				alpha = score;
			}
		}
	}
	best.score = bestScore;
	/*if (best.from == -100) {
		std::cout << "Error";
	}*/
	bitboard->bestMove = best;

	free(moves.moves);
	moves.moves = NULL;

	if (legalMoves == 0) {
		if (whiteOnMove && bitboard->isCheck(true)) {
			//bitboard->bestMove.from = BLACK_WIN;
			return -INFINITE - depthLeft;
		}
		else if (!whiteOnMove &&  bitboard->isCheck(false)) {
			//bitboard->bestMove.from = WHITE_WIN;
			return -INFINITE - depthLeft;
		}
		else {
			//bitboard->bestMove.from = DRAW;
			return 0;
		}
	}
	if (OldAlpha == alpha) {
		//if (nullPruning) {
			addRecord(boardKey, best, alpha, whiteOnMove, depthLeft, ALPHA);
		//}
	}
	else {
		//if (nullPruning) {
			addRecord(boardKey, best, bestScore, whiteOnMove, depthLeft, EXACT);
		//}
	}

	return bestScore;
}
int helpFromFile = 0;


int iterativeDeepening(SearchData *sd, Bitboard * bitboard) {
	bool helpFromFileOccured = false;
	bool white = bitboard->whiteOnMove;
	//MoveList *ml = new MoveList(10);
	unsigned long long key = simpleHash(bitboard);
	bool done = false;

	/*Board *b2 = new Board();
	b2->setPieces(b->pieces);
	b2->setBlackPieceList(b->blackPiecesList);
	b2->setWhitePieceList(b->whitePiecesList);
	b2->enPassant = b->enPassant;
	b2->whiteCastleAlowed = b->whiteCastleAlowed;
	b2->blackCastleAlowed = b->blackCastleAlowed;*/
	//std::thread t1(searchFile, b2, key, ml, white, &done, (int)sd->timeOnMove, sd->searchInFile);

	//std::cout << "Klic: " << key << std::endl;
	//printBoard(b->pieces);
	int score = 0;

	auto start = std::chrono::high_resolution_clock::now();
	int i = 1;
	sd->maxDepthQuiesce = QUIESCENE_MAX_DEPTH;
	/*if (!CompareStateOfTheBoard(bitboard, b)) {
		std::cout << "ERROR different state before move played" << std::endl;
	}*/
	for (; i < sd->maxDepth; i++) {
		bitboard->ply = 0;
		sd->currentDepth = i;
		sd->maxDepthQuiesce = -i;
		//b->ply = 0;
		alphaBeta(sd, -INFINITE, INFINITE, i, white, false, bitboard);
		auto finish = std::chrono::high_resolution_clock::now();
		auto microseconds = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
		std::cout << "info depth " << i << " seldepth " << i * 2 << " nodes " << sd->numberOfPositions + sd->quiescencePositions << " nps " << ((sd->numberOfPositions + sd->quiescencePositions) / (microseconds.count() + 1)) * 1000 << " score cp " << bitboard->bestMove.score << " time " << microseconds.count() << " pv ";
		std::cout << PrMove(bitboard->bestMove) << std::endl;
		//info depth 4 seldepth 8 nodes 2920 nps 1 score cp 115 time 0 pv h2g3 
		if (sd->timeOnMove < microseconds.count() * 3) {
			break;
		}
		if (sd->end) {
			break;
		}
		/*if (bitboard->bestMove.from < a1 || bitboard->bestMove.from > h8 || bitboard->bestMove.to < a1 || bitboard->bestMove.to > h8) {
			std::cout << "ARROR" << std::endl;
		}*/
		/*if (done && ml->length > 0) {
			b->bestMove = ml->popMove();
			helpFromFile++;
			helpFromFileOccured = true;
			break;
			/*int bestOccur = 0;
			for (int j = 0; j < ml->length; j++) {
				int occur = 0;
				Move temp = ml->moves[j];
				for (int i = 0; i < ml->length; i++) {
					Move mv = ml->moves[i];
					if (temp.from == mv.from && temp.to == mv.to) {
						occur++;
					}
				}
				if (occur > bestOccur) {
					bestOccur = occur;
					best = temp;
				}
			}
			helpFromFile++;
			b->bestMove = best;
			break;*/
			//}
	}

	//t1.join();
	/*free(b2->history.moves);
	delete b2;
	free(ml->moves);
	delete ml;

	if (!helpFromFileOccured) {
		saveMove(key, b->bestMove, (int)sd->timeOnMove, white);
	}*/
	//std::cout << "Klic: " << simpleHash(b) << std::endl;
	std::cout << "HELP FROM FILE: " << helpFromFile << std::endl;
	sd->depthReached = 2 * i;
	return score;
}