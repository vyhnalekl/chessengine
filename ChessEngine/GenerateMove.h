#pragma once
#ifndef GENERATEMOVE_H
#define GENERATEMOVE_H

#include "stdafx.h"
#include "Definitions.cpp"
#include "MoveList.h"

bool pawnThreathens(char, char);
bool knightThreathens(char, char);
bool bishopThreathens(char[], char , char );
bool rookThreathens(char[], char, char);
bool queenThreathens(char[], char , char );
bool kingThreathens(char , char );

void GeneratePawnMoves(MoveList*, char[], char[], bool, char);
void GenerateBishopMoves(MoveList*, char[], char[], bool);
void GenerateKnightMoves(MoveList*, char[], char[], bool);
void GenerateRookMoves(MoveList*, char[], char[], bool);
void GenerateQueenMoves(MoveList*, char[], char[], bool);
bool OpponentThreathens(char[], char [6][MAX_NUMBER_OF_PIECES], char );
void GenerateKingMoves(MoveList*, char [], char , bool);
void GenerateCastelingMoves(MoveList*, char[], char, bool, char, char[6][MAX_NUMBER_OF_PIECES], char);
bool isCheck(char, char[CHESSBOARD_LENGTH]);

void GeneratePawnCaptureMoves(MoveList*, char[], char[], bool, char);
void GenerateBishopCaptureMoves(MoveList*, char[], char[], bool);
void GenerateKnightCaptureMoves(MoveList*, char[], char[], bool);
void GenerateRookCaptureMoves(MoveList*, char[], char[], bool);
void GenerateQueenCaptureMoves(MoveList*, char[], char[], bool);
void GenerateKingCaptureMoves(MoveList*, char[], char, bool);

//std::list<struct ThreatheningPiece> isCheck(char, char[]);
/*std::list<struct Move> RemoveNonLegalMoves(std::list<struct Move>, ThreatheningPiece);*/
#endif