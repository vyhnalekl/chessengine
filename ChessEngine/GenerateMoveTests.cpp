#include "stdafx.h"
/*#include "Definitions.cpp"
#include "GenerateMove.h"
#include "Board.h"
#include "Debug.h"
#include <iostream>

char board1[120] = { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
					NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
					NONE, whiteRook, EMPTY, EMPTY, EMPTY, whiteKing, EMPTY, EMPTY, whiteRook, NONE,
					NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
					NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
					NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
					NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
					NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
					NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
					NONE, blackRook, EMPTY, EMPTY, EMPTY, blackKing, EMPTY, EMPTY, blackRook, NONE,
					NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
					NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char whiteRooks[] = { A1, H1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char whiteKingB1 = E1;
char blackRooks[] = { A8, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char blackKingB1 = E8;
char blackPieceList[6][10] = { { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ A8, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ E8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },};
char whitePieceList[6][10] = { { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ A1, H1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
								{ E1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };


char board2[120] = {	NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
						NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
						NONE, whiteBishop, EMPTY, EMPTY, EMPTY, whiteKing, EMPTY, EMPTY, whiteBishop, NONE,
						NONE, EMPTY, EMPTY, whitePawn, EMPTY, EMPTY, EMPTY, blackPawn, EMPTY, NONE,
						NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
						NONE, EMPTY, EMPTY, EMPTY, EMPTY, whitePawn, EMPTY, EMPTY, EMPTY, NONE,
						NONE, blackPawn, whitePawn, EMPTY, EMPTY, blackPawn, EMPTY, EMPTY, EMPTY, NONE,
						NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
						NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, whitePawn, EMPTY, NONE,
						NONE, blackBishop, EMPTY, EMPTY, EMPTY, blackKing, EMPTY, EMPTY, blackBishop, NONE,
						NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
						NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char enPassantB2 = A6;
char whiteBishopsB2[] = { A1, H1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char whitePawnsB2[] = { B5, C2, E4, G7, NONE, NONE, NONE, NONE, NONE, NONE };
char whiteKingB2 = E1;
char blackBishopsB2[] = { A8, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char blackPawnsB2[] = { A5, E5, G2, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char blackKingB2 = E8;
char blackPieceListB2[6][10] = { { A5, E5, G2, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ A8, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };
char whitePieceListB2[6][10] = { { B5, C2, E4, G7, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ A1, H1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE },
{ E1, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE }, };



char board3[120] = { NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, whiteKnight, EMPTY, EMPTY, EMPTY, whiteKing, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, whiteQueen, EMPTY, EMPTY, EMPTY, blackQueen, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, whiteKnight, EMPTY, EMPTY, EMPTY, NONE,
NONE, blackPawn, whitePawn, EMPTY, EMPTY, blackPawn, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, NONE,
NONE, EMPTY, blackKnight, EMPTY, EMPTY, EMPTY, EMPTY, whitePawn, EMPTY, NONE,
NONE, EMPTY, EMPTY, EMPTY, EMPTY, blackKing, EMPTY, EMPTY, blackKnight, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };

char whiteKnightsB3[] = { A1, E4, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char whiteQueensB3[] = { C2, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char blackKnightB3[] = { B7, H8, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };
char blackQueenB3[] = { G2, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE };


void runGenerateMoveTests() {
	std::list<struct Move> moves;

	GenerateRookMoves(&moves, board1, whiteRooks, true, E8, BothSides, BothSides);
	if (moves.size() != 19) {
		std::cout << "White Rook Move Generation | returned: " << moves.size() << " Expected 19."<< std::endl;
	}
	
	GenerateRookMoves(&moves, board1, blackRooks, false, E1, BothSides, BothSides);
	if (moves.size() != 38) {
		std::cout << "Black Rook Move Generation | returned: " << moves.size() << " Expected 38." << std::endl;
	}

	GenerateKingMoves(&moves, board1, whiteKingB1, true, blackPieceList, BothSides, BothSides);
	if (moves.size() != 43) {
		std::cout << "White King Move Generation | returned: " << moves.size() << " Expected 43." << std::endl;
	}

	GenerateKingMoves(&moves, board1, blackKingB1, false, whitePieceList, BothSides, BothSides);
	if (moves.size() != 48) {
		std::cout << "Black King Move Generation | returned: " << moves.size() << " Expected 48." << std::endl;
	}
	//
	//void GenerateCastelingMoves(std::list<struct Move> * moves, char pieces[], char king, bool white, char opKingPos, char opponentPieces[6][MAX_NUMBER_OF_PIECES]
	//	, char castelingOptions) {
	GenerateCastelingMoves(&moves, board1, whiteKingB1, true, blackKingB1, blackPieceList, BothSides);
	if (moves.size() != 50) {
		std::cout << "White Casteling Move Generation | returned: " << moves.size() << " Expected 50." << std::endl;
	}

	GenerateCastelingMoves(&moves, board1, blackKingB1, false, whiteKingB1, whitePieceList, BothSides);
	if (moves.size() != 52) {
		std::cout << "Black Casteling Move Generation | returned: " << moves.size() << " Expected 52." << std::endl;
	}

	
	// Board 2 tests
	
	GeneratePawnMoves(&moves, board2, whitePawnsB2, true, E8, enPassantB2, BothSides, BothSides);
	//printBoard(board2);
	//printMoveList(moves);
	if (moves.size() != 64) {
		std::cout << "White Pawn Move Generation | returned: " << moves.size() << " Expected 64." << std::endl;
	}
	
	GeneratePawnMoves(&moves, board2, blackPawnsB2, false, E1, enPassantB2, BothSides, BothSides);
	//printBoard(board2);
	//printMoveList(moves);
	if (moves.size() != 73) {
		std::cout << "Black Pawn Move Generation | returned: " << moves.size() << " Expected 73." << std::endl;
	}

	GenerateBishopMoves(&moves, board2, whiteBishopsB2, true, E8, BothSides, BothSides);
	//printBoard(board2);
	//printMoveList(moves);
	if (moves.size() != 78) {
		std::cout << "White Bishop Move Generation | returned: " << moves.size() << " Expected 78." << std::endl;
	}

	GenerateBishopMoves(&moves, board2, blackBishopsB2, false, E1, BothSides, BothSides);
	//printBoard(board2);
	//printMoveList(moves);
	if (moves.size() != 83) {
		std::cout << "Black Bishop Move Generation | returned: " << moves.size() << " Expected 83." << std::endl;
	}

	GenerateKnightMoves(&moves, board3, whiteKnightsB3, true, E8, BothSides, BothSides);
	//printBoard(board2);
	//printMoveList(moves);
	if (moves.size() != 92) {
		std::cout << "White Knight Move Generation | returned: " << moves.size() << " Expected 78." << std::endl;
	}

	GenerateKnightMoves(&moves, board3, blackKnightB3, false, E1, BothSides, BothSides);
	//printBoard(board3);
	//printMoveList(moves);
	if (moves.size() != 97) {
		std::cout << "Black Knight Move Generation | returned: " << moves.size() << " Expected 83." << std::endl;
	}

	GenerateQueenMoves(&moves, board3, whiteQueensB3, true, E8, BothSides, BothSides);
	//printBoard(board3);
	//printMoveList(moves);
	if (moves.size() != 115) {
		std::cout << "White Queen Move Generation | returned: " << moves.size() << " Expected 115." << std::endl;
	}

	GenerateQueenMoves(&moves, board3, blackQueenB3, false, E1, BothSides, BothSides);
	//printBoard(board3);
	//printMoveList(moves);
	if (moves.size() != 131) {
		std::cout << "Black Queen Move Generation | returned: " << moves.size() << " Expected 131." << std::endl;
	}
}
*/