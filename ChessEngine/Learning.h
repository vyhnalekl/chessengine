#pragma once
#include "stdafx.h"
#include <fstream>
#include "MoveList.h"
#include "Board.h"
#include "communication.h"
#include "Debug.h"
void searchOpenings(Board *, unsigned long long, MoveList*, bool, bool*);
void saveMove(unsigned long long , Move , int , bool );
void searchFile(Board *,unsigned long long , MoveList* , bool , bool*, int, bool);
void printFile(bool);