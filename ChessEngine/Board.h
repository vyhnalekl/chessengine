#pragma once
#ifndef BOARD_H
#define BOARD_H

#include "stdafx.h"
#include "Definitions.cpp"
#include "GenerateMove.h"

class Board {
public:
	char pieces[CHESSBOARD_LENGTH];
	bool whiteOnMove = false; // who is on move
	MoveList history = MoveList(MAX_HISTORY_MOVES);
	char numberOfMovesWithoutTake = 0; // TODO tests
	char enPassant = NONE;
	unsigned long long undoCount = 0; //TODO DEBUG REMOVE
	unsigned long long playCount = 0; //TODO DEBUG REMOVE
	//unsigned long long key = 0; // TODO remove (comment)
	unsigned long long tHelpAllTime = 0;
	unsigned int tCollisionAllTime = 0;
	int tSize = 0;
	Move bestMove;
	//Move lastUndid[2]; // TODO debug remove
	int ply = 0;
	//short figureScore = 0;
	short sumOfWhitePieces = 800 + 600 + 600 + 1000 + 900;
	short sumOfBlackPieces = sumOfWhitePieces;
	//Castle
	char whiteCastleAlowed = BothSides; // Whether Castle can be made
	char blackCastleAlowed = BothSides;
	//MoveList pvMoves = MoveList(30);
	// pieces on desk
	char whitePiecesList[6][MAX_NUMBER_OF_PIECES] = { { A2, B2, C2, D2, E2, F2, G2, H2, NONE, NONE }, // pawns
	{ B1, G1, NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE }, // knight
	{ C1, F1, NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE }, // bishop
	{ A1, H1, NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE }, // rook
	{ D1, NONE ,NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE }, // queen
	{ E1, NONE ,NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE } }; // extreme case MAX_NUMBER_OF_PIECES rooks, index - 1
	char blackPiecesList[6][MAX_NUMBER_OF_PIECES] = { { A7, B7, C7, D7, E7, F7, G7, H7, NONE, NONE },
	{ B8, G8, NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE },
	{ C8, F8, NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE },
	{ A8, H8, NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE },
	{ D8, NONE ,NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE },
	{ E8, NONE ,NONE, NONE, NONE, NONE, NONE, NONE,NONE,NONE } }; // index - 8

																  //unsigned long long piecesOnBoard; // every bit represents one square on chessboard
																  // if it is set to 1 piece is there
public:														  // if it is set to 0 no piece there
	Board();
	void GenerateMoves(MoveList*);
	void GenerateCapturingMoves(MoveList *);
	int PlayMove(Move*);
	void UndoMove();
	void setPieces(char[CHESSBOARD_LENGTH]);
	void setWhitePieceList(char[6][MAX_NUMBER_OF_PIECES]);
	void setBlackPieceList(char[6][MAX_NUMBER_OF_PIECES]);
	void switchWhiteOnMove();
	char* getPieces();
	bool isRepeating(unsigned long long);
	bool FiftyWithoutTake();
	bool isDraw();
	void SetFenPosition(const char *);
	//unsigned long long getPossitionKey(); TODO think about more efficient key representation
	// you cannot just key the generated key, you can either store the key value, but then you have to update it with every move
	// or you can generate position key each time you insert a record into move table
private:
	void doEnPassantMove(Move*);
	void doTransformationMove(Move*);
	void doCastelingMove(Move*);
	void doMove(Move*);
	void undoEnPassantMove(Move*);
	void undoTransformationMove(Move*);
	void undoCastelingMove(Move*);
	void undoMove(Move*);
	void updateCastelingOptions(Move*);
	
};
#endif