#pragma once
#include "Definitions.cpp"
#include "Board.h"
#include "Bitboard.h"
void initializeTable();
void addRecord( unsigned long long , Transposition *);
Transposition getRecord(unsigned long long );
Transposition simpleSearch(Board*, SearchData *, int);
Transposition AlphaBetaSearch(SearchData *, int, int, Bitboard*);
int alphaBetaMax(Board*, SearchData*, int, int, int);
int alphaBetaMin(Board*, SearchData*, int, int, int);
int iterativeDeepening(SearchData *, Bitboard*);