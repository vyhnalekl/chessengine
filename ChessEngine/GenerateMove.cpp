#include "stdafx.h"
#include "Definitions.cpp"
#include <iostream>
#include "MoveList.h"
#include "Debug.h"

#ifndef GENERATEMOVE_CPP
#define GENERATEMOVE_CPP
int valueOfPiece[] = { 0,100,300,300,500,900,6000,100,300,300,500,900,6000 }; //TODO check the effects previously int valueOfPiece[] = { 0,100,200,300,400,500,600,100,200,300,400,500,600 };
int valueOfCapturingPiece[] = { 0,1,2,2,4,5,6,1,2,2,4,5,6 };
bool pawnThreathens(char myPiece, char opponentPiece) {
	if (myPiece < BLACK_OFFSET) {
		//white
		return (myPiece + PAWN_THREATHENS_LEFT == opponentPiece)
			|| (myPiece + PAWN_THREATHENS_RIGHT == opponentPiece);
	}
	else {
		// black - negative offsets
		return (myPiece - PAWN_THREATHENS_LEFT == opponentPiece)
			|| (myPiece - PAWN_THREATHENS_RIGHT == opponentPiece);
	}
}

bool knightThreathens(char myPiece, char opponentPiece) {
	for (int i = 0; i < KNIGHT_OFFSET_LENGTH; i++) {
		if (myPiece + knightOffsets[i] == opponentPiece) {
			return true;
		}
	}
	return false;
}

bool bishopThreathens(char pieces[], char myPiece, char opponentPiece) {
	for (int i = 0; i < BISHOP_OFFSET_LENGTH; i++) {
		int j = 1;
		while ((myPiece + (bishopOffsets[i] * j) < 21) ||
			(myPiece + (bishopOffsets[i] * j) < 98)) { // you are not checking places on left and right side of board
			if (myPiece + (bishopOffsets[i] * j) == opponentPiece) {
				return true;
			}
			else if (pieces[myPiece + (bishopOffsets[i] * j)] != EMPTY) {
				break;
			}
			j++;
		}
	}
	return false;
}

bool rookThreathens(char pieces[], char myPiece, char opponentPiece) {
	for (int i = 0; i < ROOK_OFFSET_LENGTH; i++) {
		int j = 1;
		while ((myPiece + (rookOffsets[i] * j) < 21) ||
			(myPiece + (rookOffsets[i] * j) < 98)) { // you are not checking places on left and right side of board
			if (myPiece + (rookOffsets[i] * j) == opponentPiece) {
				return true;
			}
			else if (pieces[myPiece + (rookOffsets[i] * j)] != EMPTY) {
				break;
			}
			j++;
		}
	}
	return false;
}

bool queenThreathens(char pieces[], char myPiece, char opponentPiece) {
	for (int i = 0; i < QUEEN_OFFSET_LENGTH; i++) {
		int j = 1;
		while ((myPiece + (queenOffsets[i] * j) < 21) ||
			(myPiece + (queenOffsets[i] * j) < 98)) { // you are not checking places on left and right side of board
														// but it is not nessesary because opponentPiece should be on the board
			if (myPiece + (queenOffsets[i] * j) == opponentPiece) {
				return true;
			}
			else if (pieces[myPiece + (queenOffsets[i] * j)] != EMPTY) {
				break;
			}
			j++;
		}
	}
	return false;
}

bool kingThreathens(char myPiece, char opponentPiece) {
	// whether king can move
	for (int i = 0; i < KING_OFFSET_LENGTH; i++) {
		if (myPiece + kingOffsets[i] == opponentPiece) {
			return true;
		}
	}
	return false;
}

void GeneratePawnMoves(MoveList* moves, char pieces[], char pawns[], bool white, char enPassant) {
	// white = what color pieces have
	// pieces = chessboard
	// pawns = all pawns
	// moves = possible moves will be added to this list
	// opKingPos = opponent king possition (for check detection)
	char offset = (white) ? PAWN_OFFSET : PAWN_OFFSET * -1; // black have negative offset
	char threathensLeftOffset = (white) ? PAWN_THREATHENS_LEFT : PAWN_THREATHENS_LEFT * -1;
	char threathensRightOffset = (white) ? PAWN_THREATHENS_RIGHT : PAWN_THREATHENS_RIGHT * -1;
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {

		struct Move mv;
		char square = pawns[i];

		// chech whether is pawn acctualy on board
		if (square == NONE) {
			continue;
		}//TODO delete

		if (pieces[square + offset] == EMPTY) { // whether pawn can move
			// when pawn is on last row
			if (((white) && (81 <= square && square <= 88)) // white and on last line
				|| ((!white) && (31 <= square && square <= 38))) { // black and on last line
				int pieceOffset = (white) ? 2 : BLACK_OFFSET + 1; // cannot be transformed into pawn
				for (int i = 0; i < 4; i++) {//4 pieces that can be transformed
					mv.from = square;
					mv.to = square + offset;
					//mv.check = pawnThreathens(mv.to, opKingPos); // TODO checks after transformation
					mv.enPassant = NONE;
					mv.transforms = pieceOffset + i;
					//mv.createdBy = 'p';
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
			}
			else {
				// clasic move
				if ((white && (31 <= square && square <= 38)) // white and on first line
					|| ((!white) && (81 <= square && square <= 88))) { // black and on second line
					// move by 2 squares												   //initial move (can move by 2)
					if (pieces[square + 2 * offset] == EMPTY) {
						mv.from = square;
						mv.to = square + 2 * offset;
						//mv.check = pawnThreathens(mv.to, opKingPos);
						mv.enPassant = square + offset;
						mv.transforms = EMPTY;
						//mv.createdBy = 'p';
						////checkMove(mv, pieces);
						(*moves).pushMove(mv);
					}
				}
				// move by one square
				mv.from = square;
				mv.to = square + offset;
				//mv.check = pawnThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'p';
				////checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
		int opponentPiecesFrom = (white) ? BLACK_OFFSET : 1; // starts with the equal piece
		int opponentPiecesTo = opponentPiecesFrom + 5; // must be smaller than this (this is the king)
		// moves by taking opponent piece
		if (((offset > 0) && (81 <= square && square <= 88)) // white and on last line
			|| ((offset < 0) && (31 <= square && square <= 38))) { // black and on last line
			int pieceOffset = (white) ? 2 : BLACK_OFFSET + 1; // cannot be transformed into pawn
			if ((opponentPiecesFrom <= pieces[square + threathensLeftOffset]) &&
				(pieces[square + threathensLeftOffset] < opponentPiecesTo)) {

				for (int i = 0; i < 4; i++) {//4 pieces that can be transformed
					mv.from = square;
					mv.to = square + threathensLeftOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = pieceOffset + i;
					mv.score = valueOfPiece[pieces[square + threathensLeftOffset]] + 6;
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
			}
			if ((opponentPiecesFrom <= pieces[square + threathensRightOffset]) &&
				(pieces[square + threathensRightOffset] < opponentPiecesTo)) {
				for (int i = 0; i < 4; i++) {//4 pieces that can be transformed
					mv.from = square;
					mv.to = square + threathensRightOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = pieceOffset + i;
					mv.score = valueOfPiece[pieces[square + threathensRightOffset]] + 6;
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
			}
		}
		else {
			if ((opponentPiecesFrom <= pieces[square + threathensLeftOffset]) &&
				(pieces[square + threathensLeftOffset] < opponentPiecesTo)) {
				mv.from = square;
				mv.to = square + threathensLeftOffset;
				//mv.check = pawnThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'p';
				//mv.capturedPiece = pieces[square + threathensLeftOffset]; // TODO remove
				mv.score = valueOfPiece[pieces[square + threathensLeftOffset]] + 5;
				////checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			if ((opponentPiecesFrom <= pieces[square + threathensRightOffset]) &&
				(pieces[square + threathensRightOffset] < opponentPiecesTo)) {
				mv.from = square;
				mv.to = square + threathensRightOffset;
				//mv.check = pawnThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[square + threathensRightOffset];
				mv.score = valueOfPiece[pieces[square + threathensRightOffset]] + 5;
				//mv.createdBy = 'p';
				////checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			// en Passant move
			if (enPassant != NONE) {

				if (square + threathensLeftOffset == enPassant) {
					mv.from = square;
					mv.to = square + threathensLeftOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = EMPTY;
					mv.isEnPassantMove = true;
					//mv.createdBy = 'p';
					mv.score = 105;
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
				if (square + threathensRightOffset == enPassant) {
					mv.from = square;
					mv.to = square + threathensRightOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = EMPTY;
					mv.isEnPassantMove = true;
					//mv.createdBy = 'p';
					mv.score = 105;
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
			}
		}
	}
}

void GenerateBishopMoves(MoveList* moves, char pieces[], char bishops[], bool white) {
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = bishops[i];

		if (square == NONE) {
			continue;
		}
		struct Move mv;
		for (int j = 0; j < BISHOP_OFFSET_LENGTH; j++) {
			int scale = 1;

			// all moves when there is no piece in way
			while (pieces[square + (bishopOffsets[j] * scale)] == EMPTY) {
				mv.from = square;
				mv.to = square + (bishopOffsets[j] * scale);
				//mv.check = bishopThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'b';
				////checkMove(mv, pieces);
				(*moves).pushMove(mv);
				scale++;
			}

			// can this piece be taken
			if (white && (pieces[square + (bishopOffsets[j] * scale)] >= BLACK_OFFSET)
				&& (pieces[square + (bishopOffsets[j] * scale)] < PIECES_ENUM_LENGTH - 1)) {
				mv.from = square;
				mv.to = square + (bishopOffsets[j] * scale);
				//mv.check = bishopThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'b';
				mv.score = valueOfPiece[pieces[square + (bishopOffsets[j] * scale)]] + 3;
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			else if ((!white) && (pieces[square + (bishopOffsets[j] * scale)] < BLACK_OFFSET - 1)
				&& (pieces[square + (bishopOffsets[j] * scale)] > 0)) {
				mv.from = square;
				mv.to = square + (bishopOffsets[j] * scale);
				//mv.check = bishopThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'b';
				mv.score = valueOfPiece[pieces[square + (bishopOffsets[j] * scale)]] + 3;
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
	}
}

void GenerateKnightMoves(MoveList* moves, char pieces[], char knights[], bool white) {

	int opponentPieceFrom = (white) ? BLACK_OFFSET : 1;
	int opponentPieceTo = opponentPieceFrom + 5;

	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = knights[i];
		if (square == NONE) {
			continue;
		}

		struct Move mv;

		for (int j = 0; j < KNIGHT_OFFSET_LENGTH; j++) {
			if (pieces[square + knightOffsets[j]] == EMPTY) { // Empty square
				mv.from = square;
				mv.to = square + knightOffsets[j];
				//mv.check = knightThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'k';
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			else if ((opponentPieceFrom <= pieces[square + knightOffsets[j]])
				&& (pieces[square + knightOffsets[j]] < opponentPieceTo)) { // or opponent piece
				mv.from = square;
				mv.to = square + knightOffsets[j];
				//mv.check = knightThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'k';
				//mv.capturedPiece = pieces[mv.to];
				mv.score = valueOfPiece[pieces[square + knightOffsets[j]]] + 4;
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
	}
}

void GenerateRookMoves(MoveList* moves, char pieces[], char rooks[], bool white) {
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = rooks[i];

		if (square == NONE) {
			continue;
		}

		struct Move mv;
		for (int j = 0; j < ROOK_OFFSET_LENGTH; j++) {
			int scale = 1;

			// all moves when there is no piece in way
			while (pieces[square + (rookOffsets[j] * scale)] == EMPTY) {
				mv.from = square;
				mv.to = square + (rookOffsets[j] * scale);
				//mv.check = rookThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'k';
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
				scale++;
			}

			// can this piece be taken
			if (white && (pieces[square + (rookOffsets[j] * scale)] >= BLACK_OFFSET)
				&& (pieces[square + (rookOffsets[j] * scale)] < PIECES_ENUM_LENGTH - 1)) {
				mv.from = square;
				mv.to = square + (rookOffsets[j] * scale);
				//mv.check = rookThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'r';
				mv.score = valueOfPiece[pieces[square + (rookOffsets[j] * scale)]] + 2;
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			else if ((!white) && (pieces[square + (rookOffsets[j] * scale)] < BLACK_OFFSET - 1)
				&& (pieces[square + (rookOffsets[j] * scale)] > 0)) {
				mv.from = square;
				mv.to = square + (rookOffsets[j] * scale);
				//mv.check = rookThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'r';
				mv.score = valueOfPiece[pieces[square + (rookOffsets[j] * scale)]] + 2;
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
	}
}

void GenerateQueenMoves(MoveList* moves, char pieces[], char queens[], bool white) {
	int opponentPieceFrom = (white) ? BLACK_OFFSET : 1;
	int opponentPieceTo = opponentPieceFrom + 5;
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = queens[i];

		if (square == NONE) {
			continue;
		}

		struct Move mv;
		for (int j = 0; j < QUEEN_OFFSET_LENGTH; j++) {
			int scale = 1;

			// all moves when there is no piece in way
			while (pieces[square + (queenOffsets[j] * scale)] == EMPTY) {
				mv.from = square;
				mv.to = square + (queenOffsets[j] * scale);
				//mv.check = queenThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'q';
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
				scale++;
			}

			// can this piece be taken
			if ((pieces[square + (queenOffsets[j] * scale)] >= opponentPieceFrom)
				&& (pieces[square + (queenOffsets[j] * scale)] < opponentPieceTo)) {
				mv.from = square;
				mv.to = square + (queenOffsets[j] * scale);
				//mv.check = queenThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'q';
				mv.score = valueOfPiece[pieces[square + (rookOffsets[j] * scale)]] + 1;
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}/*
			else if ((!white) && (pieces[square + (queenOffsets[j] * scale)] < BLACK_OFFSET)
				&& (pieces[square + (queenOffsets[j] * scale)] > 0)) {
				mv.from = square;
				mv.to = square + (queenOffsets[j] * scale);
				//mv.check = queenThreathens(pieces, mv.to, opKingPos);
				mv.currentCastleBlack = ccBlack;
				mv.currentCastleWhite = ccWhite;
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				(*moves).pushMove(mv);
			}*/
		}
	}
}

bool OpponentThreathens(char pieces[], char opPieces[6][MAX_NUMBER_OF_PIECES], char square) {
	int i = 0;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (opPieces[i][j] != NONE) {
			if (pawnThreathens(opPieces[i][j], square)) {
				return true;
			}
		}
	}
	i++;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (opPieces[i][j] != NONE) {
			if (knightThreathens(opPieces[i][j], square)) {
				return true;
			}
		}
	}
	i++;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (opPieces[i][j] != NONE) {
			if (bishopThreathens(pieces, opPieces[i][j], square)) {
				return true;
			}
		}
	}
	i++;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (opPieces[i][j] != NONE) {
			if (rookThreathens(pieces, opPieces[i][j], square)) {
				return true;
			}
		}
	}
	i++;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (opPieces[i][j] != NONE) {
			if (queenThreathens(pieces, opPieces[i][j], square)) {
				return true;
			}
		}
	}
	i++;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (opPieces[i][j] != NONE) {
			if (kingThreathens(opPieces[i][j], square)) {
				return true;
			}
		}
	}
	return false;
}

void GenerateKingMoves(MoveList* moves, char pieces[], char king, bool white) {
	if (king == NONE) {
		printBoard(pieces);
		return;
	}

	int opponentPieceFrom = (white) ? BLACK_OFFSET : 1;
	int opponentPieceTo = opponentPieceFrom + 5;

	struct Move mv;

	for (int j = 0; j < KING_OFFSET_LENGTH; j++) {
		if (pieces[king + kingOffsets[j]] == EMPTY) { // Empty square
			//if (!OpponentThreathens(pieces, opponentPieces, king + kingOffsets[j])) {
			mv.from = king;
			mv.to = king + kingOffsets[j];
			//mv.check = false;
			mv.enPassant = NONE;
			mv.transforms = EMPTY;
			//mv.createdBy = 'K';
			//checkMove(mv, pieces);
			(*moves).pushMove(mv);
			//}
		}
		else if ((opponentPieceFrom <= pieces[king + kingOffsets[j]])
			&& (pieces[king + kingOffsets[j]] < opponentPieceTo)) { // or opponent piece
			//if (!OpponentThreathens(pieces, opponentPieces, king + kingOffsets[j])) {
			mv.from = king;
			mv.to = king + kingOffsets[j];
			//mv.check = false;
			mv.enPassant = NONE;
			mv.transforms = EMPTY;
			//mv.capturedPiece = pieces[mv.to];
			mv.score = valueOfPiece[pieces[mv.to]];
			//mv.createdBy = 'K';
			//checkMove(mv, pieces);
			(*moves).pushMove(mv);
			//}
		}
	}
}

void GenerateCastelingMoves(MoveList* moves, char pieces[], char king, bool white, char opKingPos, char opponentPieces[6][MAX_NUMBER_OF_PIECES]
	, char castelingOptions) {
	struct Move mv;
	char offset = (white) ? 0 : 70;
	char rook = (white) ? whiteRook : blackRook;
	if (E1 + offset != king) {
		return;
	}

	if (castelingOptions == BothSides || castelingOptions == KingSide) {
		//king side castelling
		if (!(OpponentThreathens(pieces, opponentPieces, F1 + offset) ||
			OpponentThreathens(pieces, opponentPieces, G1 + offset) ||
			OpponentThreathens(pieces, opponentPieces, E1 + offset))
			&& pieces[G1 + offset] == EMPTY && pieces[F1 + offset] == EMPTY
			&& pieces[H1 + offset] == rook) {
			//Debug
			/*if (pieces[H1 + offset] != whiteRook && pieces[H1 + offset] != blackRook) {
				printBoard(pieces);
				std::cout << "ERROR Casteling when rook is not in place" << std::endl;
			}*/

			mv.from = E1 + offset;
			mv.to = G1 + offset;
			////mv.check = rookThreathens(pieces, F1 + offset, opKingPos);
			mv.enPassant = NONE;
			mv.transforms = EMPTY;
			//mv.createdBy = 'c';
			//checkMove(mv, pieces);
			mv.isCastelingMove = true;
			(*moves).pushMove(mv);
		}
	}
	if (castelingOptions == BothSides || castelingOptions == QueenSide) {
		if (!(OpponentThreathens(pieces, opponentPieces, E1 + offset) ||
			OpponentThreathens(pieces, opponentPieces, D1 + offset) ||
			OpponentThreathens(pieces, opponentPieces, C1 + offset) ||
			OpponentThreathens(pieces, opponentPieces, B1 + offset))
			&& pieces[B1 + offset] == EMPTY && pieces[C1 + offset] == EMPTY && pieces[D1 + offset] == EMPTY
			&& pieces[A1 + offset] == rook) {
			mv.from = E1 + offset;
			mv.to = C1 + offset;
			//Debug
			/*if (pieces[H1 + offset] != whiteRook && pieces[H1 + offset] != blackRook) {
				printBoard(pieces);
				std::cout << "ERROR Casteling when rook is not in place" << std::endl;
			}*/

			//mv.check = rookThreathens(pieces, D1 + offset, opKingPos);

			mv.enPassant = NONE;
			mv.transforms = EMPTY;
			//mv.createdBy = 'c';
			//checkMove(mv, pieces);
			mv.isCastelingMove = true;
			(*moves).pushMove(mv);
		}
	}

}

bool isCheck(char king, char pieces[CHESSBOARD_LENGTH]) {
	bool white = (pieces[king] < BLACK_OFFSET);
	char opponentKing = (white) ? blackKing : whiteKing;
	char opponentKnight = (white) ? blackKnight : whiteKnight; // OPPONENT
	char opponentPawn = (white) ? blackPawn : whitePawn;
	char opponentBishop = (white) ? blackBishop : whiteBishop;
	char opponentRook = (white) ? blackRook : whiteRook;
	char opponentQueen = (white) ? blackQueen : whiteQueen;
	char pawnThreathensLeft = (white) ? PAWN_THREATHENS_LEFT : PAWN_THREATHENS_LEFT * -1;
	char pawnThreathensRight = (white) ? PAWN_THREATHENS_RIGHT : PAWN_THREATHENS_RIGHT * -1;
	//std::list<struct ThreatheningPiece> threatheningPieces;
	//ThreatheningPiece temp;
	for (int i = 0; i < KNIGHT_OFFSET_LENGTH; i++) {
		if (pieces[king + knightOffsets[i]] == opponentKnight) {
			return true;
			//temp.possition = king + knightOffsets[i];
			//temp.squaresInWay[0] = king + knightOffsets[i];
			//temp.squaresInWay[1] = NONE;
			//threatheningPieces.push_front(temp);
		}
	}
	for (int i = 0; i < QUEEN_OFFSET_LENGTH; i++) {
		int scale = 1;
		while (pieces[king + queenOffsets[i] * scale] == EMPTY) {
			scale++;
		}
		if (scale == 1) {
			if (pieces[king + queenOffsets[i] * scale] == opponentKing) { // king
				return true;
			}
			if (pieces[king + queenOffsets[i] * scale] == opponentPawn
				&& (queenOffsets[i] == pawnThreathensLeft || queenOffsets[i] == pawnThreathensRight)) {
				//temp.possition = king + queenOffsets[i];
				//temp.squaresInWay[0] = king + queenOffsets[i];
				//temp.squaresInWay[1] = NONE;
				//threatheningPieces.push_front(temp);
				//continue;
				return true;
			}
		}
		
		if (pieces[king + queenOffsets[i] * scale] == opponentBishop
			&& (queenOffsets[i] == pawnThreathensLeft || queenOffsets[i] == pawnThreathensRight
				|| queenOffsets[i] == pawnThreathensLeft * -1 || queenOffsets[i] == pawnThreathensRight * -1)) {
			/*temp.possition = king + queenOffsets[i] * scale;
			for (int j = 0; j <= scale; j++) {
				if (j == scale) {
					temp.squaresInWay[j] = NONE;
				}
				else {
					temp.squaresInWay[j] = king + queenOffsets[i] * (scale - j);
				}
			}
			threatheningPieces.push_front(temp);
			continue;*/
			return true;
		}
		else if (pieces[king + queenOffsets[i] * scale] == opponentRook
			&& (queenOffsets[i] == 10 || queenOffsets[i] == 1
				|| queenOffsets[i] == -10 || queenOffsets[i] == -1)) { // rook Offsets
			/*temp.possition = king + queenOffsets[i] * scale;
			for (int j = 0; j <= scale; j++) {
				if (j == scale) {
					temp.squaresInWay[j] = NONE;
				}
				else {
					temp.squaresInWay[j] = king + queenOffsets[i] * (scale - j);
				}
			}
			threatheningPieces.push_front(temp);
			continue;*/
			return true;
		}
		else if (pieces[king + queenOffsets[i] * scale] == opponentQueen) {
			//no need to check directions
			//temp.possition = king + queenOffsets[i] * scale;
			//for (int j = 0; j <= scale; j++) {
			//	if (j == scale) {
			//		temp.squaresInWay[j] = NONE;
			//	}
			//	else {
			//		temp.squaresInWay[j] = king + queenOffsets[i] * (scale - j);
			//	}
			//}
			//threatheningPieces.push_front(temp);
			//continue;
			return true;
		}
	}
	//return threatheningPieces;
	return false;
}

void GeneratePawnCaptureMoves(MoveList* moves, char pieces[], char pawns[], bool white, char enPassant) {
	// white = what color pieces have
	// pieces = chessboard
	// pawns = all pawns
	// moves = possible moves will be added to this list
	// opKingPos = opponent king possition (for check detection)
	//char offset = (white) ? PAWN_OFFSET : PAWN_OFFSET * -1; // black have negative offset
	char threathensLeftOffset = (white) ? PAWN_THREATHENS_LEFT : -PAWN_THREATHENS_LEFT;
	char threathensRightOffset = (white) ? PAWN_THREATHENS_RIGHT : -PAWN_THREATHENS_RIGHT;
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {

		struct Move mv;
		char square = pawns[i];

		// chech whether is pawn acctualy on board
		if (square == NONE) {
			continue;
		}

		
		int opponentPiecesFrom = (white) ? BLACK_OFFSET : 1; // starts with the equal piece
		int opponentPiecesTo = opponentPiecesFrom + 5; // must be smaller than this (this is the king)
		
		if (((white) && (A7 <= square && square <= H7)) // white and on last line
			|| ((!white) && (A2 <= square && square <= H2))) { // black and on last line
			int pieceOffset = (white) ? 2 : BLACK_OFFSET + 1; // cannot be transformed into pawn
			if ((opponentPiecesFrom <= pieces[square + threathensLeftOffset]) &&
				(pieces[square + threathensLeftOffset] < opponentPiecesTo)) {
				for (int i = 0; i < 4; i++) {//4 pieces that can be transformed
					mv.from = square;
					mv.to = square + threathensLeftOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = pieceOffset + i;
					mv.score = valueOfPiece[pieces[square + threathensLeftOffset]] + valueOfPiece[mv.transforms] - valueOfCapturingPiece[1];
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
			}
			if ((opponentPiecesFrom <= pieces[square + threathensRightOffset]) &&
				(pieces[square + threathensRightOffset] < opponentPiecesTo)) {
				for (int i = 0; i < 4; i++) {//4 pieces that can be transformed
					mv.from = square;
					mv.to = square + threathensRightOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = pieceOffset + i;
					mv.score = valueOfPiece[pieces[square + threathensRightOffset]] - valueOfCapturingPiece[1];
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
			}
		}
		else {
			if ((opponentPiecesFrom <= pieces[square + threathensLeftOffset]) &&
				(pieces[square + threathensLeftOffset] < opponentPiecesTo)) {
				mv.from = square;
				mv.to = square + threathensLeftOffset;
				//mv.check = pawnThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'p';
				//mv.capturedPiece = pieces[square + threathensLeftOffset]; // TODO remove
				mv.score = valueOfPiece[pieces[square + threathensLeftOffset]] - valueOfCapturingPiece[1];
				////checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			if ((opponentPiecesFrom <= pieces[square + threathensRightOffset]) &&
				(pieces[square + threathensRightOffset] < opponentPiecesTo)) {
				mv.from = square;
				mv.to = square + threathensRightOffset;
				//mv.check = pawnThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[square + threathensRightOffset];
				mv.score = valueOfPiece[pieces[square + threathensRightOffset]] - valueOfCapturingPiece[1];
				//mv.createdBy = 'p';
				////checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			// en Passant move
			if (enPassant != NONE) {

				if (square + threathensLeftOffset == enPassant) {
					mv.from = square;
					mv.to = square + threathensLeftOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = EMPTY;
					mv.isEnPassantMove = true;
					//mv.createdBy = 'p';
					mv.score = 50;
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
				if (square + threathensRightOffset == enPassant) {
					mv.from = square;
					mv.to = square + threathensRightOffset;
					//mv.check = pawnThreathens(mv.to, opKingPos);
					mv.enPassant = NONE;
					mv.transforms = EMPTY;
					mv.isEnPassantMove = true;
					//mv.createdBy = 'p';
					mv.score = 50;
					////checkMove(mv, pieces);
					(*moves).pushMove(mv);
				}
			}
		}
	}
}

void GenerateBishopCaptureMoves(MoveList* moves, char pieces[], char bishops[], bool white) {
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = bishops[i];

		if (square == NONE) {
			continue;
		}
		struct Move mv;
		for (int j = 0; j < BISHOP_OFFSET_LENGTH; j++) {
			int scale = 1;

			// all moves when there is no piece in way
			while (pieces[square + (bishopOffsets[j] * scale)] == EMPTY) {
				scale++;
			}

			// can this piece be taken
			if (white && (pieces[square + (bishopOffsets[j] * scale)] >= BLACK_OFFSET)
				&& (pieces[square + (bishopOffsets[j] * scale)] < PIECES_ENUM_LENGTH - 1)) {
				mv.from = square;
				mv.to = square + (bishopOffsets[j] * scale);
				//mv.check = bishopThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'b';
				mv.score = valueOfPiece[pieces[square + (bishopOffsets[j] * scale)]] - valueOfCapturingPiece[whiteBishop];
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			else if ((!white) && (pieces[square + (bishopOffsets[j] * scale)] < BLACK_OFFSET - 1)
				&& (pieces[square + (bishopOffsets[j] * scale)] > 0)) {
				mv.from = square;
				mv.to = square + (bishopOffsets[j] * scale);
				//mv.check = bishopThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'b';
				mv.score = valueOfPiece[pieces[square + (bishopOffsets[j] * scale)]] - valueOfCapturingPiece[whiteBishop];
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
	}
}

void GenerateKnightCaptureMoves(MoveList* moves, char pieces[], char knights[], bool white) {

	int opponentPieceFrom = (white) ? BLACK_OFFSET : 1;
	int opponentPieceTo = opponentPieceFrom + 5;

	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = knights[i];
		if (square == NONE) {
			continue;
		}

		struct Move mv;

		for (int j = 0; j < KNIGHT_OFFSET_LENGTH; j++) {
			if ((opponentPieceFrom <= pieces[square + knightOffsets[j]])
				&& (pieces[square + knightOffsets[j]] < opponentPieceTo)) { // or opponent piece
				mv.from = square;
				mv.to = square + knightOffsets[j];
				//mv.check = knightThreathens(mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.createdBy = 'k';
				//mv.capturedPiece = pieces[mv.to];
				mv.score = valueOfPiece[pieces[square + knightOffsets[j]]] - valueOfCapturingPiece[whiteKnight];
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
	}
}

void GenerateRookCaptureMoves(MoveList* moves, char pieces[], char rooks[], bool white) {
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = rooks[i];

		if (square == NONE) {
			continue;
		}

		struct Move mv;
		for (int j = 0; j < ROOK_OFFSET_LENGTH; j++) {
			int scale = 1;

			// all moves when there is no piece in way
			while (pieces[square + (rookOffsets[j] * scale)] == EMPTY) {
				scale++;
			}

			// can this piece be taken
			if (white && (pieces[square + (rookOffsets[j] * scale)] >= BLACK_OFFSET)
				&& (pieces[square + (rookOffsets[j] * scale)] < PIECES_ENUM_LENGTH - 1)) {
				mv.from = square;
				mv.to = square + (rookOffsets[j] * scale);
				//mv.check = rookThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'r';
				mv.score = valueOfPiece[pieces[square + (rookOffsets[j] * scale)]] - valueOfCapturingPiece[whiteRook];
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
			else if ((!white) && (pieces[square + (rookOffsets[j] * scale)] < BLACK_OFFSET - 1)
				&& (pieces[square + (rookOffsets[j] * scale)] > 0)) {
				mv.from = square;
				mv.to = square + (rookOffsets[j] * scale);
				//mv.check = rookThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'r';
				mv.score = valueOfPiece[pieces[square + (rookOffsets[j] * scale)]] - valueOfCapturingPiece[whiteRook];
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
	}
}

void GenerateQueenCaptureMoves(MoveList* moves, char pieces[], char queens[], bool white) {
	int opponentPieceFrom = (white) ? BLACK_OFFSET : 1;
	int opponentPieceTo = opponentPieceFrom + 5;
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char square = queens[i];

		if (square == NONE) {
			continue;
		}

		struct Move mv;
		for (int j = 0; j < QUEEN_OFFSET_LENGTH; j++) {
			int scale = 1;

			// all moves when there is no piece in way
			while (pieces[square + (queenOffsets[j] * scale)] == EMPTY) {
				scale++;
			}

			// can this piece be taken
			if ((pieces[square + (queenOffsets[j] * scale)] >= opponentPieceFrom)
				&& (pieces[square + (queenOffsets[j] * scale)] < opponentPieceTo)) {
				mv.from = square;
				mv.to = square + (queenOffsets[j] * scale);
				//mv.check = queenThreathens(pieces, mv.to, opKingPos);
				mv.enPassant = NONE;
				mv.transforms = EMPTY;
				//mv.capturedPiece = pieces[mv.to];
				//mv.createdBy = 'q';
				mv.score = valueOfPiece[pieces[square + (rookOffsets[j] * scale)]] - valueOfCapturingPiece[whiteQueen];
				//checkMove(mv, pieces);
				(*moves).pushMove(mv);
			}
		}
	}
}

void GenerateKingCaptureMoves(MoveList* moves, char pieces[], char king, bool white) {
	if (king == NONE) {
		printBoard(pieces);
		return;
	}

	int opponentPieceFrom = (white) ? BLACK_OFFSET : 1;
	int opponentPieceTo = opponentPieceFrom + 5;

	struct Move mv;

	for (int j = 0; j < KING_OFFSET_LENGTH; j++) {
		if ((opponentPieceFrom <= pieces[king + kingOffsets[j]])
			&& (pieces[king + kingOffsets[j]] < opponentPieceTo)) { // or opponent piece
																	//if (!OpponentThreathens(pieces, opponentPieces, king + kingOffsets[j])) {
			mv.from = king;
			mv.to = king + kingOffsets[j];
			//mv.check = false;
			mv.enPassant = NONE;
			mv.transforms = EMPTY;
			//mv.capturedPiece = pieces[mv.to];
			mv.score = valueOfPiece[pieces[mv.to]] - valueOfCapturingPiece[whiteKing];
			//mv.createdBy = 'K';
			//checkMove(mv, pieces);
			(*moves).pushMove(mv);
			//}
		}
	}
}

#endif