#include "stdafx.h"
#include <fstream>
#include "MoveList.h"
#include "Board.h"
#include "communication.h"
#include "Debug.h"
/* For opening books */
void searchOpenings(Board *b, unsigned long long key, MoveList* ml, bool white, bool * done) {
	//MoveList* ml = new MoveList(50);
	try{
		std::ifstream f;
		if (white) {
			f.open("white", std::fstream::binary);
		}
		else {
			f.open("black", std::fstream::binary);
		}
		
		unsigned long long n= 0;
		//char * temp = new char[8];
		char * move = new char[6];
		char temp;
		while (!f.eof()) {
			f.read(reinterpret_cast<char*>(&n), sizeof(n));
			if (white) {
				f.read(&temp, 1);
			}
			f.read(move, 5);
			move[5] = '\0';
			//std::cout << n << std::endl;
			//n = temp;
			//std::cout << move << std::endl;
			//std::cout << time
			if (n == key) {
				Move mv;
				ParseMove(move, b, &mv);
				if (checkMoveNoCout(mv, b->pieces)) {
					ml->pushMove(mv);
				}
				else {
					std::cout << move << std::endl;
					std::cout << "Colission" << std::endl;
				}
			
			}
		}
		f.close();
		(*done) = true;
	}
	catch (...) {
		std::cout << "WARNING: Cannot open file with games" << std::endl;
	}
}

void saveMove(unsigned long long key, Move mv, int timeOnMove, bool white) {
	/* 
		Saves possition, move, timeOnMove into binary file
	*/
	try {
		std::ofstream f;
		if (white) {
			f.open("whiteGames", std::ios::app);
		}
		else {
			f.open("blackGames", std::ios::app);
		}
		f.write(reinterpret_cast<char*>(&key), sizeof(key));
		char* temp = PrMove(mv);
		//std::cout << "saved : " << temp << std::endl;
		f.write(temp, 6);
		f.write(reinterpret_cast<char*>(&timeOnMove), sizeof(int));
		/*f << key;
		f << PrMove(mv);
		f << timeOnMove;*/
		//f << std::endl;

		f.close();
	}
	catch (...) {
		std::cout << "WARNING: Cannot open file with games" << std::endl;
	}
}

/* For searching game books */
void searchFile(Board *b, unsigned long long key, MoveList* ml, bool white, bool * done, int timeOnMove, bool start) {
	//MoveList* ml = new MoveList(50);
	if (!start) {
		return;
	}
	try {
		std::ifstream f;
		if (white) {
			f.open("whiteGames", std::fstream::binary);
		}
		else {
			f.open("blackGames", std::fstream::binary);
		}

		unsigned long long n = 0;
		//char * temp = new char[8];
		char * move = new char[6];
		//char temp;
		while (!f.eof()) {
			f.read(reinterpret_cast<char*>(&n), sizeof(unsigned long long));
			if (n == 0) {
				break;
			}
			f.read(move, 6);
			//move[5] = '\0';
			int spendTime = 0;
			f.read(reinterpret_cast<char*>(&spendTime), sizeof(int));
			//std::cout << n << " " << move << " " << spendTime << std::endl;
			if (n == key) {
				if (spendTime * 2 > timeOnMove) {
					Move mv;
					ParseMove(move, b, &mv);
					if (checkMoveNoCout(mv, b->pieces)) {
						ml->pushMove(mv);
						break;
					}
					else {
						std::cout << move << std::endl;
						std::cout << "Colission" << std::endl;
					}
				}
			}
			n = 0;
		}
		free(move);
		f.close();
		(*done) = true;
	}
	catch (...) {
		std::cout << "WARNING: Cannot open file with games" << std::endl;
	}
}
void printFile(bool white) {
	try {
		std::ifstream f;
		if (white) {
			f.open("whiteGames", std::fstream::binary);
		}
		else {
			f.open("blackGames", std::fstream::binary);
		}

		unsigned long long n = 0;
		char * move = new char[6];
		while (!f.eof()) {
			f.read(reinterpret_cast<char*>(&n), sizeof(unsigned long long));
			if (n == 0) {
				break;
			}
			f.read(move, 6);
			//move[5] = '\0';
			int spendTime = 0;
			f.read(reinterpret_cast<char*>(&spendTime), sizeof(int));
			std::cout << n << " " << move << " " << spendTime << std::endl;
		}
		free(move);
		f.close();
	}
	catch (...) {
		std::cout << "WARNING: Cannot open file with games" << std::endl;
	}
}

