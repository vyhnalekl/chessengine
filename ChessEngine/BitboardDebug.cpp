#include "stdafx.h"
#include <iostream>
#include "Definitions.cpp"
#include "BitboardDebug.h"
#include "Bitboard.h"
#include "Debug.h"
#include "Board.h"
#include <bitset>

/*namespace {
	char BitTable[64] = {
  63, 30, 3, 32, 25, 41, 22, 33, 15, 50, 42, 13, 11, 53, 19, 34, 61, 29, 2,
  51, 21, 43, 45, 10, 18, 47, 1, 54, 9, 57, 0, 35, 62, 31, 40, 4, 49, 5, 52,
  26, 60, 6, 23, 44, 46, 27, 56, 16, 7, 39, 48, 24, 59, 14, 12, 55, 38, 28,
  58, 20, 37, 17, 36, 8
	};
	

	char POP(U64 *bb) {
		U64 b = *bb ^ (*bb - 1);
		unsigned int fold = (unsigned)((b & 0xffffffff) ^ (b >> 32));
		*bb &= (*bb - 1);
		return BitTable[(fold * 0x783a9b23) >> 26];
	}
}
*/
namespace {
	char squareTo120(char sq64) {
		int result = A1;
		result += (sq64 / 8) * 10;
		result += sq64 % 8;
		return result;
	}
}

void printBitboard(U64 b) {
	std::cout << std::endl;
	for (int i = 0; i < 8; i++) {
		char a = (char)b;
		std::cout << std::bitset<8>(a) << std::endl;
		b = b >> 8;
	}
	std::cout << std::endl;
}