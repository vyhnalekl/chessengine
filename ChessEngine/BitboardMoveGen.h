

#ifndef BITBOARDMOVEGEN_H
#define BITBOARDMOVEGEN_H

#include "stdafx.h"
//#include "Definitions.cpp"
#include "MoveList.h"
#include <iostream>
#include "BitboardDebug.h"
namespace {
	extern const char BitTable[64] = {
	  63, 30, 3, 32, 25, 41, 22, 33, 15, 50, 42, 13, 11, 53, 19, 34, 61, 29, 2,
	  51, 21, 43, 45, 10, 18, 47, 1, 54, 9, 57, 0, 35, 62, 31, 40, 4, 49, 5, 52,
	  26, 60, 6, 23, 44, 46, 27, 56, 16, 7, 39, 48, 24, 59, 14, 12, 55, 38, 28,
	  58, 20, 37, 17, 36, 8
	};
}
char POP(U64 *bb);

void CountPawnMoves(short*, U64, U64, U64, bool, U64, U64[]);
void CountBishopMoves(short*, U64, U64, U64, char, U64[], char);
void CountKnightMoves(short*, U64, U64, U64, char, U64[], char base);
void CountRookMoves(short*, U64, U64, U64, char, U64[], char);
void CountQueenMoves(short*, U64, U64, U64, char, U64[], char);

void PawnMoves(MoveList*, U64, U64, U64, bool, U64, U64[]);
void BishopMoves(MoveList*, U64, U64, U64, char, U64[], char);
void KnightMoves(MoveList*, U64, U64, U64, char, U64[], char base);
void RookMoves(MoveList*, U64, U64, U64, char, U64[], char);
void QueenMoves(MoveList*, U64, U64, U64, char, U64[], char);
bool Threathens(U64 sq, U64 opponentPieceList[], U64 myPieces, U64 opponentPieces, bool white, char square);
void KingMoves(MoveList*, U64, U64, U64, char, U64[], char base);
void CastelingMoves(MoveList* moves, U64 king, U64 rooks, U64 myPieces, U64 opponentPieces, U64 opponentPieceList[], bool white, char castelingOptions);

void PawnCaptureMoves(MoveList*, U64, U64, U64, bool, U64, U64[]);
void BishopCaptureMoves(MoveList*, U64, U64, U64, char, U64[], char);
void KnightCaptureMoves(MoveList*, U64, U64, U64, char, U64[], char base);
void RookCaptureMoves(MoveList*, U64, U64, U64, char, U64[], char);
void QueenCaptureMoves(MoveList*, U64, U64, U64, char, U64[], char);
void KingCaptureMoves(MoveList*, U64, U64, U64, char, U64[], char base);

//std::list<struct ThreatheningPiece> isCheck(char, char[]);
/*std::list<struct Move> RemoveNonLegalMoves(std::list<struct Move>, ThreatheningPiece);*/
#endif