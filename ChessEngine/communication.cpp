#include "stdafx.h"
#include "Board.h"
#include "Bitboard.h"
#include "Definitions.cpp"
#include <iostream>

void ParseMove(char* mv, Board* b, Move *mov) {
	char from = 21 + ((mv[1] - '1') * 10) + (mv[0] - 'a');
	char to = 21 + ((mv[3] - '1') * 10) + (mv[2] - 'a');
	mov->from = from;
	mov->to = to;
	if (b->pieces[mov->from] == whitePawn && mov->from == mov->to - 20) {
		mov->enPassant = mov->from + 10;
	}
	if (b->pieces[mov->from] == blackPawn && mov->from == mov->to + 20) {
		mov->enPassant = mov->from - 10;
	}
	//if the line above is missing engine cannot detect black casteling move
	if (b->pieces[mov->from] == whiteKing) {
		if ((mov->from == E1 && mov->to == G1) || (mov->from == E1 && mov->to == C1)) {
			mov->isCastelingMove = true;
		}
	}
	if (b->pieces[mov->from] == blackKing) {
		if ((mov->from == E8 && mov->to == G8) || (mov->from == E8 && mov->to == C8)) {
			mov->isCastelingMove = true;
		}
	}
	if (b->pieces[mov->from] == whitePawn) {
		if (mv[4] != '\0') {
			if (mv[4] == 'q') {
				mov->transforms = whiteQueen;
			}
			else if (mv[4] == 'r') {
				mov->transforms = whiteRook;
			}
			else if (mv[4] == 'b') {
				mov->transforms = whiteBishop;
			}
			else if (mv[4] == 'n') {
				mov->transforms = whiteKnight;
			}
		}
	}
	else if (b->pieces[mov->from] == blackPawn) {
		if (mv[4] != '\0') {
			if (mv[4] == 'q') {
				mov->transforms = blackQueen;
			}
			else if (mv[4] == 'r') {
				mov->transforms = blackRook;
			}
			else if (mv[4] == 'b') {
				mov->transforms = blackBishop;
			}
			else if (mv[4] == 'n') {
				mov->transforms = blackKnight;
			}
		}
	}
	if (b->enPassant == mov->to && (b->pieces[mov->from] == whitePawn || b->pieces[mov->from] == blackPawn)) {
		mov->isEnPassantMove = true;
	}
}



char sqTo120(char sq64) {
	int result = A1;
	result += (sq64 / 8) * 10;
	result += sq64 % 8;
	return result;
}

char *PrMove(Move move) {

	static char MvStr[6];
	int ff = (move.from % 8);
	int rf = (move.from / 8);
	int ft = (move.to % 8);
	int rt = (move.to / 8);
	int promoted = move.transforms;
	MvStr[0] = ('a' + ff);
	MvStr[1] = ('1' + rf);
	MvStr[2] = ('a' + ft);
	MvStr[3] = ('1' + rt);
	if (promoted) {
		char pchar = 'q';
		if (promoted == whiteKnight || promoted == blackKnight) {
			pchar = 'n';
		}
		else if (promoted == whiteRook || promoted == blackRook) {
			pchar = 'r';
		}
		else if (promoted == whiteBishop || promoted == blackBishop) {
			pchar = 'b';
		}
		MvStr[4] = pchar;
		//sprintf_s(MvStr, "%c%c%c%c%c", ('a' + ff), ('1' + rf), ('a' + ft), ('1' + rt), pchar);
	}
	else {
		MvStr[4] = ' ';
		//sprintf_s(MvStr, "%c%c%c%c%c", ('a' + ff), ('1' + rf), ('a' + ft), ('1' + rt), ' ');
	}
	MvStr[5] = '\0';
	return MvStr;
}

