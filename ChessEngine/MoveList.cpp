#include "stdafx.h"
#include "MoveList.h"
#include <stdlib.h>
#include "Debug.h"

MoveList::MoveList(unsigned short size)
{
	moves = (Move *)malloc(size * sizeof(Move));
	length = 0;
	this->size = size;
}


MoveList::~MoveList()
{
	/*if (moves) {
		delete[] moves;
	}*/ // TODO
	//free(moves);
	
}
/*int pushCall = 0;
int popCall = 0;*/
void MoveList::pushMove(Move mv) {
	/*if (this->size == 250) {
		pushCall++;
	}*/

	if (size > length) {
		if (moves) {
			moves[length] = mv;
			length++;
		}
		
	}
	else {
		//printMoveList(this);
		//TODO throw exception
	}
}

Move MoveList::popMove() {
	/*if (this->size == 250) {
		popCall++;
	}*/

	/*if (pushCall - 10 > popCall) {
		if (pushCall - 5 > popCall) {
			std::cout << "Warning you are pushing more offten" << std::endl;
		}
	}*/
	if (0 < length) {
		length--;
		if (moves == NULL) {
			std::cout << "Warning moves already freed from memory" << std::endl;
			return Move();
		}
		return moves[length];
	}
	else {
		//printMoveList(this);
		//TODO throw exception
	}
}
bool MoveList::empty() {
	return length == 0;
}

void MoveList::copy(MoveList ml) {
	delete[] this->moves;
	this->moves = (Move *)malloc(ml.size * sizeof(Move));
	this->length = ml.length;
	for (int i = 0; i < length; i++) {
		moves[i] = ml.moves[i];
	}
	this->size = size;
}