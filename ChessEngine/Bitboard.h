#ifndef BITBOARD_H
#define BITBOARD_H

#include "stdafx.h"
#include "Definitions.cpp"
#include "MoveList.h"
#include "BitboardMoveGen.h"

class Bitboard {
public:
	unsigned long long pieces = 0b1111111111111111000000000000000000000000000000001111111111111111;
	unsigned long long whitePieces = 0b0000000000000000000000000000000000000000000000001111111111111111;
	unsigned long long blackPieces = 0b1111111111111111000000000000000000000000000000000000000000000000;

	bool whiteOnMove = false;
	char whiteCastleAlowed = BothSides;
	char blackCastleAlowed = BothSides;
	unsigned long long enPassant = 0;
	unsigned int ply = 0;
	unsigned long long tHelpAllTime = 0;
	unsigned int tCollisionAllTime = 0;

	char numberOfMovesWithoutTake = 0;
	MoveList history = MoveList(MAX_HISTORY_MOVES);
	Move bestMove;
	unsigned long long blackPiecesList[6] = {
											0b0000000011111111000000000000000000000000000000000000000000000000,
											0b0100001000000000000000000000000000000000000000000000000000000000,
											0b0010010000000000000000000000000000000000000000000000000000000000,
											0b1000000100000000000000000000000000000000000000000000000000000000,
											0b0000100000000000000000000000000000000000000000000000000000000000,
											0b0001000000000000000000000000000000000000000000000000000000000000
	};
	unsigned long long whitePiecesList[6] = {
		0b0000000000000000000000000000000000000000000000001111111100000000,
		0b0000000000000000000000000000000000000000000000000000000001000010,
		0b0000000000000000000000000000000000000000000000000000000000100100,
		0b0000000000000000000000000000000000000000000000000000000010000001,
		0b0000000000000000000000000000000000000000000000000000000000001000,
		0b0000000000000000000000000000000000000000000000000000000000010000
	};

	char numOfMoves = 0;
	char numOfMoves2 = 0;
	//U64 SetSquare[64];
	//U64 ClearSquare[64];

	//unsigned long long undoCount = 0; //TODO DEBUG REMOVE
	//unsigned long long playCount = 0; //TODO DEBUG REMOVE
	//Move lastUndid = Move(); //TODO DEBUG REMOVE
public:
	void Board();
	void GenerateMoves(MoveList*);
	void GenerateCapturingMoves(MoveList *);
	int PlayMove(Move*);
	void UndoMove();
	bool isCheck(bool);
	short countMoves(bool);
	void setPieces(char[CHESSBOARD_LENGTH]);
	void setWhitePieceList(char[6][MAX_NUMBER_OF_PIECES]);
	void setBlackPieceList(char[6][MAX_NUMBER_OF_PIECES]);
	void switchWhiteOnMove();
	char* getPieces();
	bool isRepeating(bool);
	bool FiftyWithoutTake();
	bool isDraw();
	void SetFenPosition(const char *);
	//unsigned long long getPossitionKey(); TODO think about more efficient key representation
	// you cannot just key the generated key, you can either store the key value, but then you have to update it with every move
	// or you can generate position key each time you insert a record into move table
private:
	void doEnPassantMove(Move*);
	void doTransformationMove(Move*);
	void doCastelingMove(Move*);
	void doMove(Move*);
	void undoEnPassantMove(Move*);
	void undoTransformationMove(Move*);
	void undoCastelingMove(Move*);
	void undoMove(Move*);
	void updateCastelingOptions(Move*);
};
#endif