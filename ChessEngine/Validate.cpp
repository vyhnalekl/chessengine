#include "stdafx.h"
#include "Definitions.cpp"
#include <iostream>


bool isWhite(char piece) {
	return (0 < piece && piece < BLACK_OFFSET);
}

bool isBlack(char piece) {
	return (BLACK_OFFSET <= piece && piece < PIECES_ENUM_LENGTH);
}

bool sameColor(char p1, char p2) {
	return (isWhite(p1) && isWhite(p2)) || (isBlack(p1) && isBlack(p2));
}