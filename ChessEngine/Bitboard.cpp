#include "stdafx.h"
#include "Bitboard.h"
#include "HashGenerator.h"
//#include "BitboardMoveGen.h"

void Bitboard::Board() {
	/*for (int i = 0; i < 64; i++) {
		this->SetSquare[i] = 1ULL << i;
	}
	for (int i = 0; i < 64; i++) {
		this->ClearSquare[i] = ~this->SetSquare[i];
	}*/
}

namespace {
	char COUNT(U64 b) {
		int r;
		for (r = 0; b; r++, b &= b - 1);
		return r;
	}
	char* print64(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}

	char* print642(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}
	void PrintMoveList64(MoveList * ml) {
		for (int i = 0; i < ml->length; i++) {
			Move mv = ml->moves[i];
			std::cout << print64(mv.from) << "-->" << print642(mv.to) << std::endl;
		}
	}
}
bool Bitboard::isDraw() {
	return !(COUNT(this->whitePiecesList[0]) > 0 || COUNT(this->blackPiecesList[0]) > 0
		|| COUNT(this->whitePiecesList[1]) + COUNT(this->whitePiecesList[2]) > 1
		|| COUNT(this->blackPiecesList[1]) + COUNT(this->blackPiecesList[2]) > 1
		|| COUNT(this->whitePiecesList[3]) > 0 || COUNT(this->whitePiecesList[4]) > 0
		|| COUNT(this->blackPiecesList[3]) > 0 || COUNT(this->blackPiecesList[4]) > 0);
}
bool Bitboard::isCheck(bool white) {
	if (white) {
		U64 temp = whitePiecesList[5];
		return Threathens(whitePiecesList[5], blackPiecesList, whitePieces, blackPieces, true, POP(&temp));
	}
	else {
		U64 temp = blackPiecesList[5];
		return Threathens(blackPiecesList[5], whitePiecesList, blackPieces, whitePieces, false, POP(&temp));
	}
}
bool Bitboard::isRepeating(bool nullMove) {
	//char numberOfRepetitions = 0;
	//unsigned long long hash = history.moves[history.length - 1].keyBeforeMove;a
	//PrintMoveList64(&history);
	char offset = (nullMove)? 6: 5;
	U64 key = history.moves[history.length - 1].keyAfterMove;
	// Because at history.length - 1 is the last move played, we want to check for moves before
	for (int i = history.length - offset; i >= 0; i = i - 4) {
		Move mv = history.moves[i];
		if (mv.taken == EMPTY && 
			mv.movedPiece != whitePawn &&
			mv.movedPiece != blackPawn &&
			!mv.isCastelingMove) {
			if (mv.keyAfterMove == key) {
				/*numberOfRepetitions++;
				if (numberOfRepetitions > 1) {*/
					return true;
				//}
			}
		}
		else {
			return false;
		}
	}
	return false;
}

void Bitboard::updateCastelingOptions(Move *mv) {
	// Move from = whiteKing = NO
	// Move from = a1 => KingSide
	if (whiteCastleAlowed != NO && whiteCastleAlowed != Made) {
		if ((*mv).from == e1) {
			this->whiteCastleAlowed = NO;
		}
		else{
			if ((*mv).from == a1) {
				//this->whiteCastleAlowed += 1; Can't be done, consider moving a rook twice (3 times)
				if (this->whiteCastleAlowed == BothSides) {
					this->whiteCastleAlowed = KingSide;
				}
				else if (this->whiteCastleAlowed == QueenSide) {
					this->whiteCastleAlowed = NO;
				}
			}
			else if ((*mv).from == h1) {
				//this->whiteCastleAlowed += 2;Can't be done, consider moving a rook twice (3 times)
				if (this->whiteCastleAlowed == BothSides) {
					this->whiteCastleAlowed = QueenSide;
				}
				else if (this->whiteCastleAlowed == KingSide) {
					this->whiteCastleAlowed = NO;
				}
			}
		}
	}

	// castleOptions (Black)
	if (blackCastleAlowed != NO && blackCastleAlowed != Made) {
		if ((*mv).to == e8) {
			this->blackCastleAlowed = NO;
		}
		else {
			if ((*mv).from == a8) {
				//this->blackCastleAlowed += 1;Can't be done, consider moving a rook twice (3 times)
				if (this->blackCastleAlowed == BothSides) {
					this->blackCastleAlowed = KingSide;
				}
				else if (this->blackCastleAlowed == QueenSide) {
					this->blackCastleAlowed = NO;
				}
			}
			else if ((*mv).from == h8) {
				//this->blackCastleAlowed += 2;Can't be done, consider moving a rook twice (3 times)
				if (this->blackCastleAlowed == BothSides) {
					this->blackCastleAlowed = QueenSide;
				}
				else if (this->blackCastleAlowed == KingSide) {
					this->blackCastleAlowed = NO;
				}
			}
		}
	}
	/*if (!checkBoard(pieces) ||
		!checkPiecesList(pieces, whitePiecesList, blackPiecesList)) {
		std::cout << "Blah " << std::endl;
	}*/
}

int Bitboard::PlayMove(Move* mv2) {
	Move *mv = &Move(); // TODO try to change and use mv2 instead of creating new move
	mv->from = mv2->from; // from where
	mv->to = mv2->to;
	mv->isEnPassantMove = mv2->isEnPassantMove;
	mv->isCastelingMove = mv2->isCastelingMove;
	mv->enPassant = mv2->enPassant;
	mv->transforms = mv2->transforms;
	mv->taken = mv2->taken;
	mv->score = mv2->score;
	mv->movedPiece = mv2->movedPiece;
	mv->keyBeforeMove = mv2->keyBeforeMove;
	mv->keyAfterMove = mv2->keyBeforeMove;
	//this->playCount++;
	(*mv).numberOfMovesWithoutTake = numberOfMovesWithoutTake;
	// Store the current value of castle ( needed for undo move)
	(*mv).currentCastleWhite = this->whiteCastleAlowed;
	(*mv).currentCastleBlack = this->blackCastleAlowed;
	(*mv).enPassantBeforeMove = this->enPassant;
	updateCastelingOptions(mv);
	
	if (mv->movedPiece <= whiteKing) {
		if (mv->currentCastleWhite != this->whiteCastleAlowed) {
			mv->keyAfterMove = hashCastelingChange(mv->keyAfterMove, mv->currentCastleWhite, this->whiteCastleAlowed);
		}
		//white moves
		if (((1ULL << (*mv).to) & enPassant) && (*mv).movedPiece == whitePawn ) {
			whitePiecesList[0] &= ~(1ULL << mv->from); // remove old piece
			whitePieces &= ~(1ULL << mv->from); // remove old piece

			blackPiecesList[0] &= ~((1ULL << mv->to) >> UP); // remove taken piece
			blackPieces &= ~((1ULL << mv->to) >> UP);

			mv->isEnPassantMove = true;
			whitePiecesList[0] |= (1ULL << mv->to); // add new piece
			whitePieces |= (1ULL << mv->to); // add new piece

			mv->keyAfterMove = hashWhiteEnPassantMove(mv->keyAfterMove, mv);
		}
		else if ((*mv).transforms != EMPTY) {
			//doTransformationMove(mv);
			whitePiecesList[0] &= ~(1ULL << mv->from); // remove old piece
			whitePieces &= ~(1ULL << mv->from); // remove old piece
			//blackPiecesList[0] &= ~((1ULL << mv->to)); // remove taken piece if there is any
			
			if (mv->taken) {
				blackPieces &= ~(1ULL << mv->to);
				blackPiecesList[mv->taken - 7] &= ~(1ULL << mv->to);
			}

			whitePiecesList[mv->transforms - whitePawn] |= (1ULL << mv->to); // add new piece
			whitePieces |= (1ULL << mv->to); // add new piece

			mv->keyAfterMove = hashTransformationMove(mv->keyAfterMove, mv);
		}
		else if ((*mv).isCastelingMove) {
			//doCastelingMove(mv);
			if (mv->to == c1) {
				//queenside
				whitePiecesList[5] &= ~(1ULL << mv->from); // remove old piece
				whitePiecesList[5] |= (1ULL << mv->to); // add new piece
				whitePiecesList[3] &= ~(1ULL << a1); // remove old piece
				whitePiecesList[3] |= (1ULL << d1); // add new piece
				whitePieces &= ~((1ULL << mv->from) | (1ULL << a1)); // remove old piece
				whitePieces |= ((1ULL << mv->to) | (1ULL << d1)); // add new piece
				mv->keyAfterMove = hashCastelingQueenWhiteMove(mv->keyAfterMove, mv);
			}
			else {
				//kingside
				whitePiecesList[5] &= ~(1ULL << mv->from); // remove old piece
				whitePiecesList[5] |= (1ULL << mv->to); // add new piece
				whitePiecesList[3] &= ~(1ULL << h1); // remove old piece
				whitePiecesList[3] |= (1ULL << f1); // add new piece
				whitePieces &= ~(((1ULL << mv->from) | (1ULL << h1))); // remove old piece
				whitePieces |= ((1ULL << mv->to) | (1ULL << f1)); // add new piece
				mv->keyAfterMove = hashCastelingKingWhiteMove(mv->keyAfterMove, mv);
			}
			whiteCastleAlowed = Made;
			
		}
		else {
			whitePiecesList[mv->movedPiece - 1] &= ~(1ULL << mv->from); // remove old piece
			whitePieces &= ~(1ULL << mv->from); // remove old piece
			whitePiecesList[mv->movedPiece - 1] |= (1ULL << mv->to); // add new piece
			whitePieces |= (1ULL << mv->to); // add new piece
			
			if (mv->taken) {
				blackPieces &= ~(1ULL << mv->to);
				blackPiecesList[mv->taken - 7] &= ~(1ULL << mv->to);
			}
			mv->keyAfterMove = hashMove(mv->keyAfterMove, mv);
		}

		if ((!(mv->taken)) && (!((1ULL << (*mv).to) & whitePiecesList[0]))) {
			numberOfMovesWithoutTake++;
		}
		else {
			numberOfMovesWithoutTake = 0;
		}

		history.pushMove((*mv));// add move to history
		this->enPassant = mv->enPassant;
		U64 temp = whitePiecesList[5];
		return !Threathens(whitePiecesList[5], blackPiecesList, whitePieces, blackPieces, true, POP(&temp));
	}
	else {
		if (mv->currentCastleBlack != this->blackCastleAlowed) {
			mv->keyAfterMove = hashCastelingChange(mv->keyAfterMove, mv->currentCastleBlack, this->blackCastleAlowed);
		}
		//black moves
		if (((1ULL << (*mv).to) & enPassant) && (*mv).movedPiece == blackPawn) {
			blackPiecesList[0] &= ~(1ULL << mv->from); // remove old piece
			blackPieces &= ~(1ULL << mv->from); // remove old piece

			whitePiecesList[0] &= ~((1ULL << mv->to) << UP); // remove taken piece
			whitePieces &= ~((1ULL << mv->to) << UP);

			mv->isEnPassantMove = true;
			blackPiecesList[0] |= (1ULL << mv->to); // add new piece
			blackPieces |= (1ULL << mv->to); // add new piece
			mv->keyAfterMove = hashBlackEnPassantMove(mv->keyAfterMove, mv);
		}
		else if ((*mv).transforms != EMPTY) {
			//doTransformationMove(mv);
			blackPiecesList[0] &= ~(1ULL << mv->from); // remove old piece
			blackPieces &= ~(1ULL << mv->from); // remove old piece

			if (mv->taken) {
				whitePieces &= ~(1ULL << mv->to);
				whitePiecesList[mv->taken - 1] &= ~(1ULL << mv->to);
			}
			
			//whitePiecesList[0] &= ~((1ULL << mv->to)); // remove taken piece if there is any
			blackPiecesList[mv->transforms - blackPawn] |= (1ULL << mv->to); // add new piece
			blackPieces |= (1ULL << mv->to); // add new piece
			mv->keyAfterMove = hashTransformationMove(mv->keyAfterMove, mv);
		}
		else if ((*mv).isCastelingMove) {
			//doCastelingMove(mv);
			if (mv->to == c8) {
				//queenside
				blackPiecesList[5] &= ~(1ULL << mv->from); // remove old piece
				blackPiecesList[5] |= (1ULL << mv->to); // add new piece
				blackPiecesList[3] &= ~(1ULL << a8); // remove old piece
				blackPiecesList[3] |= (1ULL << d8); // add new piece
				blackPieces &= ~((1ULL << mv->from) | (1ULL << a8)); // remove old piece
				blackPieces |= (1ULL << mv->to) | (1ULL << d8); // add new piece

				mv->keyAfterMove = hashCastelingQueenBlackMove(mv->keyAfterMove, mv);
			}
			else {
				//kingside
				blackPiecesList[5] &= ~(1ULL << mv->from); // remove old piece
				blackPiecesList[5] |= (1ULL << mv->to); // add new piece
				blackPiecesList[3] &= ~(1ULL << h8); // remove old piece
				blackPiecesList[3] |= (1ULL << f8); // add new piece
				blackPieces &= ~((1ULL << mv->from) | (1ULL << h8)); // remove old piece
				blackPieces |= (1ULL << mv->to) |(1ULL << f8); // add new piece

				mv->keyAfterMove = hashCastelingKingBlackMove(mv->keyAfterMove, mv);
			}
			blackCastleAlowed = Made;
		}
		else {
			blackPiecesList[mv->movedPiece - 7] &= ~(1ULL << mv->from); // remove old piece
			blackPieces &= ~(1ULL << mv->from); // remove old piece
			blackPiecesList[mv->movedPiece - 7] |= (1ULL << mv->to); // add new piece
			blackPieces |= (1ULL << mv->to); // add new piece
			if (mv->taken) {
				whitePieces &= ~(1ULL << mv->to);
				whitePiecesList[mv->taken - 1] &= ~(1ULL << mv->to);
			}

			mv->keyAfterMove = hashMove(mv->keyAfterMove, mv);
		}
		if ((!mv->taken) && (!((1ULL << (*mv).to) & blackPiecesList[0]))) {
			numberOfMovesWithoutTake++;
		}
		else {
			numberOfMovesWithoutTake = 0;
		}

		history.pushMove((*mv));// add move to history
		this->enPassant = mv->enPassant;
		U64 temp = blackPiecesList[5];
		return !Threathens(blackPiecesList[5], whitePiecesList, blackPieces, whitePieces, false, POP(&temp));
	}
	
}

void Bitboard::UndoMove() {
	Move last;
	/*this->undoCount++;
	if (history.empty()) {
		// nothing to undo
		std::cout << "Error empty history " << std::endl;
		return;
	}*/
	//TODO DEBUG REMOVE
	last = history.popMove();
	/*lastUndid.from = last.from;
	lastUndid.to = last.to;
	lastUndid.movedPiece = last.movedPiece;
	lastUndid.taken = last.taken;*/

	this->enPassant = last.enPassantBeforeMove; // because we need the state before the move was played
	this->whiteCastleAlowed = last.currentCastleWhite;
	this->blackCastleAlowed = last.currentCastleBlack;
	this->numberOfMovesWithoutTake = last.numberOfMovesWithoutTake;
	if (last.movedPiece <= whiteKing) {
		//white moved
		if (last.isEnPassantMove) {
			this->whitePiecesList[0] |= (1ULL << last.from);
			this->whitePiecesList[0] &= ~(1ULL << last.to);
			this->whitePieces |= (1ULL << last.from);
			this->whitePieces &= ~(1ULL << last.to);

			this->blackPiecesList[0] |= ((1ULL << last.to) >> UP); //  >> because from white view we are goig down
			this->blackPieces |= ((1ULL << last.to) >> UP);
		}
		else if (last.transforms != EMPTY) {
			this->whitePiecesList[0] |= (1ULL << last.from);
			this->whitePiecesList[last.transforms - 1] &= ~(1ULL << last.to);
			this->whitePieces |= (1ULL << last.from);
			this->whitePieces &= ~(1ULL << last.to);

			if (last.taken) {
				this->blackPiecesList[last.taken - 7] |= (1ULL << last.to);
				this->blackPieces |= (1ULL << last.to);
			}
		}
		else if (last.isCastelingMove) {
			this->whitePiecesList[5] |= (1ULL << last.from);
			this->whitePiecesList[5] &= ~(1ULL << last.to);
			this->whitePieces |= (1ULL << last.from);
			this->whitePieces &= ~(1ULL << last.to);

			if (last.to == g1) {
				//king side
				this->whitePiecesList[3] |= (1ULL << h1);
				this->whitePiecesList[3] &= ~(1ULL << f1);
				this->whitePieces |= (1ULL << h1);
				this->whitePieces &= ~(1ULL << f1);
			}
			else {
				this->whitePiecesList[3] |= (1ULL << a1);
				this->whitePiecesList[3] &= ~(1ULL << d1);
				this->whitePieces |= (1ULL << a1);
				this->whitePieces &= ~(1ULL << d1);
			}
		}
		else {
			this->whitePiecesList[last.movedPiece - 1] |= (1ULL << last.from);
			this->whitePiecesList[last.movedPiece - 1] &= ~(1ULL << last.to);
			this->whitePieces |= (1ULL << last.from);
			this->whitePieces &= ~(1ULL << last.to);

			if (last.taken) {
				this->blackPiecesList[last.taken - 7] |= (1ULL << last.to);
				this->blackPieces |= (1ULL << last.to);
			}
		}
	}else {
		//black moved
		if (last.isEnPassantMove) {
			this->blackPiecesList[0] |= (1ULL << last.from);
			this->blackPiecesList[0] &= ~(1ULL << last.to);
			this->blackPieces |= (1ULL << last.from);
			this->blackPieces &= ~(1ULL << last.to);

			this->whitePiecesList[0] |= ((1ULL << last.to) << UP); //  << because from black view we are goig down
			this->whitePieces |= ((1ULL << last.to) << UP);
		}
		else if (last.transforms != EMPTY) {
			this->blackPiecesList[0] |= (1ULL << last.from);
			this->blackPiecesList[last.transforms - 7] &= ~(1ULL << last.to);
			this->blackPieces |= (1ULL << last.from);
			this->blackPieces &= ~(1ULL << last.to);

			if (last.taken) {
				this->whitePiecesList[last.taken - 1] |= (1ULL << last.to);
				this->whitePieces |= (1ULL << last.to);
			}
		}
		else if (last.isCastelingMove) {
			this->blackPiecesList[5] |= (1ULL << last.from);
			this->blackPiecesList[5] &= ~(1ULL << last.to);
			this->blackPieces |= (1ULL << last.from);
			this->blackPieces &= ~(1ULL << last.to);

			if (last.to == g8) {
				//king side
				this->blackPiecesList[3] |= (1ULL << h8);
				this->blackPiecesList[3] &= ~(1ULL << f8);
				this->blackPieces |= (1ULL << h8);
				this->blackPieces &= ~(1ULL << f8);
			}
			else {
				this->blackPiecesList[3] |= (1ULL << a8);
				this->blackPiecesList[3] &= ~(1ULL << d8);
				this->blackPieces |= (1ULL << a8);
				this->blackPieces &= ~(1ULL << d8);
			}
		}
		else {
			this->blackPiecesList[last.movedPiece - 7] |= (1ULL << last.from);
			this->blackPiecesList[last.movedPiece - 7] &= ~(1ULL << last.to);
			this->blackPieces |= (1ULL << last.from);
			this->blackPieces &= ~(1ULL << last.to);

			if (last.taken) {
				this->whitePiecesList[last.taken - 1] |= (1ULL << last.to);
				this->whitePieces |= (1ULL << last.to);
			}
		}
	}
}

void Bitboard::GenerateMoves(MoveList* result) {
	if (whiteOnMove) {
		//printBoard(pieces);
		PawnMoves(result, whitePiecesList[0], whitePieces, blackPieces, true, enPassant, blackPiecesList);
		KnightMoves(result, whitePiecesList[1],  whitePieces, blackPieces, whiteKnight, blackPiecesList, blackPawn);
		BishopMoves(result, whitePiecesList[2], whitePieces, blackPieces, whiteBishop, blackPiecesList, blackPawn);
		RookMoves(result, whitePiecesList[3], whitePieces, blackPieces, whiteRook, blackPiecesList, blackPawn);
		QueenMoves(result, whitePiecesList[4], whitePieces, blackPieces, whiteQueen, blackPiecesList, blackPawn);
		KingMoves(result, whitePiecesList[5], whitePieces, blackPieces, whiteKing, blackPiecesList, blackPawn);
		if (whiteCastleAlowed != NO && whiteCastleAlowed != Made) {
			CastelingMoves(result, whitePiecesList[5], whitePiecesList[3], whitePieces, blackPieces, blackPiecesList, true, whiteCastleAlowed);
		}
	}
	else {
		PawnMoves(result, blackPiecesList[0], blackPieces, whitePieces, false, enPassant, whitePiecesList);
		KnightMoves(result, blackPiecesList[1], blackPieces, whitePieces, blackKnight, whitePiecesList, whitePawn);
		BishopMoves(result, blackPiecesList[2], blackPieces, whitePieces, blackBishop, whitePiecesList, whitePawn);
		RookMoves(result, blackPiecesList[3], blackPieces, whitePieces, blackRook, whitePiecesList, whitePawn);
		QueenMoves(result, blackPiecesList[4], blackPieces, whitePieces, blackQueen, whitePiecesList, whitePawn);
		KingMoves(result, blackPiecesList[5], blackPieces, whitePieces, blackKing, whitePiecesList, whitePawn);
		if (blackCastleAlowed != NO && blackCastleAlowed != Made) {
			CastelingMoves(result, blackPiecesList[5], blackPiecesList[3], blackPieces, whitePieces, whitePiecesList, false, blackCastleAlowed);
		}
	}
}

void Bitboard::GenerateCapturingMoves(MoveList * result) {
	if (whiteOnMove) {
		PawnCaptureMoves(result, whitePiecesList[0], whitePieces, blackPieces, true, enPassant, blackPiecesList);
		KnightCaptureMoves(result, whitePiecesList[1], whitePieces, blackPieces, whiteKnight, blackPiecesList, blackPawn);
		BishopCaptureMoves(result, whitePiecesList[2], whitePieces, blackPieces, whiteBishop, blackPiecesList, blackPawn);
		RookCaptureMoves(result, whitePiecesList[3], whitePieces, blackPieces, whiteRook, blackPiecesList, blackPawn);
		QueenCaptureMoves(result, whitePiecesList[4], whitePieces, blackPieces, whiteQueen, blackPiecesList, blackPawn);
		KingCaptureMoves(result, whitePiecesList[5], whitePieces, blackPieces, whiteKing, blackPiecesList, blackPawn);
	}
	else {
		PawnCaptureMoves(result, blackPiecesList[0], blackPieces, whitePieces, false, enPassant, whitePiecesList);
		KnightCaptureMoves(result, blackPiecesList[1], blackPieces, whitePieces, blackKnight, whitePiecesList, whitePawn);
		BishopCaptureMoves(result, blackPiecesList[2], blackPieces, whitePieces, blackBishop, whitePiecesList, whitePawn);
		RookCaptureMoves(result, blackPiecesList[3], blackPieces, whitePieces, blackRook, whitePiecesList, whitePawn);
		QueenCaptureMoves(result, blackPiecesList[4], blackPieces, whitePieces, blackQueen, whitePiecesList, whitePawn);
		KingCaptureMoves(result, blackPiecesList[5], blackPieces, whitePieces, blackKing, whitePiecesList, whitePawn);
	}
	// For evaluation, only needs to be here, because you evaluate only after quiescene search
	//this->numOfMoves2 = this->numOfMoves;
	//this->numOfMoves = result->length;
}

short Bitboard::countMoves(bool white) {
	short result = 0;
	if (white) {
		//printBoard(pieces);
		CountPawnMoves(&result, whitePiecesList[0], whitePieces, blackPieces, true, enPassant, blackPiecesList);
		CountKnightMoves(&result, whitePiecesList[1], whitePieces, blackPieces, whiteKnight, blackPiecesList, blackPawn);
		CountBishopMoves(&result, whitePiecesList[2], whitePieces, blackPieces, whiteBishop, blackPiecesList, blackPawn);
		CountRookMoves(&result, whitePiecesList[3], whitePieces, blackPieces, whiteRook, blackPiecesList, blackPawn);
		CountQueenMoves(&result, whitePiecesList[4], whitePieces, blackPieces, whiteQueen, blackPiecesList, blackPawn);
	}
	else {
		CountPawnMoves(&result, blackPiecesList[0], blackPieces, whitePieces, false, enPassant, whitePiecesList);
		CountKnightMoves(&result, blackPiecesList[1], blackPieces, whitePieces, blackKnight, whitePiecesList, whitePawn);
		CountBishopMoves(&result, blackPiecesList[2], blackPieces, whitePieces, blackBishop, whitePiecesList, whitePawn);
		CountRookMoves(&result, blackPiecesList[3], blackPieces, whitePieces, blackRook, whitePiecesList, whitePawn);
		CountQueenMoves(&result, blackPiecesList[4], blackPieces, whitePieces, blackQueen, whitePiecesList, whitePawn);
	}
	return result;
}