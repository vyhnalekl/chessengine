
#include "stdafx.h"
//#include "Definitions.cpp"
#include "Board.h"
#include "Bitboard.h"
#include "Debug.h"
#include <iostream>
#include <bitset>

/*#define PAWN_RATING 100
#define BISHOP_RATING 300
#define KNIGHT_RATING 300
#define ROOK_RATING 500
#define QUEEN_RATING 900
*/
const int pieceValueTemp[] = { PAWN_RATING, KNIGHT_RATING + 50, BISHOP_RATING + 50, ROOK_RATING, QUEEN_RATING };
const int PawnTable[64] = {
0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,
5, 10, 10, -20, -20, 10, 10, 5, //
5, -5, -10, 0, 0, -10, -5, 5, //
0, 0, 0, 20, 20, 0, 0, 0, //
5, 5, 10, 25, 25, 10, 5, 5, //
10, 10, 20, 30, 30, 20, 10, 10, //
50, 50, 50, 50, 50, 50, 50, 50, //
0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
};

const int KnightTable[64] = { //
			-50, -40, -30, -30, -30, -30, -40, -50, //
			-40, -20, 0, 0, 0, 0, -20, -40, //
			-30, 0, 10, 15, 15, 10, 0, -30, //
			-30, 5, 15, 20, 20, 15, 5, -30, //
			-30, 0, 15, 20, 20, 15, 0, -30, //
			-30, 5, 10, 15, 15, 10, 5, -30, //
			-40, -20, 0, 5, 5, 0, -20, -40, //
			-50, -40, -30, -30, -30, -30, -40, -50 //
};

const int BishopTable[64] = { //
			-20, -10, -10, -10, -10, -10, -10, -20, //
			-10, 0, 0, 0, 0, 0, 0, -10, //
			-10, 0, 5, 10, 10, 5, 0, -10, //
			-10, 5, 5, 10, 10, 5, 5, -10, //
			-10, 0, 10, 10, 10, 10, 0, -10, //
			-10, 10, 10, 10, 10, 10, 10, -10, //
			-10, 5, 0, 0, 0, 0, 5, -10, //
			-20, -10, -10, -10, -10, -10, -10, -20 //
};

const int RookTable[64] = { //
	0, 0, 0, 5, 5, 0, 0, 0,//


			-5, 0, 0, 0, 0, 0, 0, -5, //
			-5, 0, 0, 0, 0, 0, 0, -5, //
			-5, 0, 0, 0, 0, 0, 0, -5, //
			-5, 0, 0, 0, 0, 0, 0, -5, //
			-5, 0, 0, 0, 0, 0, 0, -5, //
			5, 10, 10, 10, 10, 10, 10, 5, //
	0, 0, 0, 0, 0, 0, 0, 0 //

};
const int QueenTable[64] = { //
			-20, -10, -10, -5, -5, -10, -10, -20, //
			-10, 0, 0, 0, 0, 0, 0, -10, //
			-10, 0, 5, 5, 5, 5, 0, -10, //
			-5, 0, 5, 5, 5, 5, 0, -5, //
			-5, 0, 5, 5, 5, 5, 0, -5, //
			-10, 5, 5, 5, 5, 5, 0, -10, //
			-10, 0, 5, 0, 0, 0, 0, -10, //
			-20, -10, -10, -5, -5, -10, -10, -20 //
};

const int KingE[64] = { //
			-50, -40, -30, -20, -20, -30, -40, -50, //
			-30, -20, -10, 0, 0, -10, -20, -30, //
			-30, -10, 20, 30, 30, 20, -10, -30, //
			-30, -10, 30, 40, 40, 30, -10, -30, //
			-30, -10, 30, 40, 40, 30, -10, -30, //
			-30, -10, 20, 30, 30, 20, -10, -30, //
			-30, -30, 0, 0, 0, 0, -30, -30, //
			-50, -30, -30, -30, -30, -30, -30, -50 //
};

const int KingO[64] = {
	20, 30, 10, 0, 0, 10, 30, 20, //
	20, 20, 0, 0, 0, 0, 20, 20, //
	-50	,	-50	,	-50	,	-50	,	-50	,	-50	,	-50	,	-50	,
	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,
	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,
	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,
	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,
	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70	,	-70
};



const int pawnBonus[64] = {
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,
	10	,	10	,	0	,	-10	,	-10	,	5	,	10	,	10	,
	5	,	0	,	8	,	5	,	5	,	0	,	0	,	5	,
	0	,	0	,	10	,	20	,	20	,	5	,	0	,	0	, // TODO dependent on kings possition
	5	,	5	,	5	,	10	,	10	,	5	,	5	,	5	,
	10	,	10	,	10	,	10	,	10	,	10	,	10	,	10	,
	20	,	20	,	20	,	20	,	20	,	20	,	20	,	20	,
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
};

const int pawnBonusEnd[64] = {
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,
	5	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,
	10	,	10	,	10	,	10	,	10	,	10	,	10	,	10	,
	20	,	20	,	20	,	20	,	20	,	20	,	20	,	20	,
	40	,	40	,	40	,	40	,	40	,	40	,	40	,	40	,
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
};

const int knightBonus[64] = {
	0	,	-10	,	0	,	0	,	0	,	0	,	-10	,	0	,
	0	,	0	,	0	,	5	,	5	,	0	,	0	,	0	,
	0	,	0	,	15	,	10	,	10	,	15	,	0	,	0	,
	0	,	5	,	10	,	20	,	20	,	10	,	5	,	0	,
	0	,	5	,	15	,	20	,	20	,	15	,	5	,	0	,
	0	,	0	,	10	,	10	,	10	,	10	,	0	,	0	,
	0	,	0	,	0	,	10	,	10	,	0	,	0	,	0	,
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
};

const int bishopBonus[64] = {
	5	,	0	,	-10	,	0	,	0	,	-10	,	0	,	5	,
	0	,	15	,	0	,	5	,	5	,	0	,	15	,	0	,
	0	,	0	,	10	,	5	,	5	,	10	,	0	,	0	,
	0	,	5	,	15	,	15	,	15	,	15	,	5	,	0	,
	0	,	10	,	10	,	20	,	20	,	10	,	10	,	0	,
	0	,	0	,	5	,	10	,	10	,	5	,	0	,	0	,
	0	,	0	,	0	,	5	,	5	,	0	,	0	,	0	,
	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
};

const int rookBonus[64] = {
	-5	,	0	,	5	,	10	,	10	,	5	,	0	,	-5	,
	0	,	0	,	5	,	10	,	10	,	5	,	0	,	0	,
	0	,	0	,	5	,	10	,	10	,	5	,	0	,	0	,
	0	,	0	,	5	,	10	,	10	,	5	,	0	,	0	,
	0	,	0	,	5	,	10	,	10	,	5	,	0	,	0	,
	0	,	0	,	5	,	10	,	10	,	5	,	0	,	0	,
	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,
	0	,	0	,	5	,	10	,	10	,	5	,	0	,	0
};

const int KingEnd[64] = {
	-100,	-90	,	-90	,	-90	,	-90	,	-90	,	-90	,	-100	,
	-90	,	-75	,	-70	,	-70	,	-70	,	-70	,	-75	,	-90	,
	-90	,	-70	,	-50	,	-50	,	-50	,	-50	,	-70	,	-90	,
	-90	,	-70	,	-50	,	-10	,	-10	,	-50	,	-70	,	-90	,
	-90	,	-70	,	-50	,	-10	,	-10	,	-50	,	-70	,	-90	,
	-90	,	-70	,	-50	,	-50	,	-50	,	-50	,	-70	,	-90	,
	-90	,	-75	,	-70	,	-70	,	-70	,	-70	,	-75	,	-90	,
	-100,	-90	,	-90	,	-90	,	-90	,	-90	,	-90	,	-100
};

const int* bonusArrays[] = { PawnTable, KnightTable, BishopTable, RookTable, QueenTable };

char to64index(char initSquare) { // TODO test
	int square = initSquare;
	square -= 21;
	char temp = (int) square / 10;
	return temp * 8 + square % 10;
}

char mirrorIndex(char square) {
	char debug = square;
	if (A8 <= square && square <= H8) {
		square -= 70;
	}else if (A7 <= square && square <= H7) {
		square -= 50;
	}
	else if (A6 <= square && square <= H6) {
		square -= 30;
	}
	else if (A5 <= square && square <= H5) {
		square -= 10;
	}
	else if (A4 <= square && square <= H4) {
		square += 10;
	}
	else if (A3 <= square && square <= H3) {
		square += 30;
	}
	else if (A2 <= square && square <= H2) {
		square += 50;
	}
	else if (A1 <= square && square <= H1) {
		square += 70;
	}
	return square;
}
/*
int basicFigureRate(Board b, bool * phase) {
	int currentRating = 0;
	//add white figures up
	for (int i = 0; i < 5; i++) { // only to 4 because king is not rated
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (b.whitePiecesList[i][j] != NONE) {
				currentRating += pieceValueTemp[i];
			}
		}
	}
	if (currentRating < 2000) {
		*phase = END_GAME; 
	}
	else {
		*phase = MIDDLE_GAME; 
	}
	//subtract black pieces
	for (int i = 0; i < 5; i++) { // only to 4 because king is not rated
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (b.blackPiecesList[i][j] != NONE) {
				currentRating -= pieceValueTemp[i];
			}
		}
	}
	return currentRating;
}
*/
int bonusPossitionRate(Board b) {
	int currentRating = 0;
	//**********************
	//FIGURE POSSITION BONUS
	//**********************
	//add white figures up
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (b.whitePiecesList[i][j] != NONE) {
				currentRating += bonusArrays[i][to64index(b.whitePiecesList[i][j])];
			}
		}
	}
	//subtract black pieces
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (b.blackPiecesList[i][j] != NONE) {
				currentRating -= bonusArrays[i][to64index(mirrorIndex(b.blackPiecesList[i][j]))];
			}
		}
	}
	return currentRating;
}

int bonusEndPossitionRate(Board b) {
	int currentRating = 0;

	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.whitePiecesList[0][j] != NONE) {
			currentRating += pawnBonusEnd[to64index(b.whitePiecesList[0][j])];
		}
	}
	//subtract black pawns

	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.blackPiecesList[0][j] != NONE) {
			currentRating += pawnBonusEnd[to64index(mirrorIndex(b.blackPiecesList[0][j]))] * -1;
		}
	}
	
	return currentRating;
}

int doubleBishopRate(Board b) {
	int currentRate = 0;
	int i = 2;
	int k = 0;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.whitePiecesList[i][j] != NONE) {
			k++;
			if (k >= 2) {
				currentRate =30;
				break;
			}
		}
	}
	k = 0;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.blackPiecesList[i][j] != NONE) {
			k++;
			if (k >= 2) {
				currentRate -= 30;
				break;
			}
		}
	}
	return currentRate;
}

int mobilityRate(Board b) {
	int currentRating = 0;
	// KNIGHT
	int i = 1;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.whitePiecesList[i][j] != NONE) {
			for (int k = 0; k < KNIGHT_OFFSET_LENGTH; k++) {
				char square = b.whitePiecesList[i][j];
				if (b.pieces[square + knightOffsets[k]] == EMPTY
					|| (b.pieces[square + knightOffsets[k]] > BLACK_OFFSET &&
						b.pieces[square + knightOffsets[k]] < PIECES_ENUM_LENGTH)) {
					currentRating += 3;
				}
			}
		}
	}
	// BISHOP
	i = 2;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.whitePiecesList[i][j] != NONE) {
			for (int k = 0; k < BISHOP_OFFSET_LENGTH; k++) {
				char square = b.whitePiecesList[i][j];
				int scale = 1;
				while (b.pieces[square + bishopOffsets[k] * scale] == EMPTY) {
					currentRating += 3;
					scale++;
				}
				if(b.pieces[square + bishopOffsets[k] * scale] > BLACK_OFFSET &&
				b.pieces[square + bishopOffsets[k] * scale] < PIECES_ENUM_LENGTH) {
					currentRating += b.pieces[square + bishopOffsets[k] * scale] - BLACK_OFFSET;
				}
			}
		}
	}
	// Rook
	i = 3;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.whitePiecesList[i][j] != NONE) {
			for (int k = 0; k < ROOK_OFFSET_LENGTH; k++) {
				char square = b.whitePiecesList[i][j];
				int scale = 1;
				while (b.pieces[square + rookOffsets[k] * scale] == EMPTY) {
					currentRating += 3;
					scale++;
				}
				if (b.pieces[square + rookOffsets[k] * scale] > BLACK_OFFSET &&
					b.pieces[square + rookOffsets[k] * scale] < PIECES_ENUM_LENGTH) {
					currentRating += b.pieces[square + rookOffsets[k] * scale] - BLACK_OFFSET;
				}
			}
		}
	}
	// QUEEN
	i = 4;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.whitePiecesList[i][j] != NONE) {
			for (int k = 0; k < QUEEN_OFFSET_LENGTH; k++) {
				char square = b.whitePiecesList[i][j];
				int scale = 1;
				while (b.pieces[square + queenOffsets[k] * scale] == EMPTY) {
					currentRating += 3;
					scale++;
				}
				if (b.pieces[square + queenOffsets[k] * scale] > BLACK_OFFSET &&
					b.pieces[square + queenOffsets[k] * scale] < PIECES_ENUM_LENGTH) {
					currentRating += b.pieces[square + queenOffsets[k] * scale] - BLACK_OFFSET;
				}
			}
		}
	}
	/*********************
			BLACK
	 *********************/
	// KNIGHT
	i = 1;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.blackPiecesList[i][j] != NONE) {
			for (int k = 0; k < KNIGHT_OFFSET_LENGTH; k++) {
				char square = b.blackPiecesList[i][j];
				if ((b.pieces[square + knightOffsets[k]] >= 0 &&
					b.pieces[square + knightOffsets[k]] < BLACK_OFFSET)) {
					currentRating -= 3;
				}
			}
		}
	}
	// BISHOP
	i = 2;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.blackPiecesList[i][j] != NONE) {
			for (int k = 0; k < BISHOP_OFFSET_LENGTH; k++) {
				char square = b.blackPiecesList[i][j];
				int scale = 1;
				while (b.pieces[square + bishopOffsets[k] * scale] == EMPTY) {
					currentRating -= 3;
					scale++;
				}
				if (b.pieces[square + bishopOffsets[k] * scale] > 0 &&
					b.pieces[square + bishopOffsets[k] * scale] < BLACK_OFFSET) {
					currentRating -= b.pieces[square + bishopOffsets[k] * scale];
				}
			}
		}
	}
	// Rook
	i = 3;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.blackPiecesList[i][j] != NONE) {
			for (int k = 0; k < ROOK_OFFSET_LENGTH; k++) {
				char square = b.blackPiecesList[i][j];
				int scale = 1;
				while (b.pieces[square + rookOffsets[k] * scale] == EMPTY) {
					currentRating -= 3;
					scale++;
				}
				if (b.pieces[square + rookOffsets[k] * scale] > 0 &&
					b.pieces[square + rookOffsets[k] * scale] < BLACK_OFFSET) {
					currentRating -= b.pieces[square + rookOffsets[k] * scale];
				}
			}
		}
	}
	// QUEEN
	i = 4;
	for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
		if (b.blackPiecesList[i][j] != NONE) {
			for (int k = 0; k < QUEEN_OFFSET_LENGTH; k++) {
				char square = b.blackPiecesList[i][j];
				int scale = 1;
				while (b.pieces[square + queenOffsets[k]* scale] == EMPTY) {
					currentRating -= 3;
					scale++;
				}
				if (b.pieces[square + queenOffsets[k] * scale] > 0 &&
					b.pieces[square + queenOffsets[k] * scale] < BLACK_OFFSET) {
					currentRating -= b.pieces[square + queenOffsets[k] * scale];
				}
			}
		}
	}
	return currentRating;
}
int kingsSafety(Board b) {
	// WHITE KING
	int rate = 0;
	if (b.whiteCastleAlowed == Made) {
		rate += 50;
	}
	else if (b.whiteCastleAlowed == NO) {
		rate -= 50;
	}

	if (b.blackCastleAlowed == Made) {
		rate -= 50;
	}else if (b.blackCastleAlowed == NO) {
		rate += 50;
	}
	/*
	char wKing = b.whitePiecesList[5][0];
	char offsets[] = { 9, 10, 11 };*/
	//On the line
	/*int i = 1;
	while (b.pieces[wKing + i] == EMPTY) {
		i++;
	}
	if (b.pieces[wKing + i] == NONE || b.pieces[wKing + i] < BLACK_OFFSET) {
		rate += 5;
	}
	else {
		rate -= 5;
	}
	i = 1;
	while (b.pieces[wKing - i] == EMPTY) {
		i++;
	}
	if (b.pieces[wKing - i] == NONE || b.pieces[wKing - i] < BLACK_OFFSET) {
		rate += 5;
	}
	else {
		rate -= 5;
	}*/
	/*
	if (b.pieces[wKing + 9] == whitePawn) {
		rate += 10;
	}
	if (b.pieces[wKing + 10] == whitePawn) {
		rate += 20;
	}
	if (b.pieces[wKing + 11] == whitePawn) {
		rate += 10;
	}
	*/
	// the line directly above king
	/*
	for (int j = 0; j < 3; j++) {
		float multiply = 1.0;
		int i = 2;
		char piece = b.pieces[wKing + offsets[j] * i];
		while (piece != NONE) {
			if (whitePawn <= piece && piece < whiteKing) {
				//white piece defending
				rate += 6 - piece; // smaller the piece the better
			}
			else {
				if (piece == blackPawn) {
					rate += i * 3; // he is in a way, so black cant attack
									// the further he is the better
					multiply = 0.8 - (i / 10); // if rook is on row and pawn is not there
				}
				else {
					if (piece == blackQueen) {
						rate -= 10;
					}
					else if (piece == blackRook) {
						rate -= 20 * multiply;
					}
				}
			}
			i++;
			piece = b.pieces[wKing + offsets[j] * i];
		}
	}
	*/


	
	
	/*

	int i = 0;
	for (int j = 0; j < 3; j++) {
		i = 1;
		while (b.pieces[wKing + offsets[j] * i] != NONE) {
			if (b.pieces[wKing + offsets[j] * i] < BLACK_OFFSET - 2 && b.pieces[wKing + offsets[j] * i] > 0) { // dont want a queen
				rate += 3;
			}
			else if (b.pieces[wKing + offsets[j] * i] > BLACK_OFFSET && b.pieces[wKing + offsets[j] * i] != blackKing) { // dont want a pawn
				rate -= 3; // TODO check whether piece is pointing to king
			}

			i++;
		}
	}*/
	

	// BLACK KING
	/*
	char bKing = b.blackPiecesList[5][0];
	offsets[0] = -9;
	offsets[1] = -10;
	offsets[2] = -11;*/

	//On the line
	/*i = 1;
	while (b.pieces[bKing + i] == EMPTY) {
		i++;
	}
	if (b.pieces[bKing + i] == NONE || b.pieces[bKing + i] >= BLACK_OFFSET) {
		rate -= 5;
	}
	else {
		rate += 5;
	}
	i = 1;
	while (b.pieces[bKing - i] == EMPTY) {
		i++;
	}
	if (b.pieces[bKing - i] == NONE || b.pieces[bKing - i] >= BLACK_OFFSET) {
		rate -= 5;
	}
	else {
		rate += 5;
	}*/
/*
	if (b.pieces[bKing - 9] == blackPawn) {
		rate -= 10;
	}
	if (b.pieces[bKing - 10] == blackPawn) {
		rate -= 20;
	}
	if (b.pieces[bKing - 11] == blackPawn) {
		rate -= 10;
	}*/
	/*
	for (int j = 0; j < 3; j++) {
		float multiply = 1.0;
		int i = 2;
		char piece = b.pieces[bKing + offsets[j] * i];
		while (piece != NONE) {
			if (piece >= blackPawn && piece < blackKing) {
				//white piece defending
				rate -= 6 - (piece - BLACK_OFFSET); // smaller the piece the better
			}
			else {
				if (piece == whitePawn) {
					rate -= i * 3; // he is in a way, so black cant attack
									// the further he is the better
					multiply = 0.8 - (i / 10); // if rook is on row and pawn is not there
				}
				else {
					if (piece == whiteQueen) {
						rate += 10;
					}
					else if (piece == whiteRook) {
						rate += 20 * multiply;
					}
				}
			}
			i++;
			piece = b.pieces[bKing + offsets[j] * i];
		}
	}*/

	/*
	for (int j = 0; j < 3; j++) {
		i = 1; // must be one, when king is in a corner, with 2 you will reach beyond the array
		while (b.pieces[bKing + offsets[j] * i] != NONE) {
			if (b.pieces[bKing + offsets[j] * i] >= BLACK_OFFSET && b.pieces[bKing + offsets[j] * i] != blackQueen) { // dont want a queen
				rate -= 3;
			}
			else if (b.pieces[bKing + offsets[j] * i] < BLACK_OFFSET - 1 && b.pieces[bKing + offsets[j] * i] > whitePawn) { // dont want a pawn
				rate += 3; // TODO check whether piece is pointing to king
			}
			i++;
		}
	}*/

	return rate;
}
int pawnRatingEnd(Board b) {
	int LENGTH = 7;
	int rating = 0;
	char pawns[] = { 0,0,0,0,0,0,0,0 };
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char temp = b.whitePiecesList[0][i];
		if (temp != NONE) {
			int index = (temp % 10) - 1;
			pawns[index] = pawns[index] + 1;
		}
	}

	bool left = false; // is there a pawn on left collumn?
	bool left2 = true;
	for (int i = 0; i < LENGTH; i++) {
		if (pawns[i] > 1) {
			//more than 1 pawn on one row
			rating -= 50;
		}
		if (pawns[i] == 0) {
			if ((!left2) && left) {
				rating -= 25;
			}
		}
		left2 = left;
		left = (pawns[i] >= 1);
	}

	//BLACK
	char bPawns[] = { 0,0,0,0,0,0,0,0 };

	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char temp = b.blackPiecesList[0][i];
		if (temp != NONE) {
			int index = (temp % 10) - 1;
			bPawns[index] = bPawns[index] + 1;
		}
	}
	left = false; // is there a pawn on left collumn?
	left2 = true;
	for (int i = 0; i < LENGTH; i++) {
		if (bPawns[i] > 1) {
			//more than 1 pawn on one row
			rating += 50;
		}
		if (bPawns[i] == 0) {
			if ((!left2) && left) {
				rating += 25;
			}
		}
		left2 = left;
		left = (bPawns[i] >= 1);
	}

	/*ROOK ON OPEN FILE BONUS*/
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char sq = b.whitePiecesList[3][i];
		if (sq != NONE) {
			char index = (sq % 10) - 1;
			if (pawns[index] == 0 && bPawns[index] == 0) {
				rating += 15;
			}
		}
	}
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char sq = b.blackPiecesList[3][i];
		if (sq != NONE) {
			char index = (sq % 10) - 1;
			if (bPawns[index] == 0 && pawns[index] == 0) {
				rating -= 15;
			}
		}
	}
	/*ROOK ON OPEN FILE NNUS END*/
	return rating;
}
int pawnRatingWithKingSafety(Board b) {
	int LENGTH = 7;
	int rating = 0;
	char pawns[] = { 0,0,0,0,0,0,0,0 };
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char temp = b.whitePiecesList[0][i];
		if (temp != NONE) {
			int index = (temp % 10) - 1;
			pawns[index] = pawns[index] + 1;
		}
	}
	/*KINGS Safety*/
	char wKing = b.whitePiecesList[5][0];
	char wKingColumn = wKing % 10;
	if (wKingColumn <= 4) {
		//queen side
		for (int i = 0; i < wKingColumn; i++) {
			if (pawns[i] == 0) {
				rating -= 30;
			}
		}
	}
	else {
		//kings side
		for (int i = 7; i >= wKingColumn - 1; i--) {
			if (pawns[i] == 0) {
				rating -= 30;
			}
		}
	}
	/*Kings safety END*/
	bool left = false; // is there a pawn on left collumn?
	bool left2 = true;
	for (int i = 0; i < LENGTH; i++) {
		if (pawns[i] > 1) {
			//more than 1 pawn on one row
			rating -= 50;
		}
		if (pawns[i] == 0) {
			if ((!left2) && left) {
				rating -= 25;
			}
		}
		left2 = left;
		left = (pawns[i] >= 1);
	}

	//BLACK
	char bPawns[] = { 0,0,0,0,0,0,0,0 };
	
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char temp = b.blackPiecesList[0][i];
		if (temp != NONE) {
			int index = (temp % 10) - 1;
			bPawns[index] = bPawns[index] + 1;
		}
	}
	/*KING safety*/
	char bKing = b.blackPiecesList[5][0];
	char bKingColumn = bKing % 10;
	if (bKingColumn <= 4) {
		//queen side
		for (int i = 0; i < bKingColumn; i++) {
			if (bPawns[i] == 0) {
				rating += 30;
			}
		}
	}
	else {
		//kings side
		for (int i = 7; i >= bKingColumn - 1; i--) {
			if (bPawns[i] == 0) {
				rating += 30;
			}
		}
	}
	/*KING safety END*/
	left = false; // is there a pawn on left collumn?
	left2 = true;
	for (int i = 0; i < LENGTH; i++) {
		if (bPawns[i] > 1) {
			//more than 1 pawn on one row
			rating += 50;
		}
		if (bPawns[i] == 0) {
			if ((!left2) && left) {
				rating += 25;
			}
		}
		left2 = left;
		left = (bPawns[i] >= 1);
	}

	/*ROOK ON OPEN FILE BONUS*/
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char sq = b.whitePiecesList[3][i];
		if (sq != NONE){
			char index = (sq % 10) - 1;
			if (pawns[index] == 0 && bPawns[index] == 0) {
				rating += 15;
			}
		}
	}
	for (int i = 0; i < MAX_NUMBER_OF_PIECES; i++) {
		char sq = b.blackPiecesList[3][i];
		if (sq != NONE) {
			char index = (sq % 10) - 1;
			if (bPawns[index] == 0 && pawns[index] == 0) {
				rating -= 15;
			}
		}
	}
	/*ROOK ON OPEN FILE NNUS END*/
	return rating;
}

int kingToCorners(Board b) {
	int currentRating = 0;
	currentRating += KingEnd[to64index(b.whitePiecesList[5][0])];
	currentRating -= KingEnd[to64index(b.blackPiecesList[5][0])];
	return currentRating;
}

int endGameRating(Board b) {
	int rating = 0;
	rating += bonusEndPossitionRate(b);
	rating += mobilityRate(b);
	rating += doubleBishopRate(b);
	rating += pawnRatingEnd(b);
	rating += kingToCorners(b);
	return rating;
}
//position startpos moves e2e4 g8f6 e4e5
int beginningGameRating(Board b) {
	int rating = 0;
	rating = rating + bonusPossitionRate(b);
	rating = rating + doubleBishopRate(b);
	rating = rating + pawnRatingWithKingSafety(b);
	rating = rating + mobilityRate(b);
	rating = rating + kingsSafety(b);
	return rating;
}
void debugRate(Board b) {
	std::cout << "Bonus: " <<  bonusPossitionRate(b) << std::endl;
	std::cout << "Double Bishop: " << doubleBishopRate(b) << std::endl;
	std::cout << "Pawn: " << pawnRatingWithKingSafety(b) << std::endl;
	std::cout << "Mobility: " << mobilityRate(b) << std::endl;
	std::cout << "King: " << kingsSafety(b) << std::endl;
}
int rateBoard(Board b) {
	int rating = 0;
	bool endGame = b.sumOfWhitePieces < 1800 && b.sumOfBlackPieces < 1800;
	/*rating = basicFigureRate(b, &endGame);
	if (rating !=  (b.sumOfWhitePieces - b.sumOfBlackPieces)) {
		std::cout << "ERROR " << std::endl;
	}*/
	rating = b.sumOfWhitePieces - b.sumOfBlackPieces;
	if (b.isDraw()) {
		return 0;
	}
	if (endGame) {
		rating = rating + endGameRating(b);
	}
	else {
		rating = rating + beginningGameRating(b);
	}

	if (b.whiteOnMove) {
		return rating;
	}
	else {
		return -rating;
	}
	
}

U64 CENTER = ((1ULL << f6) |
(1ULL << e6) |
(1ULL << d6) |
(1ULL << c6) |
(1ULL << c5) |
(1ULL << d5) |
(1ULL << e5) |
(1ULL << f5) |
(1ULL << f4) |
(1ULL << e4) |
(1ULL << d4) |
(1ULL << c4) |
(1ULL << c3) |
(1ULL << d3) |
(1ULL << e3) |
(1ULL << f3));

U64 TOTAL_CENTER = (
(1ULL << d5) |
(1ULL << e5) |
(1ULL << e4) |
(1ULL << d4)
);

U64 SIDE = ((1ULL << f6) |
(1ULL << e6) |
(1ULL << d6) |
(1ULL << c6) |
(1ULL << c5) |
(1ULL << d5) |
(1ULL << e5) |
(1ULL << f5) |
(1ULL << f4) |
(1ULL << e4) |
(1ULL << d4) |
(1ULL << c4) |
(1ULL << c3) |
(1ULL << d3) |
(1ULL << e3) |
(1ULL << f3) |(1ULL << f2) |
(1ULL << e2) |
(1ULL << d2) |
(1ULL << c2) |
(1ULL << c7) |
(1ULL << d7) |
(1ULL << e7) |
(1ULL << f7) |
(1ULL << g4) |
(1ULL << g5) |
(1ULL << g6) |
(1ULL << g3) |
(1ULL << b4) |
(1ULL << b5) |
(1ULL << b6) |
(1ULL << b3) |(1ULL << a4) |
(1ULL << a5) |
(1ULL << a6) |
(1ULL << a3) | (1ULL << h4) |
(1ULL << h5) |
(1ULL << h6) |
(1ULL << h3));
namespace {
	U64 H_FILE = ((1ULL << h1) |
		(1ULL << h2) |
		(1ULL << h3) |
		(1ULL << h4) |
		(1ULL << h5) |
		(1ULL << h6) |
		(1ULL << h7) |
		(1ULL << h8));
	U64 G_FILE = H_FILE >> 1;
	U64 F_FILE = G_FILE >> 1;
	U64 E_FILE = F_FILE >> 1;
	U64 D_FILE = E_FILE >> 1;
	U64 C_FILE = D_FILE >> 1;
	U64 B_FILE = C_FILE >> 1;
	U64 A_FILE = B_FILE >> 1;
	const U64 files[] = { H_FILE,G_FILE, F_FILE, E_FILE, D_FILE, C_FILE, B_FILE, A_FILE };
	U64 NOT_H_FILE = ~((1ULL << h1) |
		(1ULL << h2) |
		(1ULL << h3) |
		(1ULL << h4) |
		(1ULL << h5) |
		(1ULL << h6) |
		(1ULL << h7) |
		(1ULL << h8));
	U64 NOT_A_FILE = ~((1ULL << a1) |
		(1ULL << a2) |
		(1ULL << a3) |
		(1ULL << a4) |
		(1ULL << a5) |
		(1ULL << a6) |
		(1ULL << a7) |
		(1ULL << a8));
	//U64 ROW_8 = 0b1111111100000000000000000000000000000000000000000000000000000000;
	//U64 ROW_1 = 0b0000000000000000000000000000000000000000000000000000000011111111;

	U64 ROW_8 = 0b1111111100000000000000000000000000000000000000000000000000000000;
	U64 ROW_7 = ROW_8 >> UP;
	U64 ROW_6 = ROW_7 >> UP;
	U64 ROW_5 = ROW_6 >> UP;
	U64 ROW_4 = ROW_5 >> UP;
	U64 ROW_3 = ROW_4 >> UP;
	U64 ROW_2 = ROW_3 >> UP;
	U64 ROW_1 = ROW_2 >> UP;

	/*char POP(U64 *bb) {
		U64 b = *bb ^ (*bb - 1);
		unsigned int fold = (unsigned)((b & 0xffffffff) ^ (b >> 32));
		*bb &= (*bb - 1);
		return BitTable[(fold * 0x783a9b23) >> 26];
	}*/

	char COUNT(U64 b) {
		int r;
		for (r = 0; b; r++, b &= b - 1);
		return r;
	}
}
int centerControl(Bitboard b) {
	return -1;
}

int piecesRate(Bitboard b, bool *end) {
	int score = 0;
	for (int i = 0; i < 5; i++) {
		score += COUNT(b.whitePiecesList[i]) * pieceValueTemp[i];
	}
	*end = score < 1800;
	for (int i = 0; i < 5; i++) {
		score -= COUNT(b.blackPiecesList[i]) * pieceValueTemp[i];
	}
	return score;
}

U64 passed_pawns_offsets_white[64];
U64 passed_pawns_offsets_black[64];

const char rank_of_square[64] = {0,0,0,0,0,0,0,0,
1,1,1,1,1,1,1,1,
2,2,2,2,2,2,2,2,
3,3,3,3,3,3,3,3,
4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,
6,6,6,6,6,6,6,6,
7,7,7,7,7,7,7,7};

const int PawnIsolated = 10;
const int PawnPassed[8] = { 0, 5, 10, 20, 35, 60, 100, 200 };
const int PawnPassedBlack[8] = { 200, 100, 60, 35, 20, 10, 5, 0 };
const int RookOpenFile = 10;
const int RookSemiOpenFile = 5;
const int QueenOpenFile = 5;
const int QueenSemiOpenFile = 3;
const int BishopPair = 30;


void initPassedPawnsOffsets() {
	for (int i = a2; i <= h6; i++) {
		U64 temp = 1ULL << i;
		temp = ((temp & NOT_H_FILE) << LEFT_UP) | ((temp & NOT_A_FILE) << RIGHT_UP) | temp << UP;
		while (!(temp & ROW_7)) {
			temp |= temp << UP;
		}
		passed_pawns_offsets_white[i] = temp;
	}
	for (int i = a7; i <= h8; i++) {
		passed_pawns_offsets_white[i] = 0;
	}
	for (int i = a1; i < a2; i++) {
		passed_pawns_offsets_white[i] = 0;
	}

	//Black
	for (int i = a3; i <= h7; i++) {
		U64 temp = 1ULL << i;
		temp = ((temp & NOT_A_FILE) >> LEFT_UP) | ((temp & NOT_H_FILE) >> RIGHT_UP) | temp >> UP;
		while (!(temp & ROW_2)) {
			temp |= temp >> UP;
		}
		passed_pawns_offsets_black[i] = temp;
	}
	for (int i = a1; i < a3; i++) {
		passed_pawns_offsets_white[i] = 0;
	}
	for (int i = a8; i <= h8; i++) {
		passed_pawns_offsets_white[i] = 0;
	}
}
U64 squareToFile[64];
void initSquareToFile() {
	for (int i = a1; i <= h1; i++) {
		U64 temp = 1ULL << i;
		while (!(temp & ROW_8)) {
			temp |= temp << UP;
		}
		squareToFile[i] = temp;
		squareToFile[i + 8] = temp;
		squareToFile[i + 16] = temp;
		squareToFile[i + 24] = temp;
		squareToFile[i + 32] = temp;
		squareToFile[i + 40] = temp;
		squareToFile[i + 48] = temp;
		squareToFile[i + 56] = temp;
	}
}

U64 AroundKingW[64];
U64 AroundKingB[64];
U64 AroundKingLargerW[64];
U64 AroundKingLargerB[64];

namespace {
	void printBitboard(U64 b) {
		std::cout << std::endl;
		for (int i = 0; i < 8; i++) {
			char a = (char)b;
			std::cout << std::bitset<8>(a) << std::endl;
			b = b >> 8;
		}
		std::cout << std::endl;
	}
}
void initAroundKing() {
	for (char i = a1; i <= h8; i++) {
		U64 temp = 1ULL << i;
		temp |= (temp & NOT_A_FILE) >> LEFT;
		temp |= (temp & NOT_H_FILE) << LEFT;
		temp |= (temp & (~ROW_8)) << UP;
		temp |= (temp & (~ROW_8)) << UP;
		AroundKingW[i] = temp;
		AroundKingLargerW[i] = (temp & (~ROW_8)) << UP;
	}
	for (char i = a1; i <= h8; i++) {
		U64 temp = 1ULL << i;
		temp |= (temp & NOT_A_FILE) >> LEFT;
		temp |= (temp & NOT_H_FILE) << LEFT;
		temp |= (temp & (~ROW_1)) >> UP;
		temp |= (temp & (~ROW_1)) >> UP;
		AroundKingB[i] = temp;
		AroundKingLargerB[i] = (temp & (~ROW_1)) >> UP;
	}
}

void InitRatingStructures() {
	initPassedPawnsOffsets();
	initSquareToFile();
	initAroundKing();
}
char pawnStructureWhite[] = { 0,0,0,0,0,0,0,0,0,0 };
char pawnStructureBlack[] = { 0,0,0,0,0,0,0,0,0,0 };

char mirrorSquare[64] = { h8,g8,f8,e8,d8,c8,b8,a8,
h7,g7,f7,e7,d7,c7,b7,a7,
h6,g6,f6,e6,d6,c6,b6,a6,
h5,g5,f5,e5,d5,c5,b5,a5,
h4,g4,f4,e4,d4,c4,b4,a4,
h3,g3,f3,e3,d3,c3,b3,a3,
h2,g2,f2,e2,d2,c2,b2,a2,
h1,g1,f1,e1,d1,c1,b1,a1 };

const int unsupportedPawn = 5;
const int unsupportedPawnEnd = 10;

int GetBonus(Bitboard *b) {
	U64 temp = 0;
	int rating = 0;
	for (int i = 0; i < 5; i++) {
		temp = b->whitePiecesList[i];
		while (temp) {
			rating += bonusArrays[i][POP(&temp)];
		}
		temp = b->blackPiecesList[i];
		while (temp) {
			rating -= bonusArrays[i][mirrorSquare[POP(&temp)]];
		}
	}
	return rating;
}

void debugRate(Bitboard *b, bool whiteOnMove) {
	int rating = 0, t = 0;
	bool endGame = false;
	for (int i = 0; i < 5; i++) {
		rating += COUNT(b->whitePiecesList[i]) * pieceValueTemp[i];
	}
	std::cout << "WHITE PIECES:" << rating << std::endl;
	endGame = rating < 1800;
	for (int i = 0; i < 5; i++) {
		t += COUNT(b->blackPiecesList[i]) * pieceValueTemp[i];
	}
	std::cout << "BLACK PIECES:" << t << std::endl;
	U64 temp = 0;
	
	for (int i = 0; i < 4; i++) {
		temp = b->whitePiecesList[i];
		rating = 0;
		while (temp) {
			rating += bonusArrays[i][POP(&temp)];
		}
		std::cout << "WHITE "<< i <<" BONUS:" << rating << std::endl;
	}
	for (int i = 0; i < 4; i++) {
		temp = b->blackPiecesList[i];
		rating = 0;
		while (temp) {
			rating += bonusArrays[i][mirrorSquare[POP(&temp)]];
		}
		std::cout << "BLACK " << i << " BONUS:" << rating << std::endl;
	}

	if (endGame) {
		std::cout << "END GAME" << std::endl;
		rating = 0;
		U64 t = b->whitePiecesList[5];
		char whiteKingSq = POP(&t);
		rating += KingE[whiteKingSq];
		std::cout << "WHITE KING POSITION: " << rating << std::endl;
		rating = 0;
		t = b->blackPiecesList[5];
		char blackKingSq = POP(&t);
		rating += KingE[mirrorSquare[blackKingSq]];
		std::cout << "BLACK KING POSITION: " << rating << std::endl;
		
		//MOBILITY RATE
		rating = 0;
		rating += (b->countMoves(true) - b->countMoves(false)) * 3;
		std::cout << "MOBILITY RATE: " << rating << std::endl;

		rating = 0;
		rating += COUNT(b->whitePiecesList[2]) * 5; // Double bishop
		std::cout << "DOUBLE BISHOP: " << rating << std::endl;
		rating = 0;
		rating += COUNT(b->blackPiecesList[2]) * 5; // Double bishop
		std::cout << "DOUBLE BISHOP: " << rating << std::endl;

		//PASSED PAWNS
		rating = 0;
		U64 temp = b->whitePiecesList[0];
		while (temp) {
			char sq = POP(&temp);
			if (!(passed_pawns_offsets_white[sq] & b->blackPiecesList[0])) {
				rating += PawnPassed[rank_of_square[sq]];
			}
		}
		std::cout << "WHITE PASSED PAWNS: " << rating << std::endl;
		rating = 0;
		temp = b->blackPiecesList[0];
		while (temp) {
			char sq = POP(&temp);
			if (!(passed_pawns_offsets_black[sq] & b->whitePiecesList[0])) {
				rating += PawnPassedBlack[rank_of_square[sq]];
			}
		}
		std::cout << "BLACK PASSED PAWNS: " << rating << std::endl;

		// PAWNS on same line
		rating = 0;
		for (int i = 0; i < 8; i++) {
			pawnStructureWhite[i + 1] = COUNT(b->whitePiecesList[0] & files[i]);
			//overallWhitePawns += pawnStructureWhite[i + 1];
			if (pawnStructureWhite[i + 1] > 1) {
				rating -= 40;
			}
			pawnStructureBlack[i + 1] = COUNT(b->blackPiecesList[0] & files[i]);
			//overallBlackPawns += pawnStructureBlack[i + 1];
			if (pawnStructureBlack[i + 1] > 1) {
				rating += 40;
			}
		}
		std::cout << "BOTH PAWNS ON SAME LINE: " << rating << std::endl;
		//PAWNS (Cripled Pawns = pawn on A or H file)

		//PAWNS structure (alone pawn)
		rating = 0;
		for (int i = 1; i < 9; i++) {
			if (pawnStructureWhite[i + 1] == 0 && pawnStructureWhite[i - 1] == 0 && pawnStructureWhite[i] != 0) {
				rating -= PawnIsolated * 2;
			}
			if (pawnStructureBlack[i + 1] == 0 && pawnStructureBlack[i - 1] == 0 && pawnStructureBlack[i] != 0) {
				rating += PawnIsolated * 2;
			}
		}
		std::cout << "BOTH PAWN STRUCTURE: " << rating << std::endl;
		//PAWN IN FRONT OF A KING
		/*U64 pos = ((b->whitePiecesList[5] & (~ROW_8)) << UP)
			| (((b->whitePiecesList[5] & NOT_H_FILE) & (~ROW_8)) << LEFT_UP)
			| (((b->whitePiecesList[5] & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP);
		rating += COUNT(pos & b->whitePiecesList[0]) * 10;

		pos = ((b->blackPiecesList[5] & (~ROW_1)) >> UP) |
			(((b->blackPiecesList[5] & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) |
			(((b->blackPiecesList[5] & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP);
		rating -= COUNT(pos & b->blackPiecesList[0]) * 10;*/

		//ROOKS on empty file
		rating = 0;
		U64 pos = b->whitePiecesList[3];
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureWhite[t] == 0) {
				if (pawnStructureBlack[t] == 0) {
					rating += RookOpenFile;
				}
				else {
					rating += RookSemiOpenFile;
				}
			}
		}
		std::cout << "WHITE ROOKS EMPTY FILE: " << rating << std::endl;
		rating = 0;
		pos = b->blackPiecesList[3];
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureBlack[t] == 0) {
				if (pawnStructureWhite[t] == 0) {
					rating += RookOpenFile;
				}
				else {
					rating += RookSemiOpenFile;
				}
			}
		}
		std::cout << "BLACK ROOKS EMPTY FILE: " << rating << std::endl;
	}
	else {
		std::cout << "MIDDLE GAME " << std::endl;
		//MOBILITY RATE
		rating = 0;
		rating += (b->countMoves(true) - b->countMoves(false)) * 3;
		std::cout << "MOBILITY RATE: " << rating << std::endl;
		//DOUBLE BISHOP
		rating = 0;
		rating += COUNT(b->whitePiecesList[2])  == 2 ? 30 : 0; // Double bishop
		rating -= COUNT(b->blackPiecesList[2])  == 2 ? 30 : 0; // Double bishop
		std::cout << "DOUBLE BISHOPS: " << rating << std::endl;

		// KING SAFETY - PAWN IN FRONT
		U64 pos = ((b->whitePiecesList[5] & (~ROW_8)) << UP)
			| (((b->whitePiecesList[5] & NOT_H_FILE) & (~ROW_8)) << LEFT_UP)
			| (((b->whitePiecesList[5] & NOT_A_FILE) & (~ROW_8)) << RIGHT_UP);
		rating = 0;
		rating += COUNT(pos & b->whitePieces) * 6;
		rating += COUNT(pos & b->whitePiecesList[0]) * 10;
		std::cout << "WHITE PIECES IN FRONT OF A KING: " << rating << std::endl;

		pos = ((b->blackPiecesList[5] & (~ROW_1)) >> UP) |
			(((b->blackPiecesList[5] & NOT_A_FILE) & (~ROW_1)) >> LEFT_UP) |
			(((b->blackPiecesList[5] & NOT_H_FILE) & (~ROW_1)) >> RIGHT_UP);
		rating = 0;
		rating += COUNT(pos & b->blackPieces) * 6;
		rating += COUNT(pos & b->blackPiecesList[0]) * 10;
		std::cout << "BLACK PIECES IN FRONT OF A KING: " << rating << std::endl;

		//KING SAFETY - casteling moves
		rating = 0;
		if (b->whiteCastleAlowed == Made) {
			rating += 20; // TODO devide by number of moves
		}
		if (b->blackCastleAlowed == Made) {
			rating -= 20;
		}
		std::cout << "CASTELING MOVE: " << rating << std::endl;

		//KING SAFETY - empty file
		rating = 0;
		U64 t = b->whitePiecesList[5];
		char whiteKingSq = POP(&t);
		if ((squareToFile[whiteKingSq] & b->blackPiecesList[0]) == 0) {
			rating -= 20;// no opponent pawn in a way
			rating -= COUNT(squareToFile[whiteKingSq] & b->blackPiecesList[3]) * 15; // opponent rook pointing at my king
		}
		std::cout << "WHITE KING SAFETY ENPTY FILE: " << rating << std::endl;
		rating = 0;
		t = b->blackPiecesList[5];
		char blackKingSq = POP(&t);
		if ((squareToFile[blackKingSq] & b->whitePiecesList[0]) == 0) {
			rating += 20;// no opponent pawn in a way
			rating += COUNT(squareToFile[blackKingSq] & b->whitePiecesList[3]) * 15; // opponent rook pointing at my king
		}
		std::cout << "BLACK KING SAFETY ENPTY FILE: " << rating << std::endl;
		// KING SAFETY - square bonus
		rating = 0;
		rating += KingO[whiteKingSq];
		rating -= KingO[mirrorSquare[blackKingSq]];
		std::cout << "KING POSITION: " << rating << std::endl;

		//PAWNS RATE - advancing pawns
		rating = 0;
		rating += COUNT(b->whitePiecesList[0] & ROW_7) * 15;
		rating += COUNT(b->whitePiecesList[0] & ROW_6) * 5;
		rating += COUNT(b->whitePiecesList[0] & ROW_5) * 3;

		rating -= COUNT(b->blackPiecesList[0] & ROW_2) * 15;
		rating -= COUNT(b->blackPiecesList[0] & ROW_3) * 5;
		rating -= COUNT(b->blackPiecesList[0] & ROW_4) * 3;
		std::cout << "PAWN ADVANCING: " << rating << std::endl;
		rating = 0;
		// PAWNS on same line
		for (int i = 0; i < 8; i++) {
			pawnStructureWhite[i + 1] = COUNT(b->whitePiecesList[0] & files[i]);
			//overallWhitePawns += pawnStructureWhite[i + 1];
			if (pawnStructureWhite[i + 1] > 1) {
				rating -= 40;
			}
			pawnStructureBlack[i + 1] = COUNT(b->blackPiecesList[0] & files[i]);
			//overallBlackPawns += pawnStructureBlack[i + 1];
			if (pawnStructureBlack[i + 1] > 1) {
				rating += 40;
			}
		}
		std::cout << "PAWN SAME LINE: " << rating << std::endl;
		//PAWNS (Cripled Pawns = pawn on A or H file)
		rating = 0;
		//PAWNS structure (alone pawn)
		for (int i = 1; i < 9; i++) {
			if (pawnStructureWhite[i + 1] == 0 && pawnStructureWhite[i - 1] == 0 && pawnStructureWhite[i] != 0) {
				rating -= PawnIsolated;
			}
			if (pawnStructureBlack[i + 1] == 0 && pawnStructureBlack[i - 1] == 0 && pawnStructureBlack[i] != 0) {
				rating += PawnIsolated;
			}
		}
		std::cout << "PAWN ISOLATED: " << rating << std::endl;
		rating = 0;
		//ROOKS on empty file
		pos = b->whitePiecesList[3];
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureWhite[t] == 0) {
				if (pawnStructureBlack[t] == 0) {
					rating += RookOpenFile;
				}
				else {
					rating += RookSemiOpenFile;
				}
			}
		}
		pos = b->blackPiecesList[3];
		std::cout << "ROOK FILE: " << rating << std::endl;
		rating = 0;
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureBlack[t] == 0) {
				if (pawnStructureWhite[t] == 0) {
					rating -= RookOpenFile;
				}
				else {
					rating -= RookSemiOpenFile;
				}
			}
		}
		std::cout << "ROOK FILE: " << rating << std::endl;
	}
	/*if (whiteOnMove) {
		if (b.history.length % 2 != 0) {
			std::cout << "ERROR white is not on move" << std::endl;
		}
	}*/
}
int rateBoard(Bitboard* b, bool whiteOnMove) {
	int rating = 0;
	//bool endGame = b.sumOfWhitePieces < 1800 && b.sumOfBlackPieces < 1800;
	/*rating = basicFigureRate(b, &endGame);
	if (rating !=  (b.sumOfWhitePieces - b.sumOfBlackPieces)) {
		std::cout << "ERROR " << std::endl;
	}*/
	bool endGame = false;
	//rating = piecesRate(b, &endGame);
	for (int i = 0; i < 5; i++) {
		rating += COUNT(b->whitePiecesList[i]) * pieceValueTemp[i];
	}
	endGame = rating < 1800;
	for (int i = 0; i < 5; i++) {
		rating -= COUNT(b->blackPiecesList[i]) * pieceValueTemp[i];
	}
	if (endGame && b->isDraw()) {
		return 0;
	}
	U64 temp = 0;
	for (int i = 0; i < 5; i++) {
		temp = b->whitePiecesList[i];
		while (temp) {
			rating += bonusArrays[i][POP(&temp)];
		}
		temp = b->blackPiecesList[i];
		while (temp) {
			rating -= bonusArrays[i][mirrorSquare[POP(&temp)]];
		}
	}
	if (endGame) {
		U64 t = b->whitePiecesList[5];
		char whiteKingSq = POP(&t);
		rating += KingE[whiteKingSq];
		t = b->blackPiecesList[5];
		char blackKingSq = POP(&t);
		rating -= KingE[mirrorSquare[blackKingSq]];

		//MOBILITY RATE
		rating += (b->countMoves(true) - b->countMoves(false)) * 3;

		//DOUBLE BISHOP RATE
		rating += COUNT(b->whitePiecesList[2]) == 2? 30:0; // Double bishop
		rating -= COUNT(b->blackPiecesList[2]) ==2? 30:0; // Double bishop
		
		//PASSED PAWNS
		U64 temp = b->whitePiecesList[0];
		while (temp) {
			char sq = POP(&temp);
			if (!(passed_pawns_offsets_white[sq] & b->blackPiecesList[0])) {
				rating += PawnPassed[rank_of_square[sq]];
			}
		}
		temp = b->blackPiecesList[0];
		while (temp) {
			char sq = POP(&temp);
			if (!(passed_pawns_offsets_black[sq] & b->whitePiecesList[0])) {
				rating -= PawnPassedBlack[rank_of_square[sq]];
			}
		}

		// PAWNS on same line
		for (int i = 0; i < 8; i++) {
			pawnStructureWhite[i + 1] = COUNT(b->whitePiecesList[0] & files[i]);
			//overallWhitePawns += pawnStructureWhite[i + 1];
			if (pawnStructureWhite[i + 1] > 1) {
				rating -= 40;
			}
			pawnStructureBlack[i + 1] = COUNT(b->blackPiecesList[0] & files[i]);
			//overallBlackPawns += pawnStructureBlack[i + 1];
			if (pawnStructureBlack[i + 1] > 1) {
				rating += 40;
			}
		}

		//PAWNS structure (alone pawn)
		for (int i = 1; i < 9; i++) {
			if (pawnStructureWhite[i + 1] == 0 && pawnStructureWhite[i - 1] == 0 && pawnStructureWhite[i] != 0) {
				rating -= PawnIsolated * 2;
			}
			if (pawnStructureBlack[i + 1] == 0 && pawnStructureBlack[i - 1] == 0 && pawnStructureBlack[i] != 0) {
				rating += PawnIsolated * 2;
			}
		}

		// PAWN UNSUPPORTED
		temp = ((b->whitePiecesList[0] & NOT_H_FILE) << LEFT_UP) | ((b->whitePiecesList[0] & NOT_A_FILE) << RIGHT_UP);
		temp = b->whitePiecesList[0] & (~temp);
		rating -= COUNT(temp) * unsupportedPawnEnd;

		temp = ((b->blackPiecesList[0] & NOT_A_FILE) >> LEFT_UP) | ((b->blackPiecesList[0] & NOT_H_FILE) >> RIGHT_UP);
		temp = b->blackPiecesList[0] & (~temp);
		rating += COUNT(temp) * unsupportedPawnEnd;

		//ROOKS on empty file
		U64 pos = b->whitePiecesList[3];
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureWhite[t] == 0) {
				if (pawnStructureBlack[t] == 0) {
					rating += RookOpenFile;
				}
				else {
					rating += RookSemiOpenFile;
				}
			}
		}
		pos = b->blackPiecesList[3];
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureBlack[t] == 0) {
				if (pawnStructureWhite[t] == 0) {
					rating -= RookOpenFile;
				}
				else {
					rating -= RookSemiOpenFile;
				}
			}
		}
	}
	else {
		//begining and midle game
		//MOBILITY RATE
		rating += (b->countMoves(true) - b->countMoves(false)) * 3;
		
		//DOUBLE BISHOP
		rating += COUNT(b->whitePiecesList[2]) == 2?30:0; // Double bishop
		rating -= COUNT(b->blackPiecesList[2]) == 2? 30:0; // Double bishop

		// KING SAFETY - PAWN IN FRONT
		U64 pos = b->whitePiecesList[5];
		char whiteKingSq = POP(&pos);
		temp = AroundKingW[whiteKingSq];
		
		rating += COUNT(temp & b->whitePieces) * 5;
		rating += COUNT(temp & b->whitePiecesList[0]) * 5;
		temp = AroundKingLargerW[whiteKingSq];
		rating -= COUNT(temp & b->blackPieces) * 10;
		rating -= COUNT(temp & b->blackPiecesList[0]) * 10;

		pos = b->blackPiecesList[5];
		char blackKingSq = POP(&pos);
		temp = AroundKingB[blackKingSq];
		rating += COUNT(temp & b->whitePieces) * 5;
		rating += COUNT(temp & b->whitePiecesList[0]) * 5;
		temp = AroundKingLargerB[whiteKingSq];
		rating -= COUNT(temp & b->blackPieces) * 10;
		rating -= COUNT(temp & b->blackPiecesList[0]) * 10;

		//KING SAFETY - casteling moves
		if (b->whiteCastleAlowed == Made) {
			rating += 20; // TODO devide by number of moves
		}
		if (b->blackCastleAlowed == Made) {
			rating -= 20;
		}

		//KING SAFETY - empty file
		if (!(squareToFile[whiteKingSq] & b->blackPiecesList[0])) {
			rating -= 50;// no opponent pawn in a way
			rating -= COUNT(squareToFile[whiteKingSq] & b->blackPiecesList[3]) * 15; // opponent rook pointing at my king
		}

		if (!(squareToFile[blackKingSq] & b->whitePiecesList[0])) {
			rating += 50;// no opponent pawn in a way
			rating += COUNT(squareToFile[blackKingSq] & b->whitePiecesList[3]) * 15; // opponent rook pointing at my king
		}

		// KING SAFETY - square bonus
		rating += KingO[whiteKingSq];
		rating -= KingO[mirrorSquare[blackKingSq]];

		// PAWNS on same line
		for (int i = 0; i < 8; i++) {
			pawnStructureWhite[i + 1] = COUNT(b->whitePiecesList[0] & files[i]);
			//overallWhitePawns += pawnStructureWhite[i + 1];
			if (pawnStructureWhite[i + 1] > 1) {
				rating -= 40;
			}
			pawnStructureBlack[i + 1] = COUNT(b->blackPiecesList[0] & files[i]);
			//overallBlackPawns += pawnStructureBlack[i + 1];
			if (pawnStructureBlack[i + 1] > 1) {
				rating += 40;
			}
		}

		//PAWNS structure (alone pawn)
		for (int i = 1; i < 9; i++) {
			if (pawnStructureWhite[i + 1] == 0 && pawnStructureWhite[i - 1] == 0 && pawnStructureWhite[i] != 0) {
				rating -= PawnIsolated;
			}
			if (pawnStructureBlack[i + 1] == 0 && pawnStructureBlack[i - 1] == 0 && pawnStructureBlack[i] != 0) {
				rating += PawnIsolated;
			}
		}

		// PAWN UNSUPPORTED
		temp = ((b->whitePiecesList[0] & NOT_H_FILE) << LEFT_UP) | ((b->whitePiecesList[0] & NOT_A_FILE) << RIGHT_UP);
		temp = b->whitePiecesList[0] & (~temp);
		rating -= COUNT(temp) * unsupportedPawn;

		temp = ((b->blackPiecesList[0] & NOT_A_FILE) >> LEFT_UP) | ((b->blackPiecesList[0] & NOT_H_FILE) >> RIGHT_UP);
		temp = b->blackPiecesList[0] & (~temp);
		rating += COUNT(temp) * unsupportedPawn;

		//ROOKS on empty file
		pos = b->whitePiecesList[3];
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureWhite[t] == 0) {
				if (pawnStructureBlack[t] == 0) {
					rating += RookOpenFile;
				}
				else {
					rating += RookSemiOpenFile;
				}
			}
		}
		pos = b->blackPiecesList[3];
		while (pos) {
			char sq = POP(&pos);
			char t = (sq % 8) + 1;
			if (pawnStructureBlack[t] == 0) {
				if (pawnStructureWhite[t] == 0) {
					rating -= RookOpenFile;
				}
				else {
					rating -= RookSemiOpenFile;
				}
			}
		}
	}
	if (whiteOnMove) {
		return rating;
	}
	else {
		return -rating;
	}

}