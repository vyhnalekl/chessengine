#include "stdafx.h"

#ifndef DEFINITIONS_CPP
#define DEFINITIONS_CPP
#define END_GAME 1
#define MIDDLE_GAME 0
#define INPUTBUFFER 400 * 6
#define OUTPUTBUFFER 400 * 6
#define INFINITE 100000
#define CHESSBOARD_LENGTH 120 // 120 chessboard
#define MAX_GAME_DEPTH 1024 // max depth of history (halfmoves)
#define BLACK_OFFSET 7 // the index where black figures starts
#define MAX_NUMBER_OF_PIECES 10 // maximum number of pieces on a board
#define CHECK_AFTER_MOVE false;
// Piece move offsets
#define PAWN_OFFSET 10
#define KNIGHT_OFFSET_LENGTH 8
#define BISHOP_OFFSET_LENGTH 4
#define ROOK_OFFSET_LENGTH 4
#define QUEEN_OFFSET_LENGTH 8
#define KING_OFFSET_LENGTH 8
const char knightOffsets[] = { -21, -19, -12, -8, 8, 12, 19, 21 };
const char bishopOffsets[] = { -11,  -9,  9, 11 };
const char rookOffsets[] = { -10,  -1,  1, 10 };
const char queenOffsets[] = { -11, -10, -9, -1, 1,  9, 10, 11 };
const char kingOffsets[] = { -11, -10, -9, -1, 1,  9, 10, 11 };
// threathens offset - only for pawn for other pieces same as move
#define PAWN_THREATHENS_LEFT 9
#define PAWN_THREATHENS_RIGHT 11

#define PIECES_ENUM_LENGTH 13
enum {EMPTY = 0, whitePawn, whiteKnight, whiteBishop, whiteRook, whiteQueen, whiteKing,
	blackPawn, blackKnight, blackBishop, blackRook, blackQueen, blackKing };

enum { BothSides = 0, KingSide = 1, QueenSide = 2, NO = 3, Made = 4};

enum {
	A1 = 21, B1, C1, D1, E1, F1, G1, H1,
	A2 = 31, B2, C2, D2, E2, F2, G2, H2,
	A3 = 41, B3, C3, D3, E3, F3, G3, H3,
	A4 = 51, B4, C4, D4, E4, F4, G4, H4,
	A5 = 61, B5, C5, D5, E5, F5, G5, H5,
	A6 = 71, B6, C6, D6, E6, F6, G6, H6,
	A7 = 81, B7, C7, D7, E7, F7, G7, H7,
	A8 = 91, B8, C8, D8, E8, F8, G8, H8,
	NONE = 100
};

struct Move {
	//seted when generating move
	char from ; // from where
	char to; // end up 
	//not needed anymore
	//bool check = false; // whether move is check
	char currentCastleWhite;
	char currentCastleBlack;
	bool isEnPassantMove = false;
	bool isCastelingMove = false;
	char numberOfMovesWithoutTake;
	unsigned long long enPassant = 0; // when pawn is moved by 2, represents a square
	char transforms = EMPTY; // piece to which this one transforms, set only when pawn gets to other end of board
	//seted when move is played
	char taken = EMPTY; // taken piece
	char indexOfTaken; // index in pieces list
	char indexOfMoved; // iniex in pieces list
	char indexOfTransformed; // index in pieces list
	unsigned long long keyBeforeMove;
	unsigned long long keyAfterMove;
	//for Search and moves ordering
	int score = 0;
	char capturedPiece = EMPTY;
	char movedPiece;
	unsigned long long enPassantBeforeMove;

};
/*
struct ThreatheningPiece{
	char possition;
	char squaresInWay[8];
};*/

#define NO_RECORD 0


struct Transposition {
	Move move; // move played in this position
	int score = 0; // score of this position
	unsigned long long hashKey = 0; // full hash key
	int depth = 0; // represent a current depth of how far the serach went (for example 3 means it searched to depth 3 from this position on)
	bool init = true;
	bool white;
	char flag;
	//char pieces[120]; //TODO remove
};

struct SearchData
{
	//used in search
	time_t timeOnMove;
	int maxDepth;
	int maxDepthQuiesce;
	int currentDepth;
	bool end; // if set to true, the search finishes as soon as it can
	bool searchInFile;
	// Statistics
	unsigned int depthReached = 0;
	unsigned int numberOfPositions = 0;
	unsigned int quiescencePositions = 0;

};

#define MAX_GENERATE_MOVES 100 // maximum number of moves that can be generated in generateMove method of Board
#define MAX_HISTORY_MOVES 25000 // maximum number of history moves, in board class
#define QUIESCENE_MAX_DEPTH -15 // starts with depth = -1 decrements with every call

#define PAWN_RATING 100
#define BISHOP_RATING 300
#define KNIGHT_RATING 300
#define ROOK_RATING 500
#define QUEEN_RATING 900

const int pieceValue[] = {0, -PAWN_RATING, -KNIGHT_RATING, -BISHOP_RATING, -ROOK_RATING, -QUEEN_RATING,0,
PAWN_RATING, KNIGHT_RATING, BISHOP_RATING, ROOK_RATING, QUEEN_RATING,0 };


//End game definitions
#define BLACK_WIN -1
#define WHITE_WIN -2
#define DRAW -3
enum {EXACT, ALPHA, BETA};

// New Bitboard definitions

#define ONE_RANK (8)
#define TWO_RANKS (16)
#define UP (8)
#define DOWN (8)
#define LEFT (1)
#define RIGHT (1)
#define RIGHT_UP (7)
#define LEFT_UP (9)
#define RIGHT_DOWN (7)
#define LEFT_DOWN (9)

#define U64 unsigned long long

#define NO_SQUARE 100
enum enumSquare {
	a1, b1, c1, d1, e1, f1, g1, h1,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a7, b7, c7, d7, e7, f7, g7, h7,
	a8, b8, c8, d8, e8, f8, g8, h8
};
/*
char BitTable[64] = {
  63, 30, 3, 32, 25, 41, 22, 33, 15, 50, 42, 13, 11, 53, 19, 34, 61, 29, 2,
  51, 21, 43, 45, 10, 18, 47, 1, 54, 9, 57, 0, 35, 62, 31, 40, 4, 49, 5, 52,
  26, 60, 6, 23, 44, 46, 27, 56, 16, 7, 39, 48, 24, 59, 14, 12, 55, 38, 28,
  58, 20, 37, 17, 36, 8
};

char POP(U64 *bb) {
	U64 b = *bb ^ (*bb - 1);
	unsigned int fold = (unsigned)((b & 0xffffffff) ^ (b >> 32));
	*bb &= (*bb - 1);
	return BitTable[(fold * 0x783a9b23) >> 26];
}

char COUNT(U64 b) {
	int r;
	for (r = 0; b; r++, b &= b - 1);
	return r;
}*/


#endif 