#include "stdafx.h"
#include "Definitions.cpp"
#include <iostream>
#include "Validate.h"
#include "MoveList.h"
#include "Bitboard.h"

void printMove(struct Move move) {
	std::cout << (char)('A' - 1 + move.from % 10) << (char)('1' + (move.from - 20) / 10) << " - " <<
		(char)('A' - 1 + move.to % 10) << (char)('1' + (move.to - 20) / 10) << "\n";
	/*std::cout << "Taken Piece and index : " << move.taken << ", " << move.indexOfTaken << std::endl;
	std::cout << "Index of Moved : " << move.indexOfMoved << std::endl;
	std::cout << "Transformed : " <<move.transforms << ", "<< move.indexOfTransformed << std::endl;
	std::cout << "EnPassantMove : " << move.isEnPassantMove << ", " << move.enPassant << std::endl;*/
}

void printMoveList(MoveList* moves) {
	for(int i = 0 ; i < (*moves).length; i++){
		printMove((*moves).moves[i]);
	}
}

char getPiece(char piece) {
	if (piece == 0) {
		return '.';
	}else if (piece == 1) {
		return 'p';
	}
	else if (piece == 2) {
		return 'j';
	}
	else if (piece == 3) {
		return 'b';
	}
	else if (piece == 4) {
		return 'r';
	}
	else if (piece == 5) {
		return 'q';
	}
	else if (piece == 6) {
		return 'k';
	}
	else if (piece == 7) {
		return 'P';
	}
	else if (piece == 8) {
		return 'J';
	}
	else if (piece == 9) {
		return 'B';
	}
	else if (piece == 10) {
		return 'R';
	}
	else if (piece == 11) {
		return 'Q';
	}
	else if (piece == 12) {
		return 'K';
	}
	else {
		return '-';
	}
}

char* print64(char sq) {
	static char r[2];
	r[0] = ('a' + (sq % 8));
	r[1] = ('1' + (sq / 8));
	return r;
}

char* print642(char sq) {
	static char r[2];
	r[0] = ('a' + (sq % 8));
	r[1] = ('1' + (sq / 8));
	return r;
}

void PrintMoveList64(MoveList * ml) {
	for (int i = 0; i < ml->length; i++) {
		Move mv = ml->moves[i];
		std::cout << print64(mv.from) << "-->" << print642(mv.to) << std::endl;
	}
}

void printBoard(char pieces[120]) {
	for (int i = 0; i < 12; i++) {
		for (int j = 0; j < 10; j++) {
			std::cout << " " << getPiece(pieces[i * 10 + j]) << " ";
		}
		std::cout << std::endl;
	}
}

bool checkMove(struct Move mv, char pieces[CHESSBOARD_LENGTH]) {
	//debug
	if (mv.isEnPassantMove && pieces[mv.to] != EMPTY) {
		std::cout << "ERROR: En passant move to non empty square" << std::endl;
		return false;
	}
	if (pieces[mv.from] == EMPTY) {
		std::cout << "ERROR: Move from EMPTY square" << std::endl;
		return false;
	}
	if (pieces[mv.from] == NONE) {
		std::cout << "ERROR: Move from NONE square" << std::endl;
		return false;
	}
	if (pieces[mv.to] == NONE) {
		std::cout << "ERROR : Move to NONE square" << std::endl;
		return false;
	}
	if (sameColor(pieces[mv.from], pieces[mv.to])) {
		std::cout << "ERROR : Move (taken out own piece)" << std::endl;
		return false;
	}
	if (mv.transforms != EMPTY) {
		if ((!((isWhite(pieces[mv.from]) && (A7 <= mv.from && mv.from <= H7))
			|| (isBlack(pieces[mv.from]) && (A2 <= mv.from && mv.from <= H2)))) && ((pieces[mv.from] == whitePawn) || (pieces[mv.from] == blackPawn))) {
			std::cout << "ERROR : Transforms without being on last line or without being pawn" << std::endl;
			return false;
		}
	}
	return true;
}
bool checkMoveNoCout(struct Move mv, Bitboard* b) {
	//debug
	U64 allPieces = b->whitePieces | b->blackPieces;
	if (mv.isEnPassantMove && !(allPieces & 1ULL << mv.to)) {
		return false;
	}
	if (!(allPieces & (1ULL << mv.from))) {
		return false;
	}
	if ((allPieces & (1ULL << mv.to))) {
		if (((b->whitePieces & 1ULL<< mv.from) && (b->whitePieces & 1ULL << mv.to)) ||
			((b->blackPieces & 1ULL << mv.from) && (b->blackPieces & 1ULL << mv.to))){
			return false;
		}
	}
	return true;
}

bool checkMoveNoCout(struct Move mv, char pieces[CHESSBOARD_LENGTH]) {
	//debug
	if (mv.isEnPassantMove && pieces[mv.to] != EMPTY) {

		return false;
	}
	if (pieces[mv.from] == EMPTY) {

		return false;
	}
	if (pieces[mv.from] == NONE) {

		return false;
	}
	if (pieces[mv.to] == NONE) {

		return false;
	}
	if (sameColor(pieces[mv.from], pieces[mv.to])) {

		return false;
	}
	if (mv.transforms != EMPTY) {
		if ((!((isWhite(pieces[mv.from]) && (A7 <= mv.from && mv.from <= H7))
			|| (isBlack(pieces[mv.from]) && (A2 <= mv.from && mv.from <= H2)))) && ((pieces[mv.from] == whitePawn) || (pieces[mv.from] == blackPawn))) {

			return false;
		}
	}
	return true;
}

bool checkBoard(char pieces[CHESSBOARD_LENGTH]) {
	for (int i = 0; i < CHESSBOARD_LENGTH; i++) {
		if (A1 <= i && i <= H8 && (!(i % 10 == 0 || i % 10 == 9))) {
			if (pieces[i] == NONE) {
				std::cout << "ERROR : Chessboard have NONE value in chessboard" <<std::endl;
				return false;
			}
		}
	}
	return true;
}

bool checkPiecesList(char pieces[CHESSBOARD_LENGTH], char whitePieceList[6][MAX_NUMBER_OF_PIECES],
	char blackPieceList[6][MAX_NUMBER_OF_PIECES]) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (whitePieceList[i][j] == NONE) {
				continue;
			}
			if (pieces[whitePieceList[i][j]] != i + 1) {
				printBoard(pieces);
				std::cout << "ERROR : White piece from piece list is not on board ! " << (int)pieces[whitePieceList[i][j]] << " expected " << i + 1 << std::endl;
				return false;
			}
		}
	}

	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < MAX_NUMBER_OF_PIECES; j++) {
			if (blackPieceList[i][j] == NONE) {
				continue;
			}
			if (pieces[blackPieceList[i][j]] != i + BLACK_OFFSET) {
				std::cout << "ERROR : Black piece from piece list is not on board !" << (int)pieces[blackPieceList[i][j]] << " Should be " << i + BLACK_OFFSET <<"Idex i :" << i << "Idex j :" << j << std::endl;
				return false;
			}
		}
	}

	for (int i = 0; i < CHESSBOARD_LENGTH; i++) {
		if (pieces[i] > 0 && pieces[i] < PIECES_ENUM_LENGTH) {
			if (pieces[i] < BLACK_OFFSET) {
				for (int j = 0; j <= MAX_NUMBER_OF_PIECES; j++) {
					if (j == MAX_NUMBER_OF_PIECES) {
						std::cout << "ERROR : White piece that is on Board was not found in PieceList " << pieces[i] << " on square " << i << std::endl;
						return false;
						break;
					}
					if (whitePieceList[pieces[i] - 1][j] == i) {
						break;
					}
				}
			}
			else {
				for (int j = 0; j <= MAX_NUMBER_OF_PIECES; j++) {
					if (j == MAX_NUMBER_OF_PIECES) {
						std::cout << "ERROR : Black piece that is on Board was not found in PieceList" << std::endl;
						return false;
						break;
					}
					if (blackPieceList[pieces[i] - BLACK_OFFSET][j] == i) {
						break;
					}
				}
			}
		}
	}
	return true;
}

