#include "stdafx.h"
#include "Bitboard.h"
#include <fstream>
#include <string>
namespace {
	void ParseMove64(char* mv, Bitboard* bitboard, Move *mov) {
		char from = ((mv[1] - '1') * 8) + (mv[0] - 'a');
		char to = ((mv[3] - '1') * 8) + (mv[2] - 'a');
		mov->from = from;
		mov->to = to;
		if (bitboard->whitePiecesList[0] & (1ULL << mov->from) && mov->from == mov->to + 16) {
			mov->enPassant = mov->from - 8;
		}
		if (bitboard->blackPiecesList[0] & (1ULL << mov->from) && mov->from == mov->to - 16) {
			mov->enPassant = mov->from - 8;
		}
		//if the line above is missing engine cannot detect black casteling move
		if (bitboard->whitePiecesList[5] & (1ULL << mov->from)) {
			if ((mov->from == e1 && mov->to == g1) || (mov->from == e1 && mov->to == c1)) {
				mov->isCastelingMove = true;
			}
		}
		if (bitboard->blackPiecesList[5] & (1ULL << mov->from)) {
			if ((mov->from == e8 && mov->to == g8) || (mov->from == e8 && mov->to == c8)) {
				mov->isCastelingMove = true;
			}
		}
		if (bitboard->whitePiecesList[0] & (1ULL << mov->from)) {
			if (mv[4] != '\0') {
				if (mv[4] == 'q') {
					mov->transforms = whiteQueen;
				}
				else if (mv[4] == 'r') {
					mov->transforms = whiteRook;
				}
				else if (mv[4] == 'b') {
					mov->transforms = whiteBishop;
				}
				else if (mv[4] == 'n') {
					mov->transforms = whiteKnight;
				}
			}
		}
		else if (bitboard->blackPiecesList[0] & (1ULL << mov->from)) {
			if (mv[4] != '\0') {
				if (mv[4] == 'q') {
					mov->transforms = blackQueen;
				}
				else if (mv[4] == 'r') {
					mov->transforms = blackRook;
				}
				else if (mv[4] == 'b') {
					mov->transforms = blackBishop;
				}
				else if (mv[4] == 'n') {
					mov->transforms = blackKnight;
				}
			}
		}
		if (bitboard->enPassant == mov->to && (bitboard->whitePiecesList[0] & (1ULL << mov->from) || bitboard->blackPiecesList[0] & (1ULL << mov->from))) {
			mov->isEnPassantMove = true;
		}
	}
}
char SEPARATOR_BOARDS = ' ';
bool parseGo(char* line, Bitboard* bitboard) {
		bool white = true;
		char * ptrChar = strstr(line, "moves");

		if (ptrChar != NULL) {
			ptrChar += 6;

			while (*ptrChar) {
				Move move2;
				ParseMove64(ptrChar, bitboard, &move2);
				//if (!checkMove(move, b->pieces)) break;
				//move2.keyBeforeMove = simpleHash(bitboard);
				//b->PlayMove(&move);
				for (int i = 0; i < 6; i++) {
					if (bitboard->whitePiecesList[i] & (1ULL << move2.from)) {
						move2.movedPiece = whitePawn + i;
						break;
					}

					if (bitboard->blackPiecesList[i] & (1ULL << move2.from)) {
						move2.movedPiece = blackPawn + i;
						break;
					}

				}

				for (int i = 0; i < 6; i++) {
					if (bitboard->whitePiecesList[i] & (1ULL << move2.to)) {
						move2.taken = whitePawn + i;
					}

					if (bitboard->blackPiecesList[i] & (1ULL << move2.to)) {
						move2.taken = blackPawn + i;
					}

				}

				bitboard->PlayMove(&move2);
				white = !white;
				while (*ptrChar && *ptrChar != ' ') ptrChar++;
				ptrChar++;
			}
			//delete line;
			//printBitboard(bitboard);
			//printBoard(bitboard->pieces);

		}
	return white;
}

std::string getStateOfBoard(Bitboard * b, bool whiteOnMove) {
	std::string result = "";
	//std::cout << std::to_string(b->whitePiecesList[0]) << std::endl;
	for (int i = 0; i < 6; i++) {
		result += std::to_string(b->whitePiecesList[i]) + SEPARATOR_BOARDS;
	}
	for (int i = 0; i < 6; i++) {
		result += std::to_string(b->blackPiecesList[i]) + SEPARATOR_BOARDS;
	}
	result += std::to_string(whiteOnMove);
	return result;
}
void formateDataForNN() {
	int i = 0;
	std::ifstream f;
	std::ofstream out;
	std::string fileName = "data" + std::to_string(i) + ".txt";
	std::string outFileName = "nn" + std::to_string(i) + ".txt";
	f.open(fileName, std::fstream::out);
	out.open(outFileName);
	while (f.good()) {
		bool posString = true;
		// transfer it to data for NN
		std::string line;
		while (getline(f, line)) {
			char *cstr = (char *)malloc(line.size() + 1);
			std::copy(line.begin(), line.end(), cstr);
			cstr[line.size()] = '\0';
			// process game
			if (posString) {
				Bitboard* b = new Bitboard();
				bool white = parseGo(cstr, b);
				out << getStateOfBoard(b, white);
				delete[] b->history.moves;
				delete b;
			}
			else {
				out << line;
			}
			out << '\n';
			posString = !posString;
			delete cstr;
			//std::cout << cstr << std::endl;
			// using printf() in all tests for consistency
			//printf("%s", line.c_str());
		}
		//Go to another file
		i++;
		fileName = "data" + std::to_string(i) + ".txt";
		f.close();
		out.close();
		outFileName = "nn" + std::to_string(i) + ".txt";
		out.open(outFileName);
		f.open(fileName, std::fstream::out);
	}
}