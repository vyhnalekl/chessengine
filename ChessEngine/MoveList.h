#pragma once
#include "Definitions.cpp"
class MoveList
{
public:
	Move* moves; // TODO how many possible moves
	unsigned short length;
	unsigned short size;

public:
	MoveList(unsigned short);
	~MoveList();
	void pushMove(Move);
	Move popMove();
	bool empty();
	void copy(MoveList);
};

