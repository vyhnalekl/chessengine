// ChessEngine.cpp :
// Made by Lukas Vyhnalek

#include "stdafx.h"
#include "Definitions.cpp"
#include "Board.h"
#include <iostream>
#include "Debug.h"
#include "TEST.h"
#include "Search.h"
#include "HashGenerator.h"
#include <chrono>
#include <list>
#include <string>
#include "HashTests.h"
#include "communication.h"
#include "rating.h" // TODO debug remove
#include "Learning.h"
#include "Bitboard.h"
#include "CNNComunication.h"

namespace {


	int from120To64(char sq) {
		int ff = (sq % 10) - 1;
		int rf = (sq - 21) / 10;
		return (rf * 8) + ff;
	}

	char* print64(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}

	char* print642(char sq) {
		static char r[2];
		r[0] = ('a' + (sq % 8));
		r[1] = ('1' + (sq / 8));
		return r;
	}
	char squareTo120(char sq64) {
		int result = A1;
		result += (sq64 / 8) * 10;
		result += sq64 % 8;
		return result;
	}
	void PrintMoveList64(MoveList * ml) {
		for (int i = 0; i < ml->length; i++) {
			Move mv = ml->moves[i];
			std::cout << print64(mv.from) << "-->" << print642(mv.to) << std::endl;
		}
	}
	bool checkMoveLists(MoveList *ml1, MoveList *ml2) {
		Move mv, mv2;
		Move old;
		bool found = false;
		for (int j = 0; j < ml2->length; j++) {
			// nothing to undo
			mv = ml2->moves[j];
			char from = from120To64(mv.from);
			char to = from120To64(mv.to);
			for (int i = 0; i < ml1->length; i++) {
				mv2 = ml1->moves[i];
				if (from == mv2.from && to == mv2.to) {
					found = true;
				}
			}
			if (!found) {
				std::cout << "Eroor: \t" << PrMove(old) << " not found." << std::endl;
				PrintMoveList64(ml1);
				return false;
			}
		}
		return true;
	}
	void PrintMove64(Move mv) {
		std::cout << print64(mv.from) << print642(mv.to) << std::endl;
	}
}
class ChessEngine {
public:
	//struct tm timeRemaining;
	Board *b;
	bool white;
	bool searchInFile = true;
	SearchData* sd;
	Bitboard *bitboard;
	//Transposition **table;
public:
	ChessEngine(bool);
	/*Move getMove();
	void opponentPlayed(Move);*/
	void ParseGo(char []);
	void UciGo();
};

void printBitboard(Bitboard *b) {
	std::cout << std::endl;
	char pieces[120];
	for (int i = 0; i < 120; i++) {
		pieces[i] = EMPTY; // TODO, still empty on left and right side
	}
	for (int i = 0; i < 6; i++) {
		U64 temp = b->whitePiecesList[i];
		while (temp) {
			char sq = POP(&temp);
			pieces[squareTo120(sq)] = i + 1;
		}
	}
	for (int i = 0; i < 6; i++) {
		U64 temp = b->blackPiecesList[i];
		while (temp) {
			char sq = POP(&temp);
			pieces[squareTo120(sq)] = i + 7;
		}
	}
	printBoard(pieces);
	std::cout << std::endl;
}

ChessEngine::ChessEngine(bool file) {
	//this->timeRemaining = myTime;
	this->searchInFile = false;
	this->white = true;
	this->b = new Board();
	this->bitboard = new Bitboard();
	this->sd = new SearchData();
	initializeTable();
}
/*
Move ChessEngine::getMove() {
	sd->maxDepth = 40;
	sd->maxDepthQuiesce = -5;
	struct tm temp = { 0 };

	if (bitboard->history.length < 15) {
		sd->timeOnMove = timeRemaining.tm_sec / 40;
	}
	else if (bitboard->history.length < 35) {
		sd->timeOnMove = timeRemaining.tm_sec / 40;
	}
	else {
		sd->timeOnMove = timeRemaining.tm_sec / 30;
	}
	// mktime(temp);

	auto start = std::chrono::high_resolution_clock::now();

	//alphaBetaMax(b, sd, INT_MIN, INT_MAX, 6);
	iterativeDeepening(b, sd);

	auto finish = std::chrono::high_resolution_clock::now();

	auto microseconds = std::chrono::duration_cast<std::chrono::seconds>(finish - start);
	
	timeRemaining.tm_sec = timeRemaining.tm_sec - microseconds.count();
	
	
	//result = iterativeDeepening(b,sd);
	//Move result = AlphaBetaSearch(b);
	//gameHistory.push_back(result);
	bitboard->PlayMove(&bitboard->bestMove);
	printBoard(bitboard->pieces);
	printMove(bitboard->bestMove);
	std::cout << "Took: "<< microseconds.count() << "s\n";
	std::cout << "Score: " << bitboard->bestMove.score << std::endl;
	std::cout << "Depth : " << sd->depthReached << std::endl;
	std::cout << "Number Of Positions : " << sd->numberOfPositions << std::endl;
	std::cout << "Number Of Quiscene Positions : " << sd->quiescencePositions << std::endl;
	sd->numberOfPositions = 0;
	sd->quiescencePositions = 0;
	bitboard->whiteOnMove = !white;//change the color on move (whiteOnMove is also changed when generating moves)
	return bitboard->bestMove;
}
/*
char squareTo120(char sq64) {
	int result = A1;
	result += (sq64 / 8) * 10;
	result += sq64 % 8;
	return result;
}*//*
char sqTo120(char sq64) {
	int result = A1;
	result += (sq64 / 8) * 10;
	result += sq64 % 8;
	return result;
}

char *PrMove(Move move) {

	static char MvStr[6];

	int ff = (move.from % 10) - 1;
	int rf = (move.from - 21) / 10;
	int ft = (move.to % 10) - 1;
	int rt = (move.to - 21) / 10;
	int promoted = move.transforms;

	if (promoted) {
		char pchar = 'q';
		if (promoted == whiteKnight || promoted == blackKnight) {
			pchar = 'n';
		}
		else if (promoted == whiteRook || promoted == blackRook) {
			pchar = 'r';
		}
		else if (promoted == whiteBishop || promoted == blackBishop) {
			pchar = 'b';
		}
		sprintf_s(MvStr, "%c%c%c%c%c", ('a' + ff), ('1' + rf), ('a' + ft), ('1' + rt), pchar);
	}
	else {
		sprintf_s(MvStr, "%c%c%c%c", ('a' + ff), ('1' + rf), ('a' + ft), ('1' + rt));
	}

	return MvStr;
}*/
void ParseMove64(char* mv, Bitboard* bitboard, Move *mov) {
	char from = ((mv[1] - '1') * 8) + (mv[0] - 'a');
	char to = ((mv[3] - '1') * 8) + (mv[2] - 'a');
	mov->from = from;
	mov->to = to;
	if (bitboard->whitePiecesList[0] & (1ULL << mov->from) && mov->from == mov->to - 16) {
		mov->enPassant = 1ULL << (mov->from + 8);
	}
	if (bitboard->blackPiecesList[0] & (1ULL << mov->from) && mov->from == mov->to + 16) {
		mov->enPassant = 1ULL << (mov->from - 8);
	}
	//if the line above is missing engine cannot detect black casteling move
	if (bitboard->whitePiecesList[5] & (1ULL << mov->from)) {
		if ((mov->from == e1 && mov->to == g1) || (mov->from == e1 && mov->to == c1)) {
			mov->isCastelingMove = true;
		}
	}
	if (bitboard->blackPiecesList[5] & (1ULL << mov->from)) {
		if ((mov->from == e8 && mov->to == g8) || (mov->from == e8 && mov->to == c8)) {
			mov->isCastelingMove = true;
		}
	}
	if (bitboard->whitePiecesList[0] & (1ULL << mov->from)) {
		if (mv[4] != '\0') {
			if (mv[4] == 'q') {
				mov->transforms = whiteQueen;
			}
			else if (mv[4] == 'r') {
				mov->transforms = whiteRook;
			}
			else if (mv[4] == 'b') {
				mov->transforms = whiteBishop;
			}
			else if (mv[4] == 'n') {
				mov->transforms = whiteKnight;
			}
		}
	}
	else if (bitboard->blackPiecesList[0] & (1ULL << mov->from)) {
		if (mv[4] != '\0') {
			if (mv[4] == 'q') {
				mov->transforms = blackQueen;
			}
			else if (mv[4] == 'r') {
				mov->transforms = blackRook;
			}
			else if (mv[4] == 'b') {
				mov->transforms = blackBishop;
			}
			else if (mv[4] == 'n') {
				mov->transforms = blackKnight;
			}
		}
	}
	if (bitboard->enPassant == mov->to && (bitboard->whitePiecesList[0] & (1ULL << mov->from) || bitboard->blackPiecesList[0] & (1ULL << mov->from))) {
		mov->isEnPassantMove = true;
	}
}
void ChessEngine::ParseGo(char line[]) {
	//PrintMoveList64(&bitboard->history);
	printBitboard(bitboard);
	debugRate(bitboard, white);
	int depth = 50;
	int time = 300000;
	char *ptr = NULL;

	if ((ptr = strstr(line, "wtime")) && white) {
		time = atoi(ptr + 6);
		//std::cout << "wtime" << time << std::endl;
	}

	if ((ptr = strstr(line, "btime")) && !white) {
		time = atoi(ptr + 6);
		//std::cout << "btime" << time << std::endl;
	}

	if ((ptr = strstr(line, "depth"))) {
		depth = atoi(ptr + 6);
		//std::cout << "depth" << depth << std::endl;
	}

	sd->maxDepth = depth;
	if (bitboard->history.length <= 12) {
		sd->timeOnMove = (time / 40);
	}
	else if (bitboard->history.length >= 60) {
		sd->timeOnMove = (time / 10);
	}
	else if (bitboard->history.length >= 40) {
		sd->timeOnMove = (time / 20);
	}
	else {
		sd->timeOnMove = (time / 35);
	}
	sd->searchInFile = this->searchInFile;
	//std::cout << sd->timeOnMove << " seconds on Move " << std::endl;
	bitboard->whiteOnMove = white;
	auto start = std::chrono::high_resolution_clock::now();
	iterativeDeepening(sd, bitboard);
	//printMove(bitboard->bestMove);
	
	std::cout << "bestmove "<< PrMove(bitboard->bestMove) << std::endl;
	auto finish = std::chrono::high_resolution_clock::now();

	auto microseconds = std::chrono::duration_cast<std::chrono::seconds>(finish - start);
	//bitboard->PlayMove(&bitboard->bestMove);
	/*std::cout << "Took: " << microseconds.count() << "s" <<std::endl;
	std::cout << "Score: " << bitboard->bestMove.score << std::endl;
	std::cout << "Depth : " << sd->depthReached << std::endl;
	std::cout << "Number Of Positions : " << sd->numberOfPositions << std::endl;
	std::cout << "Number Of Quiscene Positions : " << sd->quiescencePositions << std::endl;
	std::cout << "HELPS ALL TIME: " << bitboard->tHelpAllTime << std::endl;
	std::cout << "COLISIONS ALL TIME: " << bitboard->tCollisionAllTime << std::endl;*/
	sd->numberOfPositions = 0;
	sd->quiescencePositions = 0;
}
void ChessEngine::UciGo() {
		char line[INPUTBUFFER];
		//setvbuf(stdin, line, NULL, INPUTBUFFER);
		
		
		
		std::cout << "uciok" << std::endl;
		std::cout << "id name ChessEngine" << std::endl;
		std::cout << "id autor LukasVyhnalek" << std::endl;
		while (true) {
			memset(&line[0], 0, sizeof(line));
			//fflush(stdin);
			//fflush(stdout);
			if (!fgets(line, INPUTBUFFER, stdin))
				continue;

			if (line[0] == '\n')
				continue;


			if (!strncmp(line, "isready", 7)) {
				std::cout<< "readyok" << std::endl;
			}
			else if (!strncmp(line, "position", 8) ) {
				char *ln = line;
				ln += 9;
				char *ptrChar = ln;

				if (strncmp(ln, "startpos", 8) == 0) {
					unsigned long long thelps = bitboard->tHelpAllTime;
					unsigned int tcollision = bitboard->tCollisionAllTime;
					free(bitboard->history.moves);
					free(b->history.moves);
					delete b;
					delete bitboard;
					b = new Board();
					bitboard = new Bitboard();
					bitboard->tCollisionAllTime = tcollision;
					bitboard->tHelpAllTime = thelps;
					white = true;
				}
				/*else {
					ptrChar = strstr(ln, "fen");
					if (ptrChar == NULL) {
						unsigned long long thelps = bitboard->tHelpAllTime;
						unsigned int tcollision = bitboard->tCollisionAllTime;
						delete b;
						b = new Board();
						bitboard->tCollisionAllTime = tcollision;
						bitboard->tHelpAllTime = thelps;
					}
					else {
						ptrChar += 4;
						bitboard->SetFenPosition(ptrChar);
					}
				}*/
				
				ptrChar = strstr(ln, "moves");

				if (ptrChar != NULL) {
					ptrChar += 6;
					this->white = true;
					while (*ptrChar) {
						Move move2;
						ParseMove64(ptrChar, bitboard, &move2);
						//if (!checkMove(move, b->pieces)) break;
						bitboard->whiteOnMove = white;
						move2.keyBeforeMove = simpleHash(bitboard);
						//b->PlayMove(&move);
						for (int i = 0; i < 6; i++) {
							if (bitboard->whitePiecesList[i] & (1ULL << move2.from)) {
								move2.movedPiece = whitePawn + i;
								break;
							}

							if (bitboard->blackPiecesList[i] & (1ULL << move2.from)) {
								move2.movedPiece = blackPawn + i;
								break;
							}
							
						}

						for (int i = 0; i < 6; i++) {
							if (bitboard->whitePiecesList[i] & (1ULL << move2.to)) {
								move2.taken = whitePawn + i;
							}

							if (bitboard->blackPiecesList[i] & (1ULL << move2.to)) {
								move2.taken = blackPawn + i;
							}

						}
						
						bitboard->PlayMove(&move2);
						this->white = !this->white;
						while (*ptrChar && *ptrChar != ' ') ptrChar++;
						ptrChar++;
					}
				}
				printBitboard(bitboard);
				//printBoard(bitboard->pieces);
			}
			else if (!strncmp(line, "ucinewgame", 10)) {
				unsigned long long thelps = bitboard->tHelpAllTime;
				unsigned int tcollision = bitboard->tCollisionAllTime;
				free(bitboard->history.moves);
				delete bitboard;
				b = new Board();
				bitboard = new Bitboard();
				bitboard->tCollisionAllTime = tcollision;
				bitboard->tHelpAllTime = thelps;
				white = true;
			}
			else if (!strncmp(line, "go", 2)) {
				int i = 2;
				std::string ln = "";
				while (line[i] != '\0') {
					ln += line[i];
					i++;
				}
				ParseGo((char*)ln.c_str());

			}
			else if (!strncmp(line, "quit", 4)) {
				sd->end = true;
				break;
			}
			else if (!strncmp(line, "uci", 3)) {
				std::cout << "uciok" << std::endl;
				std::cout << "id name ChessEngine" << std::endl;
				std::cout << "id autor LukasVyhnalek" << std::endl;
			}
			else if (!strncmp(line, "option print save moves white", 29)) {
				printFile(true);
			}
			else if (!strncmp(line, "option print save moves black", 29)) {
				printFile(false);
			}
			else if (!strncmp(line, "option set filesearch false", 27)) {
				this->searchInFile = false;
			}
			if (sd->end) break;
		}
}
/*
void ChessEngine::opponentPlayed(Move mv) {
	(*b).PlayMove(&mv);
	//gameHistory.push_back(mv);
}*/

bool white = true;
int main()
{
	Init(false);
	InitRatingStructures();
	ChessEngine *che = new ChessEngine(false);// Init() initialize hash generator
											//return true if constants are loaded from file
	//hashTest(che->bitboard);
											/*struct tm time = { 0 };
	time.tm_sec = 10 * 60;
	
	Board *b = new Board();
	std::cout << "Evaluation: " << rateBoard(*b) << std::endl;*/
	che->UciGo(); //TODO uncomment to start the engine
	//formateDataForNN();
	/*
	bool chessEnginePlays = true; // true == white
	int stop = 0;
	struct tm time = { 0 };
	time.tm_sec = 10 * 60;
	
	while (true) {
		//Transposition result;
		(*che).bitboard->whiteOnMove = white;
		(*che).bitboard->whiteOnMove = white;
		white = !white;
		auto start = std::chrono::high_resolution_clock::now();
		//result = AlphaBetaSearch((*che).b, (*che).sd, 5, INT_MIN)
		//alphaBetaMin(che->b, che->sd,INT_MIN, INT_MAX, 6);
		che->sd->maxDepth = 20;
		che->sd->timeOnMove = 12000;
		iterativeDeepening(che->b, che->sd, che->bitboard);
		auto finish = std::chrono::high_resolution_clock::now();

		auto microseconds = std::chrono::duration_cast<std::chrono::seconds>(finish - start);
		if (che->bitboard->bestMove.from == BLACK_WIN) {
			std::cout << "**********" << std::endl;
			std::cout << "BLACK WINS" << std::endl;
			std::cout << "**********" << std::endl;
			std::cin >> stop;
			break;
		}
		else if (che->bitboard->bestMove.from == WHITE_WIN) {
			std::cout << "**********" << std::endl;
			std::cout << "WHITE WINS" << std::endl;
			std::cout << "**********" << std::endl;
			std::cin >> stop;
			break;
		}
		else if (che->bitboard->bestMove.from == DRAW || che->bitboard->isDraw()) {
			std::cout << "**********" << std::endl;
			std::cout << "   DRAW" << std::endl;
			std::cout << "**********" << std::endl;
			std::cin >> stop;
			break;
		}
		(*che).b->PlayMove(&(*che).b->bestMove);
		(*che).bitboard->PlayMove(&(*che).bitboard->bestMove);
		
		//printBoard((*che).bitboard->pieces);
		PrintMove64(che->bitboard->bestMove);
		std::cout << "Took: " << microseconds.count() << "s\n";
		std::cout << "Score: " << che->bitboard->bestMove.score << std::endl;
		std::cout << "Depth : " << che->sd->depthReached << std::endl;
		std::cout << "Number Of Positions : " << (*che).sd->numberOfPositions << std::endl;
		std::cout << "Number Of Quiscene Positions : " << (*che).sd->quiescencePositions << std::endl;
		//*che).bitboard->whiteOnMove = !(*che).bitboard->whiteOnMove;//change the color on move (whiteOnMove is also changed when generating moves)
		//(*che).bitboard->whiteOnMove = !(*che).bitboard->whiteOnMove;
		(*che).sd->numberOfPositions = 0;
		che->sd->quiescencePositions = 0;
	}
	*/
    return 0;
}

