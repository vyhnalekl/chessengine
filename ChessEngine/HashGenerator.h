#pragma once
#include "Board.h"

unsigned long long getRandom64();

bool Init(bool);

/*unsigned long long generatePositionKey( Board *const);
unsigned long long updateKey( Board*const , Move);*/
unsigned long long simpleHash(Board* b);

unsigned long long simpleHash(Bitboard* b);

unsigned long long hashMove(U64 key, Move* mv);

unsigned long long hashTransformationMove(U64 key, Move* mv);

unsigned long long hashWhiteEnPassantMove(U64 key, Move* mv);

unsigned long long hashBlackEnPassantMove(U64 key, Move *mv);

unsigned long long hashCastelingQueenBlackMove(U64 key, Move * mv);

unsigned long long hashCastelingKingBlackMove(U64 key, Move * mv);

unsigned long long hashCastelingQueenWhiteMove(U64 key, Move * mv);

unsigned long long hashCastelingKingWhiteMove(U64 key, Move * mv);

unsigned long long hashCastelingChange(U64 key, char from, char to);